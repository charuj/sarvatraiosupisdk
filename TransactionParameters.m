//
//  TransactionParameters.m
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "TransactionParameters.h"
#import "ApiClient.h"
#import "SdkException.h"

@implementation TransactionParameters


@synthesize payeeParam;
//@synthesize parameterMode;
@synthesize payeeMode;
@synthesize amount;
@synthesize remark;
@synthesize customerAccount;
@synthesize payerParam;


PayeeMode payeeMode;
ParameterMode parameterMode;


BOOL usingDefaultAccount;


-(id)initWithVPA:(NSString *)vpa{
   // PayeeMode = VPA;

    return self;
}

-(int)getParameterMode{
    return (int)parameterMode;
}

-(BOOL)isUsingDefaultAccount{
    return usingDefaultAccount;
}


- (id)initWithVirtualAddress:(VirtualAddress *)virtualAddr{
    if( self = [super init] )
    {
     payerParam = [[PayerParam alloc] init];
     payerParam.vr = virtualAddr;
     parameterMode = Manage;
     [self findDefaultDebit:payerParam];
    }
    return self;
}


-(void)setDebitAccount : (CustomerAccount*)customerAccount{

    

}


//-(id)initWithAccount:(NSString *)payeeName ifsc:(NSString *)ifsc account:(NSString *)account{
//
//    if( self = [super init] )
//    {
//        payeeName = payeeName;
//        ifsc = ifsc;
//        account = account;
//
//    }
//
//    return self;
//}


-(id)initWithTransaction:(VirtualAddress*)from_vpa payee:(PayeeParam *)payee amount:(NSDecimalNumber *)amountToTrans remark:(NSString *)tranRemark{
    if( self = [super init] )
    {
        payerParam = [[PayerParam alloc] init];
       //  virtualAddress = [[VirtualAddress alloc] init];
        //  virtualAddress = from_vpa;

        payerParam.vr = from_vpa;
        payeeParam = payee;
        amount = amountToTrans;
        remark = tranRemark;
        parameterMode = Pay;
        payeeMode = (unsigned int)payee.getPayeeMode;
        [self findDefaultDebit:payerParam];

 //       NSLog(@"initWithTransaction TEST payerParam.payerVpa : %@",payerParam.vr.getCustomerAccounts.getAccount);
//        NSLog(@"Prameter vpa : %@",payerParam.payerVpa);
//        NSLog(@"Prameter payeeParam : %@",payee.account);
//        NSLog(@"Prameter amount : %@",amount);
//        NSLog(@"Prameter remark : %@",remark);
//        NSLog(@"Prameter parameterMode : %u",parameterMode);
//        NSLog(@"Prameter payee.getPayeeMode : %u",payee.getPayeeMode);
//        NSLog(@"Prameter payee.getPayeeMode : %u",payeeMode);

    }

    return self;
}

-(id)initWithTransaction:(VirtualAddress *)payee payer:(PayerParam*)payer  amount:(NSDecimalNumber *)amountToTrans remark:(NSString *)tranRemark{
    if( self = [super init] )
    {

        payerParam = [[PayerParam alloc] init];
        payerParam.payerVpa = payer.payerVpa;


        payeeParam = [PayeeParam alloc];
        
        payeeParam = [payeeParam initWithVirtualAddress:payee];
        amount = amountToTrans;
        remark = tranRemark;
        parameterMode = Collect;
      //  payeeMode = (unsigned int)payee.getPayeeMode;
        [self findDefaultDebit:payerParam];

        NSLog(@"Prameter payerParam.payerVpa : %@",payerParam.payerVpa);

    }
    
    return self;
}


-(void)validate:(int)parameter_mode{

    NSLog(@"Prameter Mode : %u",parameterMode);

    ApiClient* apiclient = [[ApiClient alloc]init];

    if (parameterMode != parameter_mode ) {

        @throw [[SdkException alloc] initWithName:@"Parameter Mode" reason:@"Invalid state of Parameters" userInfo:nil];
    }

    NSString* transactionId = [apiclient generateTransactionID];

}

-(void)findDefaultDebit:(PayerParam*)payer{

    //NSLog(@"findDefaultDebit Payer Param : %@",payer.getPayerVpa);
    customerAccount = [[CustomerAccount alloc] init];
    virtualAddress = [[VirtualAddress alloc] init];
    virtualAddress = payerParam.getPayerVpa;

    customerAccount = [self findDefaultAccount:virtualAddress];
//    NSLog(@"findDefaultDebit customerAccount : %@",customerAccount.getIfsc);
//    NSLog(@"findDefaultDebit customerAccount : %@",customerAccount.getName);


    if (customerAccount != nil) {
        usingDefaultAccount = true;
    }

}

-(CustomerAccount*)findDefaultAccount : (VirtualAddress*)vr{
    if (vr == nil)
        return nil;

    customerAccount = [[CustomerAccount alloc] init];
    customerAccount = virtualAddress.custAccount;
   // NSLog(@"findDefaultAccount customerAccount : %@ ", customerAccount.getAccount);

    if (customerAccount.getDefaultDebit) {
        return customerAccount;
    }
    return customerAccount;

}



@end
