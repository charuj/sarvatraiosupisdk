//
//  RegisterVPAService.m
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "RegisterVPAService.h"
#import "ApiProxyProtocol.h"
#import "SrvtCommonLib.h"

Mode* mode;

@implementation RegisterVPAService



-(id)initWith:(CardData*)card_data customerAccount:(CustomerAccount*)cust_acc mode:(Mode*)modeOfUser mpin:(NSString*)mpin_param otpParam:(NSString*)otp_param status:(AccountDefaultStatus*)acc_def_status{
    if (!(self = [super init]))
    {
        cardData = card_data;
        mode = modeOfUser;
        mpinParam = mpin_param;
        otpParam = otp_param;
        custAccount = cust_acc;
        accDefaultStatus = acc_def_status;

    }
    return self;
}

-(void)addApiParameters{

    NSLog(@"RegisterVPAService Card Data : %@",cardData.getLastSix);

   // NSString *actionFlag = mode.Register ? @"R" : @"P";
     NSString *actionFlag = @"R";

  //  NSString date = [NSString stringWithFormat:@"%s",cardData.expiryMonth];
    NSString* exdate = @"";


//    NSDictionary * param = @{@"action-flag":actionFlag ,@"card-digits":[cardData getLastSix] ,@"expiry-date":date,@"mpin":mpinParam,@"account-provider":[custAccount getAccountProvider],@"account-number":[custAccount getAccount],@"account-type":[custAccount getAccountType],@"default-debit":[bool [custAccount getDefaultDebit]],@"default-credit":[custAccount getDefaultCredit],@"ifsc":[custAccount getIfsc],@"otp":otpParam,@"name":[custAccount getName],@"credsAllowedJson":[custAccount getCredentialData]};

    //NSLog(@"RegisterVPAService param :",param);

}

//-(void)registerMobile : (NSDictionary*)param withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{
//    NSLog(@"RegisterVPAService Card Data :",param);
//
//    ApiProxyProtocol *apiProxy = [[ApiProxyProtocol alloc] init];
//
//    [apiProxy registerMobile:param withCompletion:^(BOOL success, NSError *error, id response) {
//
//        NSLog(@"login responce ==  %@",response); // here you get response once method camplete
//
//
//    }];
//
//
//}

@end
