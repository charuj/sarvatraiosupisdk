//
//  DeviceIdViewController.h
//  TestCommonSdkApp
//
//  Created by Charushila on 05/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Security/Security.h>
#import "FDKeychain.h"

@interface DeviceIdViewController : UIViewController
-(NSString *)generateUUID ;
@end
