//
//  SrvtCommonLib.m
//  SrvtFramework
//
//  Created by Charushila on 06/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "SrvtCommonLib.h"
#import <Foundation/Foundation.h>
#include <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonKeyDerivation.h>
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonHMAC.h>
#import <Security/Security.h>


@implementation SrvtCommonLib

- (NSString *)getChallenge:(NSString *)type device_Id:(NSString*)device_Id app_Id:(NSString*)app_Id{
    
    NSString* challenge = nil;
    NSError* error = nil;
    NSLog(@"getChallenge type : %@",type);
    NSLog(@"getChallenge Challenge : %@",device_Id);
    NSLog(@"getChallenge Challenge : %@",app_Id);

    BOOL success = [CLServices getChallengeForDeviceId:device_Id appId:app_Id type:type challenge:&challenge error:&error];
    
    NSLog(success ? @"SrvtCommonLib Yes" : @"SrvtCommonLib No");
    
    NSLog(@"SrvtCommonLib Challenge : %@",challenge);
    return challenge;
}


- (NSString*)populateHMAC:(NSString*)app_id mobile:(NSString*)mobile token:(NSString*)token deviceId:(NSString*)deviceId{
    NSString *hmac = @"";

    @try {

        NSString *message = [NSString stringWithFormat:@"%@|%@|%@",app_id,mobile,deviceId];
      //  NSLog(@"SrvtCommonLib PSP HMac msg:%@",message);
        NSData *decodedToken = [[NSData alloc] initWithBase64EncodedString:token options:0];
        NSString *decodedTokenString = [[NSString alloc] initWithData:decodedToken encoding:NSUTF8StringEncoding];
     //   NSLog(@"SrvtCommonLib NPCI decodedTokenString:%@",decodedTokenString);

     //   NSLog(@"SrvtCommonLib NPCI data : %@",[[self sha256:message] dataUsingEncoding:NSUTF8StringEncoding]);
        NSData *hmacData = [self encrypt:[[self sha256:message] dataUsingEncoding:NSUTF8StringEncoding] key:decodedTokenString iv:nil];
        if (hmacData) {
            hmac = [hmacData base64EncodedStringWithOptions:0];
     //       NSLog(@"SrvtCommonLib NPCI hmac:%@",hmac);
        }

        return hmac;

    } @catch (NSException *exception) {
        NSLog(@"SrvtCommonLib NSException : %@",exception);
    }

}


- (NSString*) sha256:(NSString *)key{
    const char *cstr = [key cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:key.length];
    uint8_t digest[CC_SHA256_DIGEST_LENGTH];
    
    // This is an iOS5-specific method.
    // It takes in the data, how much data, and then output format, which in this case is an int array.
    CC_SHA256(data.bytes, (int)data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    
    // Parse through the CC_SHA256 results (stored inside of digest[]).
    for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
   // NSLog(@"SrvtCommonLib SHA256 NPCI : %@",output);
    
    return output;
}


- (NSData *)encrypt:(NSData *)plainText key:(NSString *)key  iv:(NSString *)iv {
    char keyPointer[kCCKeySizeAES256+2],
    ivPointer[kCCBlockSizeAES128];
    BOOL patchNeeded;
    bzero(keyPointer, sizeof(keyPointer)); // fill with zeroes for padding
    //key = [[StringEncryption alloc] md5:key];
    patchNeeded= ([key length] > kCCKeySizeAES256+1);
    if(patchNeeded)
    {
        // NSLog(@"Key length is longer %lu", (unsigned long)[[[StringEncryption alloc] md5:key] length]);
        key = [key substringToIndex:kCCKeySizeAES256]; // Ensure that the key isn't longer than what's needed (kCCKeySizeAES256)
    }
    
    //NSLog(@"md5 :%@", key);
    [key getCString:keyPointer maxLength:sizeof(keyPointer) encoding:NSUTF8StringEncoding];
    [iv getCString:ivPointer maxLength:sizeof(ivPointer) encoding:NSUTF8StringEncoding];
    
    //    if (patchNeeded) {
    //        keyPointer[0] = '\0';  // Previous iOS version than iOS7 set the first char to '\0' if the key was longer than kCCKeySizeAES256
    //    }
    
    NSUInteger dataLength = [plainText length];
    
    // For block ciphers, the output size will always be less than or equal to the input size plus the size of one block.
    size_t buffSize = dataLength + kCCBlockSizeAES128;
    void *buff = malloc(buffSize);
    
    size_t numBytesEncrypted = 0;
    
    
    
    CCCryptorStatus status = CCCrypt(kCCEncrypt, /* kCCEncrypt, etc. */
                                     kCCAlgorithmAES128, /* kCCAlgorithmAES128, etc. */
                                     kCCOptionPKCS7Padding, /* kCCOptionPKCS7Padding, etc. */
                                     keyPointer, kCCKeySizeAES256, /* key and its length */
                                     NULL, /* initialization vector - use random IV everytime */
                                     [plainText bytes], [plainText length], /* input  */
                                     buff, buffSize,/* data RETURNED here */
                                     &numBytesEncrypted);
    
    
    if (status == kCCSuccess) {
        return [NSData dataWithBytesNoCopy:buff length:numBytesEncrypted];
    }
    
    free(buff);
    return nil;
}

- (BOOL)registerApp:(NSString *)app_Id mobile_no:(NSString*)mobile_no device_Id:(NSString*)device_Id token:(NSString*)token{
    
    NSError* register_error = nil;
    NSString* hmac = nil;

    hmac = [self populateHMAC:app_Id mobile:mobile_no token:token deviceId:device_Id ];
    BOOL success_register=  [CLServices registerAppWithHmac:hmac appID:app_Id mobile:mobile_no deviceID:device_Id error:&register_error];
    
    NSLog(success_register ? @"Yes" : @"No");
    return success_register;
    
}

- (NSString*)populateTrust:(NSString*)trust_Input token:(NSString*)token{
    NSString *trust_hmac = @"";
    
   // NSLog(@"SrvtCommonLib PSP HMac msg:%@",trust_Input);

    @try {

        NSData *decodedToken = [[NSData alloc] initWithBase64EncodedString:token options:0];
        NSString *decodedTokenString = [[NSString alloc] initWithData:decodedToken encoding:NSUTF8StringEncoding];
      //  NSLog(@"SrvtCommonLib NPCI decodedTokenString:%@",decodedTokenString);

     //   NSLog(@"SrvtCommonLib NPCI data : %@",[[self sha256:trust_Input] dataUsingEncoding:NSUTF8StringEncoding]);
        NSData *hmacData = [self encrypt:[[self sha256:trust_Input] dataUsingEncoding:NSUTF8StringEncoding] key:decodedTokenString iv:nil];
        if (hmacData) {
            trust_hmac = [hmacData base64EncodedStringWithOptions:0];
      //      NSLog(@"SrvtCommonLib NPCI trust_hmac:%@",trust_hmac);
            
        }
        return trust_hmac;


    } @catch (NSException *exception) {
        NSLog(@"SrvtCommonLib NSException : %@",exception);
    }
    
}

- (NSMutableDictionary*)getCredential:(NSString*)keyKode keyXMLPayload:(NSString*)keyXMLPayload controls:(NSDictionary*)controls configuration:(NSDictionary*)configuration salt:(NSDictionary*)salt payInfo:(NSArray*)payInfo language:(NSString*)language token:(NSString*)token view:(UIViewController *)viewcontroller withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{
    
    @try {
        
        NSError* register_error = nil;

        NSString* trust;
        NSString *trust_input =@"";

     //   NSLog(@" In token: %@", token);


        if ([salt objectForKey:@"txnAmount"]!=NULL) {
            trust_input= [NSString stringWithFormat:@"%@|",[salt objectForKey:@"txnAmount"]];
        }
        
        if ([salt objectForKey:@"txnId"]!=NULL) {
            trust_input= [trust_input stringByAppendingFormat:@"%@|",[salt objectForKey:@"txnId"]];
            
        }
        if ([salt objectForKey:@"payerAddr"]!=NULL) {
            
            trust_input= [trust_input stringByAppendingFormat:@"%@|",[salt objectForKey:@"payerAddr"]];
        }
        if ([salt objectForKey:@"payeeAddr"]!=NULL) {
            
            trust_input= [trust_input stringByAppendingFormat:@"%@|",[salt objectForKey:@"payeeAddr"]];
        }

        trust_input= [trust_input stringByAppendingFormat:@"%@|%@|%@",[salt objectForKey:@"appId"] ,[salt objectForKey:@"mobileNumber"],[salt objectForKey:@"deviceId"]];

        trust = [self populateTrust:trust_input token:token];
        __block NSMutableDictionary* credential = nil;

        [CLServices getCredentialsPresentedFrom:viewcontroller controls:controls keyCode:keyKode keyXMLPayload:keyXMLPayload configuration:configuration salt:salt trust:trust payInfo:payInfo language:language completionHandler:^(int success, NSError *error, NSDictionary *cred)
         {

             credential = [NSMutableDictionary dictionary];
             
             if(cred != NULL){
                 credential = [cred mutableCopy];
                 [credential setValue:[NSString stringWithFormat:@"%d",success] forKey:@"resultCode"];
                 
             }else{
                 
                 [credential setValue:[NSString stringWithFormat:@"%d",success] forKey:@"resultCode"];
             }
             
             
            // NSLog(@"SrvtCommonLib register_error: %@", register_error);
             
             NSLog(@"SrvtCommonLib success = %d",success);
            // NSLog(@"SrvtCommonLib credential= %@",credential);

             if(completion){
                 dispatch_async(dispatch_get_main_queue(), ^{
                     completion(YES,nil,credential); // here that call when method complete
                 });
             }

         }];
        
        return credential;

    } @catch (NSException *exception) {
        NSLog(@"SrvtCommonLib NSException : %@",exception);
    }
    
}
@end
