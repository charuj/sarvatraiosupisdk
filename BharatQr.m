//
//  BharatQr.m
//  SrvtFramework
//
//  Created by Charushila on 20/09/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "BharatQr.h"

@implementation BharatQr


- (NSMutableArray*)parse : (NSString*)tlvStr {

    NSLog(@"TLV String : %@",tlvStr);
    NSMutableArray *array = [NSMutableArray array];

    [self recursive:tlvStr array:array];

    //    NSLog(@"TLV String mutable array : %@",array);

    return array;

}

-(LinkableTransaction*)handleBharatQrData : (NSMutableArray*)array linkableTransaction:linkableTransaction{

    NSString *pa;
    NSString *mam;
    NSString *tr;
    NSString *url;
    NSString *mc;
    NSString *cu;
    NSString *am;
    NSString *pn;
    NSString *tn;
    NSString *accNoIfsc;
    NSString *aadharNo;


    for (int i=0; i<array.count; i++) {
        NSArray* arrTest = [array objectAtIndex:i];

        if([[arrTest objectAtIndex:0] isEqualToString:@"26"]){

            NSMutableArray *ar = [self parse:[arrTest objectAtIndex:2]];
            //    NSLog(@"ObjectAtIndex ar 3 : %@",ar);

            for (int j=0; j<ar.count; j++) {
                NSMutableArray *ar2 = [ar objectAtIndex:j];

                if ([[ar2 objectAtIndex:0] isEqualToString:@"01"]) {
                    pa = [ar2 objectAtIndex:2];
                    [linkableTransaction setPayeeVPA:[ar2 objectAtIndex:2]];
                }
                if ([[ar2 objectAtIndex:0] isEqualToString:@"02"]) {
                    mam = [ar2 objectAtIndex:2];
                    [linkableTransaction setMinimumAmount:[ar2 objectAtIndex:2]];

                }
            }
            NSLog(@"Payee VPA : %@",pa);
            NSLog(@"MAM : %@",mam);
        }

        if([[arrTest objectAtIndex:0] isEqualToString:@"27"]){

            NSMutableArray *ar = [self parse:[arrTest objectAtIndex:2]];
            //   NSLog(@"ObjectAtIndex ar 3 : %@",ar);

            for (int j=0; j<ar.count; j++) {
                NSMutableArray *ar2 = [ar objectAtIndex:j];

                if ([[ar2 objectAtIndex:0] isEqualToString:@"01"]) {
                    tr = [ar2 objectAtIndex:2];
                    [linkableTransaction setTransactionReferenceID: [ar2 objectAtIndex:2]];

                }
                if ([[ar2 objectAtIndex:0] isEqualToString:@"02"]) {
                    url = [ar2 objectAtIndex:2];
                    [linkableTransaction setTransactionDetailsUrl: [ar2 objectAtIndex:2]];

                }
            }
            NSLog(@"tr : %@",tr);
            NSLog(@"url : %@",url);
        }

        if([[arrTest objectAtIndex:0] isEqualToString:@"52"]){

            NSLog(@"52 mc : %@",[arrTest objectAtIndex:2]);

             mc = [arrTest objectAtIndex:2];
            [linkableTransaction setPayeeMerchantCode:[arrTest objectAtIndex:2]];

        }

        if([[arrTest objectAtIndex:0] isEqualToString:@"53"]){

            if([[arrTest objectAtIndex:2] isEqualToString:@"356"]){
                NSLog(@"53 cu : INR");
                cu = @"INR";
                [linkableTransaction setPayeeMerchantCode:@"INR"];
            }
        }

        if([[arrTest objectAtIndex:0] isEqualToString:@"54"]){

            NSLog(@"54 am : %@",[arrTest objectAtIndex:2]);
            am = [arrTest objectAtIndex:2];

            if (![am containsString:@"."]) {
                am = [am stringByAppendingString:@".00"];
            }
            [linkableTransaction setAmount:[NSDecimalNumber decimalNumberWithString:am]];
        }

        if([[arrTest objectAtIndex:0] isEqualToString:@"59"]){

            NSLog(@"59 pn merchant name: %@",[arrTest objectAtIndex:2]);
            [linkableTransaction setPayeeName: [arrTest objectAtIndex:2]];
        }

        if([[arrTest objectAtIndex:0] isEqualToString:@"62"]){

            NSMutableArray *ar = [self parse:[arrTest objectAtIndex:2]];

            for (int j=0; j<ar.count; j++) {
                NSMutableArray *ar2 = [ar objectAtIndex:j];

                if ([[ar2 objectAtIndex:0] isEqualToString:@"08"]) {
                    tn = [ar2 objectAtIndex:2];
                    [linkableTransaction setRemarks:[ar2 objectAtIndex:2]];
                }
            }
            NSLog(@"62 tn  : %@",tn);
        }

        if([[arrTest objectAtIndex:0] isEqualToString:@"08"]){

            NSLog(@"08 acc no + ifsc  : %@",[arrTest objectAtIndex:2]);
            accNoIfsc = [arrTest objectAtIndex:2];

            [linkableTransaction setIfsc:[accNoIfsc substringToIndex:11]];
            [linkableTransaction setAccNo:[accNoIfsc substringFromIndex:11]];

            NSLog(@"08 ifsc  : %@",[accNoIfsc substringToIndex:11]);
            NSLog(@"08 acc no : %@",[accNoIfsc substringFromIndex:11]);

        }

        if([[arrTest objectAtIndex:0] isEqualToString:@"28"]){

            NSMutableArray *ar = [self parse:[arrTest objectAtIndex:2]];
            NSLog(@"ObjectAtIndex ar 28 : %@",ar);

            for (int j=0; j<ar.count; j++) {
                NSMutableArray *ar2 = [ar objectAtIndex:j];
                if ([[ar2 objectAtIndex:0] isEqualToString:@"01"]) {
                    aadharNo = [ar2 objectAtIndex:2];
                    [linkableTransaction setAadharNo:[ar2 objectAtIndex:2]];
                }
            }
            NSLog(@"28 aadharNo  : %@",aadharNo);
        }
    }
    return linkableTransaction;
}

- (void)recursive : (NSString*)tlvstr array:(NSMutableArray*)array{
    NSMutableArray *arr = [NSMutableArray array];
    NSMutableString *tlvStr = [NSMutableString stringWithFormat:@"%@",tlvstr];

    if (tlvStr.length==0) {

    }else{

        //  NSLog(@"TLV String length : %lu",(unsigned long)tlvStr.length);

        NSString *firstSection = [self splitString:0 index2:2 str:tlvStr];
        //  NSLog(@"TLV String firstSection : %@",firstSection);
        NSRange range = [tlvStr rangeOfString:firstSection];

        [arr addObject:firstSection];

        [tlvStr setString: [self removeRange:tlvStr range:&range]];

        NSString *secondSection = [self splitString:0 index2:2 str:tlvStr];
        //   NSLog(@"TLV String secondSection : %@",secondSection);

        range = [tlvStr rangeOfString:secondSection];

        if([secondSection hasPrefix:@"0"]){
            secondSection = [self splitString:1 index2:1 str:secondSection];
            [arr addObject:secondSection];

        }else{
            [arr addObject:secondSection];
        }

        [tlvStr setString: [self removeRange:tlvStr range:&range]];
        NSString *thirdSection = [self splitString:0 index2:[secondSection intValue] str:tlvStr];
        [arr addObject:thirdSection];

        //     NSLog(@"TLV String thirdSection : %@",thirdSection);

        range = [tlvStr rangeOfString:thirdSection];
        [tlvStr setString: [self removeRange:tlvStr range:&range]];
        //   NSLog(@"TLV tlvStr last  : %@",tlvStr);

        //  NSLog(@"TLV String arr : %@",arr);
        [array addObject:arr];

        if(tlvStr.length>0){
            [self recursive:tlvStr array:array];
        }
    }
}



-(NSString*)splitString :(int) index1 index2 :(int) index2 str : (NSString*)str{
    
    NSString *firstSection = [str substringWithRange:NSMakeRange(index1,index2)];
    //    NSLog(@"splitString firstSection : %@",firstSection);
    
    return firstSection;
}


-(NSString*)removeRange :(NSString*)tlvStr range:(NSRange*)range{
    
    NSArray *arr;
    NSString *newString = [tlvStr stringByReplacingCharactersInRange:*range withString:@""];
    // NSLog(@"TLV String newString1 : %@",newString);
    return newString;
    
}


@end
