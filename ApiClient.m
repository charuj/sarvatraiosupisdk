//
//  ApiClient.m
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "ApiClient.h"
#import "ApiProxyProtocol.h"
#import "DeviceIdViewController.h"
#import <CommonLibrary/CLServices.h>
#include <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonKeyDerivation.h>
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonHMAC.h>
#import <Security/Security.h>
#import "DeviceProfile.h"
#import "XMLReader.h"
#import "SrvtCommonLib.h"
#import <AFNetworking.h>
#import "TransactionParameters.h"
#import "SdkServiceFactory.h"
#import "CommonUtils.h"



@implementation ApiClient

@synthesize semaphore;

ApiProxyProtocol* apiProxy;
CommonUtils* commonUtils;


-(NSString*)getVersionOfSDK{

    NSString *version = @"1.0.11";
    return version;
}

-(void)activateSdk:(SdkClientApplication*)clientApplication withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion
{
    @try{
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc]init];
        __block NSString* deviceId = [deviceIdController generateUUID];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

        apiProxy = [ApiProxyProtocol sharedManager];

        //apiProxy.clientApplication = [SdkClientApplication alloc];

        //    apiProxy.clientApplication = [apiProxy.clientApplication initWithUserName:clientApplication.getUserName appName:clientApplication.getApplicationName instituteId:clientApplication.getInstitute channel:clientApplication.getChannel bankRefUrl:clientApplication.getBankRefUrl userMail:clientApplication.getUserEmail mobile:clientApplication.retrieveMobile userIdentity:clientApplication.getUserIdentity transactionID_Prefix:clientApplication.getTransactionIDPrefix];
        // apiProxy.clientApplication = [[SdkClientApplication alloc] init];

        apiProxy.clientApplication = clientApplication;
        apiProxy.channel = clientApplication.getChannel;
        apiProxy.mobileNo = clientApplication.retrieveMobile;
        apiProxy.appName = clientApplication.getApplicationName;
        apiProxy.transactionId = clientApplication.getTransactionIDPrefix;

        [apiProxy initSDKservice];
        //   BOOL isOnline = [self isOnline];

        //    if (isOnline) {
        //        NSLog(@"You are online...");
        //    }else{
        //        NSLog(@"You are offline...");
        //
        //    }

        NSString* mobile_no = [NSString stringWithFormat:@"%@%@",@"91",apiProxy.mobileNo];

        if (apiProxy.serviceConnected) {

            __block NSDictionary *params =@{@"name":clientApplication.getUserName,@"sdk-client-id":clientApplication.getUserIdentity,@"email":clientApplication.getUserEmail,@"sdk-client":clientApplication.getApplicationName,@"device-id":deviceId,@"mobile":apiProxy.mobileNo ,@"app-id":clientApplication.getApplicationName,@"channel-code":clientApplication.getChannel} ; // Add your parameters

            NSString* userId = nil;

            if ([prefs objectForKey:@"user_Id"])
                userId = (NSString *)[prefs objectForKey:@"user_Id"];

            if(userId!=nil){

                NSString* password = [self createSdkToken:clientApplication.getApplicationName userName:clientApplication.getUserName mobileNo:apiProxy.mobileNo deviceId:deviceId userIdentity:clientApplication.getUserIdentity];
                NSDictionary* commonParam = [self getCommonParameters];

                NSDictionary *parameters =@{@"device-id":[commonParam objectForKey:@"device-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"password":password ,@"app-id":clientApplication.getApplicationName,@"mobile": [commonParam objectForKey:@"mobile"] ,@"seq-no":[commonParam objectForKey:@"seq-no"]};

                NSLog(@"login parameters ==  %@",parameters); // here you get response once method camplete

                [apiProxy loginUser:parameters withCompletion:^(BOOL success, NSError *error, id response) {

                    if([[response objectForKey:@"response"] isEqualToString:@"0"])
                    {
                        // NSLog(@"login responce ==  %@",response); // here you get response once method camplete
                        apiProxy.userId = [response objectForKey:@"user-id"];
                        apiProxy.sessionId = [response objectForKey:@"session-id"];

                        [[NSUserDefaults standardUserDefaults] setObject:apiProxy.userId forKey:@"user_Id"];
                        [[NSUserDefaults standardUserDefaults] synchronize];

                        [apiProxy activateCommonLib:clientApplication.getApplicationName deviceId:deviceId channel:clientApplication.getChannel sessionid:apiProxy.sessionId userId:apiProxy.userId mobile:mobile_no seqNo:[self generateTransactionID]];

                        [apiProxy listAccountProviderForDeviceId:parameters userId:apiProxy.userId sessionId:apiProxy.sessionId withCompletion:^(BOOL success, NSError *error, id response) {

                            if(success)
                            {
                                NSLog(@"login responce ==  %@",response); // here you get response once method camplete
                                NSString* str = [response objectForKey:@"MobileAppData"];
                                // NSLog(@"str : MobileAppData %@", str);
                                NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

                                NSDictionary* providers = [json valueForKeyPath:@"details.providers"];
                                //       NSLog(@"dictData : details %@", providers);
                                apiProxy.listAccountProvider= providers;
                                [self getMappedVirtualAddresseslist];

                            }else if(error){
                                NSLog(@"listAccountProviderForDeviceId : error %@", error);

                            }
                        }];

                        [apiProxy listKeys:parameters sessionId:apiProxy.sessionId withCompletion:^(BOOL success, NSError *error, id response) {

                            if(success)
                            {
                                NSLog(@"listkeys ApiClient responce ==  %@",response); // here you get response once method camplete
                                NSString* str = [response objectForKey:@"MobileAppData"];

                                apiProxy.xmlPayload = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];

                                NSError *parseError = nil;
                                NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLString:str error:&parseError];
                                //                                NSString* keyXmlPayload = [NSString stringWithFormat:@"%@",xmlDictionary];
                                //                                keyXmlPayload = [self parseString:keyXmlPayload];
                                //                                NSLog(@"keyXmlPayload  %@",keyXmlPayload); // here you get response once method camplete

                                NSMutableDictionary* listkey = [self parseDict:xmlDictionary];
                                if ([listkey objectForKey:@"text"]) {
                                    [self replaceString:listkey];
                                }
                                apiProxy.listKeys = listkey;
                                NSLog(@" ----------------------------------------------------------  \n\n"); // here you get response once method camplete

                                NSLog(@"apiProxy.listKeys keys ==>>  %@",apiProxy.xmlPayload); // here you get response once method camplete


                            }else if(error){
                                NSLog(@"listkey : error %@", error);

                            }
                        }];

                        completion(YES,nil,response);

                    }else{
                        completion(NO,nil,response);
                    }
                }];

            }else{

                NSLog(@"activateSdk params ==  %@",params); // here you get response once method camplete

                [apiProxy activateSdk:params withCompletion:^(BOOL success, NSError *error, id response) {

                    if([[response objectForKey:@"response"] isEqualToString:@"0"])
                    {
                        // NSLog(@"activate user responce ==  %@",response); // here you get response once method camplete

                        apiProxy.userId = [response objectForKey:@"user-id"];
                        apiProxy.sessionId = [response objectForKey:@"session-id"];
                        [[NSUserDefaults standardUserDefaults] setObject:apiProxy.userId forKey:@"user_Id"];
                        [[NSUserDefaults standardUserDefaults] synchronize];

                        [apiProxy activateCommonLib:clientApplication.getApplicationName deviceId:deviceId channel:clientApplication.getChannel sessionid:apiProxy.sessionId userId:apiProxy.userId mobile:mobile_no seqNo:[self generateTransactionID]];


                        //  NSDictionary *dict =@{@"device-id":deviceId,@"user-id":apiProxy.userId,@"channel-code":channel ,@"app-id":applicationName};
                        NSString* password = [self createSdkToken:clientApplication.getApplicationName userName:clientApplication.getUserName mobileNo:apiProxy.mobileNo deviceId:deviceId userIdentity:clientApplication.getUserIdentity];

                        NSDictionary *dict =@{@"device-id":deviceId,@"user-id":apiProxy.userId,@"channel-code":clientApplication.getChannel,@"password":password ,@"app-id":clientApplication.getApplicationName,@"mobile": apiProxy.mobileNo ,@"seq-no":[self generateTransactionID]};

                        [apiProxy listAccountProviderForDeviceId:dict userId:apiProxy.userId sessionId:apiProxy.sessionId withCompletion:^(BOOL success, NSError *error, id response) {

                            if(success)
                            {
                                //  NSLog(@"login responce ==  %@",response); // here you get response once method camplete

                                NSString* str = [response objectForKey:@"MobileAppData"];
                                NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];


                                NSDictionary* providers = [json valueForKeyPath:@"details.providers"];

                                //   NSLog(@"dictData : details %@", providers);
                                apiProxy.listAccountProvider= providers;

                                [self getMappedVirtualAddresseslist];
                            }

                        }];
                        [apiProxy listKeys:dict sessionId:apiProxy.sessionId withCompletion:^(BOOL success, NSError *error, id response) {

                            if(success)
                            {
                                NSLog(@"listkeys ApiClient responce ==  %@",response); // here you get response once method camplete
                                NSString* str = [response objectForKey:@"MobileAppData"];
                                //   NSLog(@"str : MobileAppData %@", str);

                                apiProxy.xmlPayload = [str stringByReplacingOccurrencesOfString:@"\n" withString:@""];

                                NSError *parseError = nil;
                                NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLString:str error:&parseError];
                                //                        NSLog(@"xmlDictionary ==>>  %@",xmlDictionary); // here you get response once method camplete
                                //
                                //                        [self parseDict:xmlDictionary];

                                NSMutableDictionary* listkey = [self parseDict:xmlDictionary];
                                if ([listkey objectForKey:@"text"]) {
                                    [self replaceString:listkey];
                                }
                                apiProxy.listKeys = listkey;

                            }else if(error){
                                NSLog(@"listkey : error %@", error);
                            }
                        }];

                        completion(YES,nil,response);

                    }else{
                        completion(NO,nil,response);
                    }
                }];
            }

        }else{
            NSLog(@"Sdk activation failed..");
            // NSLog(@"No internet connection..");

        }
    }@catch(NSException *exception){
        NSLog(@"Activate SDK NSException : %@",exception);
    }

}


-(NSString*)parseString:(NSString*)xmlPayload{

    NSString* strReplace = [xmlPayload stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return strReplace;
}

-(NSMutableDictionary *)parseDict :(NSDictionary*)dictToParse{

    NSMutableDictionary* keysDict = [dictToParse objectForKey:@"ns2:RespListKeys"];
    NSMutableDictionary* dictOfKeys = [keysDict mutableCopy];
    //  NSLog(@"keysDict : %@ ", keysDict);

    for(id key in dictOfKeys){

        if([key isEqualToString:@"text"]){
            //   NSLog(@"keysDict text : %@ ", keysDict);
            [self replaceString:keysDict];

            //   [self replaceString:dictOfKeys];

        }else if([key isEqualToString:@"Signature"]){
            NSMutableDictionary* signature = [dictOfKeys objectForKey:key];
            NSMutableDictionary* signatureCopy = [signature mutableCopy];
            for(id keys in signatureCopy){

                if([keys isEqualToString:@"text"]){
                    [self replaceString:signature];
                }else if([keys isEqualToString:@"KeyInfo"]){

                    NSMutableDictionary* keyInfo = [signature objectForKey:keys];
                    NSMutableDictionary* inDictCopy = [keyInfo mutableCopy];

                    for(id sig_key in inDictCopy){
                        if([sig_key isEqualToString:@"text"]){

                            [self replaceString:keyInfo];

                        }else{

                            [self recursiveCall:keyInfo key:sig_key];
                        }
                    }

                }else if([keys isEqualToString:@"SignedInfo"]){
                    NSMutableDictionary* signedInfo = [signature objectForKey:keys];
                    NSMutableDictionary* inDictCopy = [signedInfo mutableCopy];
                    for(id sig_key in inDictCopy){
                        if([sig_key isEqualToString:@"text"]){
                            [self replaceString:signedInfo];
                        }else{

                            if([sig_key isEqualToString:@"CanonicalizationMethod"]){
                                NSMutableDictionary* canonicalizationMethod = [signedInfo objectForKey:sig_key];
                                [self replaceString:canonicalizationMethod];

                            }else if([sig_key isEqualToString:@"Reference"]){
                                NSMutableDictionary* Reference = [signedInfo objectForKey:sig_key];
                                NSMutableDictionary* inDictCopy = [signedInfo mutableCopy];

                                for(id sigkey in inDictCopy){
                                    if([sigkey isEqualToString:@"text"]){
                                        [self replaceString:Reference];

                                    }else if(![sigkey isEqualToString:@"URI"]){
                                        [self recursiveCall:signedInfo key:sigkey];
                                    }
                                }
                            }else if([sig_key isEqualToString:@"SignatureMethod"]){
                                NSMutableDictionary* signatureMethod = [signedInfo objectForKey:sig_key];
                                [self replaceString:signatureMethod];
                            }
                        }
                    }
                }else if([keys isEqualToString:@"SignatureValue"]){
                    NSMutableDictionary* signedInfo = [signature objectForKey:keys];

                    if([signedInfo objectForKey:@"text"]){
                        [self replaceString:signedInfo];
                    }
                }else{
                    NSMutableDictionary* inDict = [dictOfKeys objectForKey:keys];
                    NSMutableDictionary* inDictCopy = [inDict mutableCopy];
                    if (!([keys isEqualToString:@"xmlns:ns2"] || [keys isEqualToString:@"xmlns:ns3"] )) {

                        for(id dictKey in inDictCopy){
                            if([dictKey isEqualToString:@"text"]){
                                [self replaceString:inDict];
                            }
                        }
                    }
                }
            }

        }else if([key isEqualToString:@"keyList"]){
            NSMutableDictionary* keyList = [dictOfKeys objectForKey:key];
            NSMutableDictionary* keyListCopy = [keyList mutableCopy];
            for(id listKey in keyListCopy){
                if([listKey isEqualToString:@"text"]){
                    [self replaceString:keyList];
                }else if([listKey isEqualToString:@"key"]){
                    NSMutableDictionary* keydict = [keyListCopy objectForKey:listKey];
                    if([keydict objectForKey:@"text"]){
                        [self replaceString:keydict];
                    }
                    if([keydict objectForKey:@"keyValue"]){

                        NSMutableDictionary* keyvalue = [keydict objectForKey:@"keyValue"];
                        if([keyvalue objectForKey:@"text"]){
                            [self replaceString:keyvalue];
                        }
                    }
                }
            }
        }else{
            NSMutableDictionary* inDict = [dictOfKeys objectForKey:key];
            NSMutableDictionary* inDictCopy = [inDict mutableCopy];
            if (!([key isEqualToString:@"xmlns:ns2"] || [key isEqualToString:@"xmlns:ns3"] )) {
                for(id dictKey in inDictCopy){
                    if([dictKey isEqualToString:@"text"]){
                        [self replaceString:inDict];
                    }
                }
            }

        }
    }
    //   NSLog(@"After Replace \n   ==>>  %@ ",dictOfKeys); // here you get response once method camplete
    //    NSDictionary* keys = [xmlDictionary valueForKeyPath:@"ns2:RespListKeys.keyList.key"];
    //    NSLog(@"listkeys keys ==>>  %@",keys); // here you get response once method camplete
    //    apiProxy.listKeys = keys;
    return dictOfKeys;
}

-(void)recursiveCall :(NSDictionary*)dict key:(NSString*)key{

    NSMutableDictionary* keyInfo = [dict objectForKey:key];
    NSMutableDictionary* inDictCopy = [keyInfo mutableCopy];

    for(id sig_key in inDictCopy){
        if([sig_key isEqualToString:@"text"]){
            [self replaceString:keyInfo];
        }else if (![sig_key isEqualToString:@"Algorithm"] && ![sig_key isEqualToString:@"URI"]){
            [self recursiveCall:keyInfo key:sig_key];
        }
    }
}

-(void)replaceString : (NSMutableDictionary*)dict{

    if([dict objectForKey:@"text"]){

        NSString* strReplace = [dict objectForKey:@"text"];

        strReplace = [strReplace stringByReplacingOccurrencesOfString:@"\n" withString:@""];

        [dict setValue:strReplace forKey:@"text"];
    }

}



-(NSString*)createSdkToken :(NSString*)appName userName:(NSString*)userName mobileNo:(NSString*)mobileNo deviceId:(NSString*)deviceId userIdentity:(NSString*)userIdentity{

    NSString* secret = @"ilpaptml-b7ac-4976-a070-9883bc75ee42";

    NSString* message = [NSString stringWithFormat:@"%@%@-%@-%@%@",[self trimWhiteSpace:appName],[self trimWhiteSpace:userName],[self trimWhiteSpace:mobileNo],[self trimWhiteSpace:deviceId],[self trimWhiteSpace:userIdentity]];

    // NSLog(@"Message : %@",message);
    const char *cKey  = [secret cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [message cStringUsingEncoding:NSASCIIStringEncoding];
    unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    // NSLog(@"Hash : %@",[[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)]);

    NSData *hmacData = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];

    NSString *hmac = [hmacData base64EncodedStringWithOptions:0];

    return hmac;


}

-(NSDictionary*)getAccountProviderList{

    return apiProxy.listAccountProvider;

}
-(NSString*)trimWhiteSpace :(NSString *)strForTrim{

    // NSString *string = @" this text has spaces before and after ";
    NSString *trimmedString = [strForTrim stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];

    return trimmedString;
}


-(void)getCustomerAccounts:(AccountProvider*) provider withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    __block NSDictionary* accountArr;
    @try{

        commonUtils = [[CommonUtils alloc] init];
        [commonUtils notNull:provider name:@"AccountProvider"];


        if (apiProxy.serviceConnected ) {

            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString* userId = (NSString *)[prefs objectForKey:@"user_Id"];

            NSDictionary *commonParam = [self getCommonParameters];

            [apiProxy listAccount:provider commonParam:commonParam withCompletion:^(BOOL success, NSError *error, id response) {

                //        if(success){
                //
                //            NSLog(@"getCustomerAccounts + listAccount responce ==  %@",response);
                //            NSString* str = [response objectForKey:@"MobileAppData"];
                //
                //            str = [str stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                //            NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                //            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                //
                //            accountArr = [json valueForKeyPath:@"details.accounts"];
                //            apiProxy.customerAccount  = [apiProxy.customerAccount collectCustomerDetails:accountArr provider:provider];
                //         //   NSLog(@"getCustomerAccounts + details.accounts %@", accountArr);
                //
                ////            NSLog(@"getCustomerAccounts stringByReplacingOccurrencesOfString %@", str);
                //        }
                //        if(completion){
                //            dispatch_async(dispatch_get_main_queue(), ^{
                //                completion(YES,nil,accountArr); // here that call when method complete
                //            });
                //        }

                if([[response objectForKey:@"response"] isEqualToString:@"0"]){
                    NSLog(@"getCustomerAccounts + listAccount responce ==  %@",response);
                    NSString* str = [response objectForKey:@"MobileAppData"];

                    str = [str stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

                    NSDictionary* accountDict = [json valueForKeyPath:@"details.accounts"];
                    NSDictionary* account = @{@"account-provider":provider.getId ,@"account-provider-name":provider.getAccountProvider,@"iin":provider.getIin };

                    apiProxy.customerAccount  = [apiProxy.customerAccount collectCustomerDetails:accountArr provider:provider];

                    accountArr = @{@"customerDetails":accountDict ,@"accountProvider":account};

                    completion(YES,nil,accountArr);
                }else{
                    completion(NO,nil,response);
                }


            }];
        }else{
            NSLog(@"Sdk not connected ..");
            //  NSLog(@"No internet connection..");
        }
    }@catch(NSException* exception){
        NSLog(@"Get Customer Accounts NSException : %@",exception);
    }
}


-(void)checkVPA:(NSString*)vpa :(completion)completionBlock {

    NSDictionary* commonParam = [self getCommonParameters];

    @try{
        if (apiProxy.serviceConnected ) {

            [apiProxy checkVpa:vpa commonParam:commonParam withCompletion:^(BOOL success, NSError *error, id response) {

                if(success){

                    NSLog(@"checkVPA responce ==  %@",response);

                    if([[response objectForKey:@"response"] isEqualToString:@"0"]){

                        completionBlock(success,nil,response);

                    }else{
                        completionBlock(NO,nil,response);
                    }
                }else if(error){

                    NSLog(@"Error check Vpa ==  %@",error);
                    completionBlock(NO,nil,response);
                }

            }];

        }else{
            NSLog(@"Sdk not activated ..");
        }
    }@catch(NSException* exception){
        NSLog(@"Check VPA NSException : %@",exception);

    }

}


-(void)customerVPAList:(completion)completionBlock {

    NSDictionary* commonParam = [self getCommonParameters];
    NSString* url =@"accounts/list-va";

    if (apiProxy.serviceConnected ) {

        [apiProxy responseWithUrl:commonParam urlString:url withCompletion:^(BOOL success, NSError *error, id response) {

            if(success){

                NSLog(@"customerVPAList responce ==  %@",response);

                if([[response objectForKey:@"response"] isEqualToString:@"0"]){

                    completionBlock(success,nil,response);

                }else{
                    completionBlock(NO,nil,response);
                }

            }else if(error){

                NSLog(@"Error check Vpa ==  %@",error);
                completionBlock(NO,nil,response);

            }


        }];

    }else{
        NSLog(@"Sdk not activated ..");
    }

}



-(void)generateOTP :(NSString*)accountprovider ifsc:(NSString*)ifsc accountnumber:(NSString*)accountnumber withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    @try{

        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString* userId = (NSString *)[prefs objectForKey:@"user_Id"];
        NSDictionary* commonParam = [self getCommonParameters];

        if (apiProxy.serviceConnected ) {

            [apiProxy generateOTP:accountprovider commonParam:commonParam ifsc:ifsc accountnumber:accountnumber  withCompletion:^(BOOL success, NSError *error, id response) {

                if(success){

                    NSLog(@"generateOTP responce ==  %@",response);

                }else if(error){

                    NSLog(@"Error check Vpa ==  %@",error);


                }
            }];
        }else{
            NSLog(@"Sdk not activated ..");
            //  NSLog(@"No internet connection..");
        }

    }@catch(NSException* exception){
        NSLog(@"Get Customer Accounts NSException : %@",exception);
    }

}

-(void)miniStatement:(NSString*)fromDate toDate:(NSString*)toDate withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    @try{
        __block NSDictionary* stmtArr;
        NSDictionary* commonParam = [self getCommonParameters];

        NSDictionary *dict =@{@"session-id":[commonParam objectForKey:@"session-id"],@"mobile":[commonParam objectForKey:@"mobile"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"device-id":[commonParam objectForKey:@"device-id"],@"seq-no":[commonParam objectForKey:@"seq-no"],@"from-date":fromDate,@"to-date":toDate};

        if (apiProxy.serviceConnected) {

            [apiProxy miniStatement:dict withCompletion:^(BOOL success, NSError *error, id response) {
                NSLog(@" miniStatement pay responce ==  %@",response);

                if([[response objectForKey:@"response"] isEqualToString:@"0"]){
                    NSString* str = [response objectForKey:@"MobileAppData"];

                    str = [str stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

                    stmtArr = [json valueForKeyPath:@"details.transactions"];

                    completion(YES,nil,stmtArr); // here that call when method complete

                }else {
                    NSLog(@"Error  miniStatement ==  %@",error);
                    completion(NO,nil,response); // here that call when method complete

                }

            }];
        }else{
            NSLog(@"Sdk not activated ..");
        }
    }@catch(NSException* exception){
        NSLog(@"Mini Statement NSException : %@",exception);
    }
}

-(void)pay:(TransactionParameters*)transParam view:(UIViewController*)view withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{


    @try{
        NSDictionary *dict ;
        NSDictionary *otherParam ;
        [transParam validate:[transParam getParameterMode]];

        NSString* transId = [self generateTransactionID];

        commonUtils = [[CommonUtils alloc] init];

        if (apiProxy.serviceConnected) {

            [commonUtils notNull:transParam name:@"TransactionParameters"];
            NSString* payeeMode ;

            NSString* code = @"NPCI";
            NSString* ki = @"20150822";
            SrvtCommonLib *srvt = [[SrvtCommonLib alloc]init];
            NSArray *payInfo  = [NSArray array];
            NSDictionary* salt;
            NSDictionary *commonParam = [self getCommonParameters];
            NSString* mobile_no = [NSString stringWithFormat:@"%@%@",@"91",apiProxy.mobileNo];


            NSString* amount = [NSString stringWithFormat:@"%.2f",[transParam.amount doubleValue]];
            // NSLog(@"Amount pay : %@",amount);


            if (transParam.payeeMode == 1 ) {
                NSLog(@"TransactionParameters payeeMode :%u ",VPA);
                dict = @{@"payee-va":transParam.payeeParam.getVpa};
                payeeMode = @"VPA";

                salt = @{@"txnId" :transId,@"appId":apiProxy.appName , @"deviceId":[commonParam objectForKey:@"device-id"],@"mobileNumber":mobile_no,@"txnAmount":amount,@"payerAddr":transParam.payerParam.vr.getHandle ,@"payeeAddr":transParam.payeeParam.getVpa};

                payInfo = @[@{@"name": @"note",     @"value": transParam.remark },
                            @{@"name": @"refId",    @"value": transId },
                            @{@"name": @"refUrl",   @"value": apiProxy.clientApplication.getBankRefUrl },
                            @{@"name": @"account",   @"value": transParam.customerAccount.getMaskedAccount }];

                NSLog(@"Common Lib salt :%@ ",salt);


            }else if (transParam.payeeMode ==2){
                NSLog(@"TransactionParameters payeeMode :%u ",Account);
                payeeMode = @"Account";

                dict =@{@"mcc":@"0000",@"merchant-type":@"PERSON",@"payee-name":transParam.payeeParam.payeeName,@"global-address-type":@"ACCOUNTIFSC",@"payee-account":transParam.payeeParam.account,@"payee-ifsc":transParam.payeeParam.ifsc};

                salt = @{@"txnId" :transId,@"appId":apiProxy.appName , @"deviceId":[commonParam objectForKey:@"device-id"],@"mobileNumber":mobile_no,@"txnAmount":amount,@"payerAddr":transParam.payerParam.vr.getHandle ,@"payeeAddr":transParam.payeeParam.getVpa};

                payInfo = @[@{@"name": @"payeeName",@"value": transParam.payeeParam.payeeName},
                            @{@"name": @"note",     @"value": transParam.remark },
                            @{@"name": @"refId",    @"value": transId },
                            @{@"name": @"refUrl",   @"value": apiProxy.clientApplication.getBankRefUrl },
                            @{@"name": @"account",   @"value": transParam.customerAccount.getMaskedAccount }];
                NSLog(@"Common Lib salt :%@ ",salt);


            }else if(transParam.payeeMode ==3){
                NSLog(@"TransactionParameters payeeMode :%u ",Mobile);
                NSString* mmid = [NSString stringWithFormat:@"%d",transParam.payeeParam.getMmid];
                NSString* mobile = [NSString stringWithFormat:@"%@%@",@"91",transParam.payeeParam.getVpa];
                dict =@{@"mcc":@"0000",@"merchant-type":@"PERSON",@"payee-name":transParam.payeeParam.payeeName,@"global-address-type":@"MOBILEMMID",@"payee-mobile":transParam.payeeParam.getMobile,@"mmid":mmid};
                payeeMode = @"Mobile";

                salt = @{@"txnId" :transId,@"appId":apiProxy.appName , @"deviceId":[commonParam objectForKey:@"device-id"],@"mobileNumber":mobile_no,@"txnAmount":amount,@"payerAddr":transParam.payerParam.vr.getHandle ,@"payeeAddr":mobile};
                payInfo = @[@{@"name": @"payeeName",@"value": transParam.payeeParam.payeeName},
                            @{@"name": @"note",     @"value": transParam.remark },
                            @{@"name": @"refId",    @"value": transId },
                            @{@"name": @"refUrl",   @"value": apiProxy.clientApplication.getBankRefUrl },
                            @{@"name": @"account",   @"value": transParam.customerAccount.getMaskedAccount }];

                NSLog(@"Common Lib salt :%@ ",salt);

            }else if(transParam.payeeMode ==4){
                NSLog(@"TransactionParameters payeeMode :%u",AADHAAR_IIN);
                payeeMode = @"AADHAAR_IIN";
                dict =@{@"mcc":@"0000",@"merchant-type":@"PERSON",@"payee-name":transParam.payeeParam.payeeName,@"global-address-type":@"AADHAR",@"payee-aadhar":transParam.payeeParam.aadhar,@"payee-iin":transParam.payeeParam.getAccountProvider.getIin};

                salt = @{@"txnId" :transId,@"appId":apiProxy.appName , @"deviceId":[commonParam objectForKey:@"device-id"],@"mobileNumber":mobile_no,@"txnAmount":amount,@"payerAddr":transParam.payerParam.vr.getHandle ,@"payeeAddr":transParam.payeeParam.getVpa};

                payInfo = @[@{@"name": @"payeeName",@"value": transParam.payeeParam.payeeName},
                            @{@"name": @"note",     @"value": transParam.remark },
                            @{@"name": @"refId",    @"value": transId },
                            @{@"name": @"refUrl",   @"value": apiProxy.clientApplication.getBankRefUrl },
                            @{@"name": @"account",   @"value": transParam.customerAccount.getMaskedAccount }];
                NSLog(@"Common Lib salt :%@ ",salt);

            }

            NSString* defaultAcc = ([transParam isUsingDefaultAccount] ? @"D" : @"N");


            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

            NSString* token = (NSString *)[prefs objectForKey:@"token"];

            NSDictionary* config = @{@"payerBankName":transParam.customerAccount.provider.getAccountProvider};


            __block NSString* mpinParam;

            NSDictionary* credA = transParam.customerAccount.getCredArr;
            //NSLog(@"credA Pay : %@",credA);

            NSMutableArray * key = [credA objectForKey:@"CredAllowed"];

            NSMutableString* strCred = [NSMutableString stringWithFormat:@"%@",@"{\"CredAllowed\":["];


            for(int i=0 ; i<key.count ; i++){
                NSDictionary* ar = [key objectAtIndex:i];
                //   NSLog(@"key Pay : %d =  %@",i,[key objectAtIndex:i]);
                if ([[ar objectForKey:@"subtype"] isEqualToString:@"MPIN"]) {
                    strCred = [strCred stringByAppendingFormat:@"{\"type\":\"%@\",\"subtype\":\"%@\",\"dType\":\"%@\",\"dLength\":%@ }]}",[ar objectForKey:@"type"],[ar objectForKey:@"subtype"],[ar objectForKey:@"dType"],[ar objectForKey:@"dLength"]];
                }
            }

            NSDictionary* finalCred = [commonUtils createDict:strCred];

            if (!transParam.isUsingDefaultAccount) {

                //   NSLog(@"In !transParam.isUsingDefaultAccount");

                otherParam = @{@"amount":amount , @"note":transParam.remark,@"pre-approved":@"M",@"use-default-acc":defaultAcc,@"default-credit": @"N",@"account-type":transParam.customerAccount.getAccountType,@"mode":payeeMode , @"account-provider":transParam.customerAccount.provider.getId,@"payer-va":transParam.payerParam.vr.getHandle,@"use-default-acc":defaultAcc,@"seq-no":transId,@"ifsc":transParam.customerAccount.getIfsc,@"account-number":transParam.customerAccount.getAccount,@"payee-va":transParam.payeeParam.getVpa};

            }else{

                //  NSLog(@"In !transParam.isUsingDefaultAccount ELSE");

                otherParam = @{@"amount":amount , @"note":transParam.remark,@"pre-approved":@"M",@"use-default-acc":defaultAcc,@"default-credit": @"N",@"account-type":transParam.customerAccount.getAccountType,@"mode":payeeMode , @"account-provider":transParam.customerAccount.provider.getId,@"payer-va":transParam.payerParam.vr.getHandle,@"use-default-acc":defaultAcc,@"seq-no":transId,@"payee-va":transParam.payeeParam.getVpa};
            }


            [srvt getCredential:@"NPCI" keyXMLPayload:apiProxy.xmlPayload controls:finalCred configuration:config salt:salt payInfo:payInfo language:@"en_US" token:token view:view withCompletion:^(BOOL success, NSError *error, id response) {

                NSLog(@" ApiClient getCredential output ==  %@",response);

                NSDictionary* comResponse = [response objectForKey:@"credBlocks"];
                //                NSLog(@"  comResponse  ==  %@",comResponse);
            if(success && [[response objectForKey:@"resultCode"] isEqualToString:@"1"]){

                if([comResponse objectForKey:@"MPIN"]){
                    NSLog(@" MPIN ");
                    NSDictionary* data1 = [comResponse valueForKey:@"MPIN"];
                    NSDictionary* data2 = [data1 valueForKey:@"data"];
                    mpinParam = [NSString stringWithFormat:@"%@,%@,%@",code,ki,[data2 objectForKey:@"encryptedBase64String"]] ;
                    NSLog(@" Pay mpinParam  ==  %@",mpinParam);

                }

                [apiProxy pay:dict otherParam:otherParam commonParam:commonParam mpin:mpinParam withCompletion:^(BOOL success, NSError *error, id response) {

                    if([[response objectForKey:@"response"] isEqualToString:@"0"]){
                        NSLog(@" ApiClient pay responce ==  %@",response);

                        completion(YES,nil,response); // here that call when method complete

                    }else {
                        NSLog(@"Error  pay ==  %@",error);
                        completion(NO,nil,response); // here that call when method complete

                    }
                }];

            }else{
                NSLog(@"Error  Pay =  %@",error);
                NSLog(@"Response  Pay =  %@",response);

            }

            }];

        }else{
            NSLog(@"Sdk not connected..");
        }
    }@catch(NSException *exception){
        NSLog(@"Pay NSException : %@",exception);
    }

}

/**
 * Get the Mapped Virtual Addresses of the Customer.
 *
 * return List of the virtual Addresses
 */

-(void)getMappedVirtualAddresseslist{

    __block NSArray* mapped_accounts;
    @try{
        if (apiProxy.serviceConnected) {

            NSDictionary *dict = [self getCommonParameters];

            [apiProxy listMappedVirtualAddresses:dict withCompletion:^(BOOL success, NSError *error, id response) {

                if(success){

                    NSLog(@"getMappedVirtualAddresses responce ==  %@",response);

                    NSString* str = [response objectForKey:@"MobileAppData"];

                    // str = [str stringByReplacingOccurrencesOfString:@"\" withString:@""];
                    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

                    mapped_accounts  = [json valueForKeyPath:@"details.accounts"];
                    apiProxy.listMappedVirtualId = mapped_accounts;



                }else if(error){

                    NSLog(@"Error check Vpa ==  %@",error);
                }
            }];

        }else{
            NSLog(@"Sdk not connected..");
        }
    }@catch(NSException *exception){
        NSLog(@"Mapped Virtual Address list NSException : %@",exception);
    }
}

-(void)validatePayeeVirtualAddress:(NSString*)virtualAddr withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    NSDictionary* commonParam = [self getCommonParameters];

    NSDictionary *dict =@{@"virtual-address":virtualAddr,@"payee-name":@"ABC"};
    @try{
        if (apiProxy.serviceConnected) {

            [apiProxy validateVirtualAddresses:dict commonParam:(NSDictionary*)commonParam withCompletion:^(BOOL success, NSError *error, id response) {

                NSLog(@"validateAddress responce ==  %@",response);

                if([[response objectForKey:@"response"] isEqualToString:@"0"]){

                    NSString* name = [response objectForKey:@"MobileAppData"];
                    NSArray *splitResponse = [name componentsSeparatedByString:@"="];
                    name = [splitResponse lastObject];
                    // NSLog(@"validateAddress name ==  %@",name);
                    completion(YES,nil,name); // here that call when method complete

                }else{

                    NSString* message = [response objectForKey:@"message"];
                    completion(NO,nil,message); // here that call when method complete
                }

            }];

        }else{

            NSLog(@"Sdk not connected..");
        }
    }@catch(NSException *exception){
        NSLog(@"Validate Payee Virtual Address NSException : %@",exception);

    }
}

-(void)listKeys{

    if (apiProxy.serviceConnected) {

        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString* userId = (NSString *)[prefs objectForKey:@"user_Id"];

        //  NSDictionary *dict =@{@"session-id":apiProxy.sessionId,@"user-id":userId,@"channel-code":apiProxy.channel};

        NSDictionary *dict = [self getCommonParameters];

        [apiProxy listKeys:dict sessionId:apiProxy.sessionId withCompletion:^(BOOL success, NSError *error, id response) {

            if(success){

                NSLog(@"listKeys responce ==  %@",response);

            }else if(error){

                NSLog(@"Error  listKeys ==  %@",error);


            }
        }];
    }else{
        NSLog(@"Sdk not connected..");
    }
}


-(void)mapAccount:(CardData *)card vpa:(NSString*)vpa custAccount:(CustomerAccount *)custAccount accountDefaultStatus:(AccountDefaultStatus *)status view:(UIViewController*)view  withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    __block NSDictionary* inputDict;
    __block NSDictionary* output;

    @try{

        CommonUtils *commonUtils = [[CommonUtils alloc] init];
        [commonUtils notNull:card name:@"Card Data"];
        [commonUtils notNull:custAccount name:@"Customer Account"];
        [commonUtils notNull:status name:@"Account Default Status"];
        [commonUtils notNull:vpa name:@"VPA"];

        if (apiProxy.serviceConnected) {


            DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc]init];
            __block NSString* deviceId = [deviceIdController generateUUID];

            NSDictionary* commonParam = [self getCommonParameters];

            NSString* defaultDebit;
            NSString* defaultCredit;

            NSString* actionFlag = @"R";

            //    if (regVpa.mode == Register) {
            //        actionFlag = @"R";
            //
            //    }else{
            //        actionFlag = @"P";
            //
            //    }

            if (status.defaultDebit) {
                defaultDebit = @"D";
            }else{
                defaultDebit = @"N";
            }

            if (status.defaultCredit) {
                defaultCredit = @"D";
            }else{
                defaultCredit = @"N";
            }

            NSString* code = @"NPCI";
            NSString* ki = @"20150822";
            NSString* result = @"1";

            //    NSString* code = [apiProxy.listKeys objectForKey:@"code"];
            //    NSString* ki = [apiProxy.listKeys objectForKey:@"ki"];

            //NSLog(@" ApiClient registerMobileNo listkeys ==  %@",apiProxy.listKeys);

            NSString* expeiryDate = [NSString stringWithFormat:@"%@%@",card.expiryMonth,card.expiryYear];

            SrvtCommonLib *srvt = [[SrvtCommonLib alloc]init];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString* token = (NSString *)[prefs objectForKey:@"token"];

            NSDictionary* config = @{@"payerBankName":custAccount.provider.getAccountProvider , @"resendOTPFeature":@"true"};
            NSString* transId = [self generateTransactionID];
            NSString* mobile_no = [NSString stringWithFormat:@"%@%@",@"91",apiProxy.mobileNo];

            NSDictionary* salt = @{@"txnId" :transId,@"appId":apiProxy.appName , @"deviceId":deviceId,@"mobileNumber":mobile_no};

            NSArray *payInfo  = [NSArray array];
            payInfo = @[@{@"name": @"payeeName",@"value": custAccount.getName},
                        @{@"name": @"refId",    @"value": transId },
                        @{@"name": @"refUrl",   @"value": apiProxy.clientApplication.getBankRefUrl },
                        @{@"name": @"account",   @"value": custAccount.getMaskedAccount }];

            NSString* userId = (NSString *)[prefs objectForKey:@"user_Id"];
            __block NSString* mpinParam = @"";
            __block NSString* otpParam = @"";
            __block NSString* atmParam = @"";


            [apiProxy generateOTP:custAccount.provider.getId commonParam:commonParam ifsc:custAccount.getIfsc accountnumber:custAccount.getMaskedAccount  withCompletion:^(BOOL success, NSError *error, id response) {

                if(success){

                    NSLog(@"generateOTP responce ==  %@",response);

                    [srvt getCredential:@"NPCI" keyXMLPayload:apiProxy.xmlPayload controls:custAccount.getCredArr configuration:config salt:salt payInfo:payInfo language:@"en_US" token:token view:view withCompletion:^(BOOL success, NSError *error, id response) {

                        NSLog(@" ApiClient getCredential output response ==  %@",response);
                        NSLog(@"You got: %@",success ? @"getCredential YES" : @"getCredential NO");

                        if(success && [[response objectForKey:@"resultCode"] isEqualToString:@"1"]){

                            //                    NSLog(@"You got: %@",success ? @"getCredential YES" : @"getCredential NO");

                            NSDictionary* comResponse = [response objectForKey:@"credBlocks"];
                            NSString* resultCode = [response objectForKey:@"resultCode"];

                            //                NSLog(@"  comResponse  ==  %@",comResponse);

                            if([comResponse objectForKey:@"MPIN"]){
                                NSDictionary* data1 = [comResponse valueForKey:@"MPIN"];
                                NSDictionary* data2 = [data1 valueForKey:@"data"];
                                mpinParam = [NSString stringWithFormat:@"%@,%@,%@",code,ki,[data2 objectForKey:@"encryptedBase64String"]] ;
                                //                    NSLog(@"  mpinParam  ==  %@",mpinParam);

                            }
                            if([comResponse objectForKey:@"SMS"]){
                                NSDictionary* data1 = [comResponse valueForKey:@"SMS"];
                                NSDictionary* data2 = [data1 valueForKey:@"data"];
                                otpParam = [NSString stringWithFormat:@"%@,%@,%@",code,ki,[data2 objectForKey:@"encryptedBase64String"]] ;
                            }
                            if([comResponse objectForKey:@"ATM"]){
                                NSDictionary* data1 = [comResponse valueForKey:@"ATM"];
                                NSDictionary* data2 = [data1 valueForKey:@"data"];
                                atmParam = [NSString stringWithFormat:@"%@,%@,%@",code,ki,[data2 objectForKey:@"encryptedBase64String"]] ;
                            }

                            inputDict = @{@"session-id":apiProxy.sessionId,@"user-id":userId,@"channel-code":apiProxy.channel,@"action-flag":actionFlag,@"card-digits":card.getLastSix,@"expiry-date":expeiryDate , @"mpin":mpinParam ,@"account-provider":custAccount.provider.getId,@"account-number":custAccount.getMaskedAccount,@"account-type":custAccount.getAccountType,@"default-debit":defaultDebit,@"default-credit":defaultCredit,@"ifsc":custAccount.getIfsc,@"otp":otpParam,@"name":custAccount.getName,@"credsAllowedJson":custAccount.getCredentialRequired,@"virtual-address":vpa,@"mmid":custAccount.getMmid,@"mobile":apiProxy.mobileNo,@"seq-no":transId };


                            [apiProxy registerMobile:inputDict withCompletion:^(BOOL success, NSError *error, id response) {
                                NSLog(@" registerMobile in apiClient ==  %@",response);

                                if([[response objectForKey:@"response"] isEqualToString:@"0"]){
                                    NSLog(@" ApiClient registerMobileNo responce ==  %@",response);

                                    completion(YES,nil,response); // here that call when method complete

                                }else {
                                    completion(NO,error,response); // here that call when method complete

                                }
                            }];
                        }else if([[response objectForKey:@"resultCode"] isEqualToString:@"2"]){
                            NSLog(@" resultCode ==  %@",[response objectForKey:@"resultCode"]);


                            [apiProxy generateOTP:custAccount.provider.getId commonParam:commonParam ifsc:custAccount.getIfsc accountnumber:custAccount.getMaskedAccount  withCompletion:^(BOOL success, NSError *error, id response) {
                                NSLog(@" resend otp ==  %@",response);
                            }];

                        }else if(error){
                            NSLog(@"Error  registerMobileNo ==  %@",error);
                        }

                    }];
                }else if(error){
                    NSLog(@"Error mapAccount  ==  %@",error);
                }
            }];

        }else{
            NSLog(@"Sdk not connected..");
        }
    }@catch (NSException *exception) {
        NSLog(@"NSException : %@",exception);
    }

}


-(void)mapAccount :(SdkClientApplication *)clientApp vpa:(NSString*)vpa custAccount:(CustomerAccount *)custAccount  accountDefaultStatus:(AccountDefaultStatus *)status withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    __block NSDictionary* inputDict;

    NSString* defaultDebit;
    NSString* defaultCredit;
    @try{
        if (apiProxy.serviceConnected) {


            if (status.defaultDebit) {
                defaultDebit = @"D";
            }else{
                defaultDebit = @"N";
            }

            if (status.defaultCredit) {
                defaultCredit = @"D";
            }else{
                defaultCredit = @"N";
            }

            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString* userId = (NSString *)[prefs objectForKey:@"user_Id"];

            inputDict = @{@"session-id":apiProxy.sessionId,@"user-id":userId,@"channel-code":apiProxy.channel,@"account-provider":custAccount.provider.getId,@"account-number":custAccount.getMaskedAccount,@"account-type":custAccount.getAccountType,@"default-debit":defaultDebit,@"default-credit":defaultCredit,@"ifsc":custAccount.getIfsc,@"name":custAccount.getName,@"credsAllowedJson":custAccount.getCredentialRequired,@"virtual-address":vpa,@"mmid":custAccount.getMmid,@"mobile":apiProxy.mobileNo,@"seq-no":[self generateTransactionID]};


            [apiProxy storeVPA:inputDict withCompletion:^(BOOL success, NSError *error, id response) {
                if([[response objectForKey:@"response"] isEqualToString:@"0"]){
                    NSLog(@" ApiClient store account responce ==  %@",response);
                    completion(YES,nil,response); // here that call when method complete

                }else{
                    NSLog(@"Error  store account ==  %@",error);
                    completion(NO,nil,response); // here that call when method complete

                }
            }];

        }else{
            NSLog(@"Sdk not connected..");
        }
    }@catch(NSException * exception){
        NSLog(@"NSException :%@",exception);

    }
}


-(void)removeVPA :(NSString*)vpa reason:(NSString*)reason withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    @try{

        if(apiProxy.serviceConnected){

            NSDictionary* commonParams = [self getCommonParameters];

            [apiProxy removeVPA:vpa reason:reason params:commonParams  withCompletion:^(BOOL success, NSError *error, id response) {

                if([[response objectForKey:@"response"] isEqualToString:@"0"]){
                    NSLog(@" removeVPA  responce ==  %@",response);
                    completion(YES,nil,response); // here that call when method complete

                }else{
                    NSLog(@"Error  removeVPA  ==  %@",error);
                    completion(NO,nil,response); // here that call when method complete

                }
            }];

        }else{
            NSLog(@"Sdk not connected..");
        }
    }@catch(NSException * exception){
        NSLog(@"NSException :%@",exception);

    }
}

-(void)deRegister :(NSString*)reason withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    @try{

        if(apiProxy.serviceConnected){
            NSDictionary* commonParams = [self getCommonParameters];

            [apiProxy deregister:reason params:commonParams  withCompletion:^(BOOL success, NSError *error, id response) {

                if([[response objectForKey:@"response"] isEqualToString:@"0"]){
                    NSLog(@" deRegister  responce ==  %@",response);
                    completion(YES,nil,response); // here that call when method complete

                }else{
                    NSLog(@"Error  deRegister  ==  %@",error);
                    completion(NO,nil,response); // here that call when method complete

                }
            }];
        }else{
            NSLog(@"Sdk not connected..");
        }
    }@catch(NSException * exception){
        NSLog(@"deRegister NSException :%@",exception);
    }
}


-(void)getPendingTransactionAsPayee :(completion) completionBlock{

    __block NSDictionary*output ;
    __block NSMutableArray *pending = [NSMutableArray new];

    @try{
        if(apiProxy.serviceConnected){
            NSDictionary* commonParam = [self getCommonParameters];
            NSString* transcMode = @"INITIATED";

            [apiProxy getPendingTransaction:commonParam withCompletion:^(BOOL success, NSError *error, id response){
                if([[response objectForKey:@"response"] isEqualToString:@"0"]){
                    NSLog(@" getPendingTransactionAsPayee  responce ==  %@",response);

                    NSString* str = [response objectForKey:@"MobileAppData"];
                    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

                    output  = [json valueForKeyPath:@"details.pending"];

                    //                    NSLog(@"output  getPendingTransactionForApproval  ==  %@",output);
                    //                    NSLog(@"output count  getPendingTransactionForApproval  ==  %u",(unsigned)output.count);

                    if ([output isKindOfClass:[NSString class]]) {
                    NSLog(@"output  isKindOfClass:[NSString class]  ");

                        [pending addObject:output];

                    }else if ([output isKindOfClass:[NSDictionary class]]) {
                        NSLog(@"output  isKindOfClass:[NSDictionary class]  ");

                        if ([[output objectForKey:@"direction"] isEqualToString:transcMode]) {

                            [pending addObject:output];
                        }
                    }else if([output isKindOfClass:[NSArray class]]){
                        NSLog(@"output  isKindOfClass:[NSArray class]  ");

                        for(id key in output){

                            if ([[key objectForKey:@"direction"] isEqualToString:transcMode]) {

                                [pending addObject:key];
                            }
                        }
                    }

                    completionBlock(YES,nil,pending); // here that call when method complete

                }else{
                    NSLog(@"Error  getPendingTransactionAsPayee  ==  %@",error);
                    completionBlock(NO,nil,response); // here that call when method complete
                }
            }];
        }else{
            NSLog(@"Sdk not connected..");
        }
    }@catch(NSException * exception){
        NSLog(@"getPendingTransactionAsPayee NSException :%@",exception);
    }

}

-(void)getPendingTransactionForApproval :(completion) completionBlock{

    __block NSDictionary*output ;
    __block NSMutableArray *pending = [NSMutableArray new];

    @try {

        if(apiProxy.serviceConnected){
            NSDictionary* commonParam = [self getCommonParameters];
            NSString* transcMode = @"RECEIVED";

            [apiProxy getPendingTransaction:commonParam withCompletion:^(BOOL success, NSError *error, id response){
                NSLog(@" getPendingTransactionForApproval  responce ==  %@",response);

                if([[response objectForKey:@"response"] isEqualToString:@"0"] ){

                    NSString* str = [response objectForKey:@"MobileAppData"];

                    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

                    output  = [json valueForKeyPath:@"details.pending"];

                    NSLog(@"output  getPendingTransactionForApproval  ==  %@",output);
                    NSLog(@"output count  getPendingTransactionForApproval  ==  %u",(unsigned)output.count);

                    if ([output isKindOfClass:[NSString class]]) {
                        NSLog(@"output  isKindOfClass:[NSString class]  ");

                            [pending addObject:output];

                    }else if ([output isKindOfClass:[NSDictionary class]]) {
                        NSLog(@"output  isKindOfClass:[NSDictionary class]  ");

                        if ([[output objectForKey:@"direction"] isEqualToString:transcMode]) {

                            [pending addObject:output];
                        }
                    }else if([output isKindOfClass:[NSArray class]]){
                        NSLog(@"output  isKindOfClass:[NSArray class]  ");

                        for(id key in output){

                            if ([[key objectForKey:@"direction"] isEqualToString:transcMode]) {

                                [pending addObject:key];
                            }
                        }
                    }

                    completionBlock(YES,nil,pending); // here that call when method complete
                }else{
                    NSLog(@"Error  getPendingTransactionForApproval  ==  %@",error);
                    completionBlock(NO,nil,response); // here that call when method complete

                }
            }];


        }else{
            NSLog(@"Sdk not connected..");
        }

    } @catch (NSException *exception) {
        NSLog(@"Get Pending Transaction For Approval NSException : %@",exception);
    }


}


-(NSDictionary*)getCommonParameters{

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* userId = (NSString *)[prefs objectForKey:@"user_Id"];

    DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc]init];
    NSString* deviceId = [deviceIdController generateUUID];

    NSDictionary* commonParam = @{@"session-id":apiProxy.sessionId,@"user-id":userId,@"device-id":deviceId,@"channel-code":apiProxy.channel,@"mobile": apiProxy.mobileNo , @"seq-no":[self generateTransactionID]};

    return commonParam;
}


-(void) getMappedVirtualAddresses:(completion) completionBlock{


    __block NSArray* mapped_accounts;

    @try{
        if(apiProxy.serviceConnected){

            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];

            NSDictionary *dict = [self getCommonParameters];

            [apiProxy listMappedVirtualAddresses:dict withCompletion:^(BOOL success, NSError *error, id response) {

                if(success){

                    NSLog(@"getMappedVirtualAddresses responce ==  %@",response);

                    NSString* str = [response objectForKey:@"MobileAppData"];


                    // str = [str stringByReplacingOccurrencesOfString:@"\" withString:@""];
                    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

                    mapped_accounts  = [json valueForKeyPath:@"details.accounts"];
                    apiProxy.listMappedVirtualId = mapped_accounts;


                    completionBlock(YES,nil,mapped_accounts);


                }else if(error){

                    NSLog(@"Error getMappedVirtualAddresses ==  %@",error);
                    completionBlock(YES,nil,error);

                }
            }];

            //completionBlock(NO,nil,mapped_accounts);
        }else{
            NSLog(@"Sdk not connected..");
        }
    }@catch(NSException * exception){
        NSLog(@"get Mapped Virtual Addresses NSException : %@",exception);

    }
}


-(NSString *) generateTransactionID {

    //    DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc]init];
    //    __block NSString* deviceId = [deviceIdController generateUUID];
    //
    //    NSString *letters = [deviceId stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    NSMutableString *randomString = [NSMutableString stringWithCapacity: 32];

    for (int i=0; i<[letters length]; i++) {

        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }

    NSString* randomId = apiProxy.clientApplication.getTransactionIDPrefix;

    randomId = [randomId stringByAppendingString:randomString];

    if([randomId length]>35){
        randomId = [randomId substringToIndex:35];
    }else if([randomId length]<35){
        randomId = [randomId stringByPaddingToLength:35 withString:@"a" startingAtIndex:0];
    }

    //   NSLog(@"RandomId : %@",randomId);
    return randomId;
}


-(void)changeMPIN :(VirtualAddress*)virtualaddress custAccount:(CustomerAccount*)custAccount view:(UIViewController*)view :(completion) completionBlock{

    CommonUtils* commonUtils = [[CommonUtils alloc] init];
    @try{
        if(apiProxy.serviceConnected){

            [commonUtils notNull:virtualaddress name:@"VirtualAddress"];
            [commonUtils notNull:custAccount name:@"CustomerAccount"];

            __block NSDictionary* inputDict;

            NSString* code = @"NPCI";
            NSString* ki = @"20150822";

            SrvtCommonLib *srvt = [[SrvtCommonLib alloc]init];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString* token = (NSString *)[prefs objectForKey:@"token"];

            NSDictionary* config = @{@"payerBankName":custAccount.provider.getAccountProvider};
            NSString* transId = [self generateTransactionID ];
            NSString* mobile_no = [NSString stringWithFormat:@"%@%@",@"91",apiProxy.mobileNo];
            NSDictionary* commonParams = [self getCommonParameters];

            NSDictionary* salt = @{@"txnId" :transId,@"appId":apiProxy.appName , @"deviceId":[commonParams objectForKey:@"device-id"],@"mobileNumber":mobile_no};

            NSArray *payInfo  = [NSArray array];
            payInfo = @[@{@"name": @"refId",    @"value": transId },
                        @{@"name": @"refUrl",   @"value": apiProxy.clientApplication.getBankRefUrl },
                        @{@"name": @"account",   @"value": custAccount.getMaskedAccount  }];

            NSDictionary* credA = custAccount.getCredArr;

            NSMutableArray * key = [credA objectForKey:@"CredAllowed"];

            NSMutableString* strCred = [NSMutableString stringWithFormat:@"%@",@"{\"CredAllowed\":["];


            for(int i=0 ; i<key.count ; i++){
                NSDictionary* ar = [key objectAtIndex:i];
                //  NSLog(@"key Pay : %d =  %@",i,[key objectAtIndex:i]);
                if ([[ar objectForKey:@"subtype"] isEqualToString:@"MPIN"]) {
                    strCred = [strCred stringByAppendingFormat:@"{\"type\":\"%@\",\"subtype\":\"%@\",\"dType\":\"%@\",\"dLength\":%@ },{\"type\":\"PIN\",\"subtype\": \"NMPIN\", \"dType\": \"NUM\",\"dLength\": \"4\" }]}",[ar objectForKey:@"type"],[ar objectForKey:@"subtype"],[ar objectForKey:@"dType"],[ar objectForKey:@"dLength"]];
                }
            }

            NSDictionary* finalCred = [commonUtils createDict:strCred];
            __block NSString* mpinParam;
            __block NSString* nmpinParam;


            [srvt getCredential:@"NPCI" keyXMLPayload:apiProxy.xmlPayload controls:finalCred configuration:config salt:salt payInfo:payInfo language:@"en_US" token:token view:view withCompletion:^(BOOL success, NSError *error, id response) {

                NSLog(@" ApiClient getCredential output ==  %@",response);

                NSDictionary* comResponse = [response objectForKey:@"credBlocks"];
                //                NSLog(@"  comResponse  ==  %@",comResponse);
                if(success && [[response objectForKey:@"resultCode"] isEqualToString:@"1"]){

                if([comResponse objectForKey:@"MPIN"]){
                    // NSLog(@" MPIN ");
                    NSDictionary* data1 = [comResponse valueForKey:@"MPIN"];
                    NSDictionary* data2 = [data1 valueForKey:@"data"];
                    mpinParam = [NSString stringWithFormat:@"%@,%@,%@",code,ki,[data2 objectForKey:@"encryptedBase64String"]] ;
                    //        NSLog(@"  mpinParam  ==  %@",mpinParam);

                }
                if([comResponse objectForKey:@"NMPIN"]){
                    // NSLog(@" NMPIN ");
                    NSDictionary* data1 = [comResponse valueForKey:@"NMPIN"];
                    NSDictionary* data2 = [data1 valueForKey:@"data"];
                    nmpinParam = [NSString stringWithFormat:@"%@,%@,%@",code,ki,[data2 objectForKey:@"encryptedBase64String"]] ;
                    //  NSLog(@"  nmpinParam  ==  %@",mpinParam);

                }
                NSString* acc_type = nil;
                if (custAccount.getAccountType == nil) {
                    acc_type = @"SAVINGS";
                }else{
                    acc_type = custAccount.getAccountType;
                }

                inputDict = @{@"old-mpin":mpinParam ,@"new-mpin":nmpinParam,@"account-provider":custAccount.provider.getId,@"account-number":custAccount.getMaskedAccount,@"account-type":acc_type,@"ifsc":custAccount.getIfsc,@"virtual-address":virtualaddress.getHandle,@"seq-no":transId};

                //                NSLog(@"  inputDict  ==  %@",inputDict);

                [apiProxy changeMpin:inputDict  commonParam:commonParams  withCompletion:^(BOOL success, NSError *error, id response) {

                    if([[response objectForKey:@"response"] isEqualToString:@"0"] ){
                        NSLog(@" changeMpin  responce ==  %@",response);
                        completionBlock(YES,nil,response); // here that call when method complete

                    }else{
                        NSLog(@"Error  changeMpin  ==  %@",error);
                        completionBlock(NO,nil,response); // here that call when method complete

                    }
                }];

                }else if([[response objectForKey:@"resultCode"] isEqualToString:@"2"]){

                    [apiProxy generateOTP:custAccount.provider.getId commonParam:commonParams ifsc:custAccount.getIfsc accountnumber:custAccount.getMaskedAccount  withCompletion:^(BOOL success, NSError *error, id response) {
                        NSLog(@" resend otp ==  %@",response);
                    }];

                }else if(error){
                    NSLog(@"Error change MPIN ==  %@",error);
                }

            }];

        }else{
            NSLog(@"Sdk not connected..");
        }
    }@catch(NSException * exception){
        NSLog(@" change MPIN NSException : %@",exception);
    }
}


-(void)resetMPIN :(VirtualAddress*)virtualaddress custAccount:(CustomerAccount*)custAccount carddata:(CardData*)carddata view:(UIViewController*)view :(completion) completionBlock{

    CommonUtils* commonUtils = [[CommonUtils alloc] init];

    @try{
        if(apiProxy.serviceConnected){

            [commonUtils notNull:virtualaddress name:@"VirtualAddress"];
            [commonUtils notNull:custAccount name:@"CustomerAccount"];
            [commonUtils notNull:carddata name:@"CardData"];
            NSDictionary* commonParam = [self getCommonParameters];

            __block NSDictionary* inputDict;
            __block NSDictionary* output;

            DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc]init];
            __block NSString* deviceId = [deviceIdController generateUUID];
            NSString* defaultDebit;
            NSString* defaultCredit;

            NSString* actionFlag = @"P";
            AccountDefaultStatus* status = [[AccountDefaultStatus alloc] init];

            if (status.defaultDebit) {
                defaultDebit = @"D";
            }else{
                defaultDebit = @"N";

            }

            if (status.defaultCredit) {
                defaultCredit = @"D";
            }else{
                defaultCredit = @"N";

            }

            NSString* code = @"NPCI";
            NSString* ki = @"20150822";

            NSString* expeiryDate = [NSString stringWithFormat:@"%@%@",carddata.expiryMonth,carddata.expiryYear];

            SrvtCommonLib *srvt = [[SrvtCommonLib alloc]init];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString* token = (NSString *)[prefs objectForKey:@"token"];

            NSDictionary* config = @{@"payerBankName":custAccount.provider.getAccountProvider,@"resendOTPFeature":@"true"};
            NSString* transId = [self generateTransactionID ];
            NSDictionary* salt = @{@"txnId" :transId,@"appId":apiProxy.appName , @"deviceId":deviceId,@"mobileNumber":apiProxy.mobileNo};

            NSArray *payInfo  = [NSArray array];
            payInfo = @[@{@"name": @"refId",    @"value": transId },
                        @{@"name": @"refUrl",   @"value": apiProxy.clientApplication.getBankRefUrl },
                        @{@"name": @"account",   @"value": custAccount.getMaskedAccount }];

            NSString* userId = (NSString *)[prefs objectForKey:@"user_Id"];
            __block NSString* mpinParam;
            __block NSString* otpParam;

            [apiProxy generateOTP:custAccount.provider.getId commonParam:commonParam ifsc:custAccount.getIfsc accountnumber:custAccount.getMaskedAccount  withCompletion:^(BOOL success, NSError *error, id response) {

                if(success){

                    NSLog(@"generateOTP responce ==  %@",response);

                    [srvt getCredential:@"NPCI" keyXMLPayload:apiProxy.xmlPayload controls:custAccount.getCredArr configuration:config salt:salt payInfo:payInfo language:@"en_US" token:token view:view withCompletion:^(BOOL success, NSError *error, id response) {

                        NSLog(@" getCredential response  ==  %@",response);

                        if(success && [[response objectForKey:@"resultCode"] isEqualToString:@"1"]){
                            //                    NSLog(@" ApiClient getCredential output response ==  %@",response);
                            //                    NSLog(@"You got: %@",success ? @"getCredential YES" : @"getCredential NO");

                            NSDictionary* comResponse = [response objectForKey:@"credBlocks"];
                            NSString* resultCode = [response objectForKey:@"resultCode"];

                            NSLog(@"  comResponse  ==  %@",comResponse);

                            if([comResponse objectForKey:@"MPIN"]){
                                //     NSLog(@" MPIN ");
                                NSDictionary* data1 = [comResponse valueForKey:@"MPIN"];
                                NSDictionary* data2 = [data1 valueForKey:@"data"];
                                mpinParam = [NSString stringWithFormat:@"%@,%@,%@",code,ki,[data2 objectForKey:@"encryptedBase64String"]] ;
                                //    NSLog(@"  mpinParam  ==  %@",mpinParam);

                            }
                            if([comResponse objectForKey:@"SMS"]){
                                // NSLog(@" SMS ");
                                NSDictionary* data1 = [comResponse valueForKey:@"SMS"];
                                NSDictionary* data2 = [data1 valueForKey:@"data"];
                                otpParam = [NSString stringWithFormat:@"%@,%@,%@",code,ki,[data2 objectForKey:@"encryptedBase64String"]] ;
                                //       NSLog(@"  otpParam  ==  %@",otpParam);
                            }
                            if([comResponse objectForKey:@"ATM"]){
                                NSLog(@"ATM");
                            }


                            //  NSString* credStr = @"[{\"CredsAllowedType\":\"OTP\",\"CredsAllowedDType\":\"Numeric\",\"CredsAllowedSubType\":\"SMS\",\"CredsAllowedDLength\":\"6\",\"dLength\":\"6\"},{\"CredsAllowedType\":\"PIN\",\"CredsAllowedDType\":\"Numeric\",\"CredsAllowedSubType\":\"MPIN\",\"CredsAllowedDLength\":\"4\",\"dLength\":\"4\"}]";

                            inputDict = @{@"session-id":apiProxy.sessionId,@"user-id":userId,@"channel-code":apiProxy.channel,@"action-flag":actionFlag,@"card-digits":carddata.getLastSix,@"expiry-date":expeiryDate , @"mpin":mpinParam ,@"account-provider":custAccount.provider.getId,@"account-number":custAccount.getMaskedAccount,@"account-type":custAccount.getAccountType,@"default-debit":defaultDebit,@"default-credit":defaultCredit,@"ifsc":custAccount.getIfsc,@"otp":otpParam,@"name":custAccount.getName,@"credsAllowedJson":custAccount.getCredentialRequired,@"virtual-address":virtualaddress.getHandle,@"mmid":custAccount.getMmid,@"mobile":apiProxy.mobileNo,@"seq-no":transId };

                            [apiProxy registerMobile:inputDict withCompletion:^(BOOL success, NSError *error, id response) {

                                if([[response objectForKey:@"response"] isEqualToString:@"0"] ){
                                    NSLog(@" resetMpin  responce ==  %@",response);
                                    completionBlock(YES,nil,response); // here that call when method complete

                                }else{
                                    NSLog(@"Error  resetMpin  ==  %@",error);
                                    completionBlock(NO,nil,response); // here that call when method complete

                                }
                            }];
                        }else if([[response objectForKey:@"resultCode"] isEqualToString:@"2"]){
                            //  NSLog(@" resultCode ==  %@",[response objectForKey:@"resultCode"]);


                            [apiProxy generateOTP:custAccount.provider.getId commonParam:commonParam ifsc:custAccount.getIfsc accountnumber:custAccount.getMaskedAccount  withCompletion:^(BOOL success, NSError *error, id response) {
                                NSLog(@" resend otp ==  %@",response);
                            }];

                        }else if(error){
                            NSLog(@"Error  registerMobileNo ==  %@",error);
                        }else{
                            NSLog(@"Error  Register Mobile No ==  %@",response);
                        }

                    }];
                }else if(error){
                    NSLog(@"Error check Vpa ==  %@",error);
                }
            }];

        }else{
            NSLog(@"Sdk not connected..");
        }
    }@catch(NSException * exception){
        NSLog(@" Reset MPIN NSException : %@",exception);
    }

}


-(void)changeDefaultAccount :(VirtualAddress*)virtualaddress custAccount:(CustomerAccount *)custAccount  accountDefaultStatus:(AccountDefaultStatus *)status withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    __block NSDictionary* inputDict;

    NSString* defaultDebit;
    NSString* defaultCredit;
    @try{

        if(apiProxy.serviceConnected){

            if (status.defaultDebit) {
                defaultDebit = @"D";
            }else{
                defaultDebit = @"N";
            }

            if (status.defaultCredit) {
                defaultCredit = @"D";
            }else{
                defaultCredit = @"N";
            }

            NSDictionary* commonParam = [self getCommonParameters];

            inputDict = @{@"account-provider":custAccount.provider.getId,@"account-number":custAccount.getMaskedAccount,@"account-type":custAccount.getAccountType,@"default-debit":defaultDebit,@"default-credit":defaultCredit,@"ifsc":custAccount.getIfsc,@"name":custAccount.getName,@"virtual-address":virtualaddress.getHandle};

            //  NSLog(@"  inputDict  ==  %@",inputDict);
            [apiProxy changeVPADefaultAccount:inputDict commonParam:commonParam withCompletion:^(BOOL success, NSError *error, id response) {

                if([[response objectForKey:@"response"] isEqualToString:@"0"]){
                    NSLog(@" changeVPADefaultAccount  responce ==  %@",response);
                    completion(YES,nil,response); // here that call when method complete

                }else{
                    NSLog(@"Error  changeVPADefaultAccount  ==  %@",error);
                    completion(NO,nil,response); // here that call when method complete

                }
            }];

        }else{
            NSLog(@"Sdk not connected..");
        }
    }@catch(NSException * exception){
        NSLog(@" Change Default Account NSException : %@",exception);
    }
}


-(void)collect :(NSString*)payerVirtualaddress payee:(VirtualAddress*)payee amount:(NSDecimalNumber*)amount remark:(NSString*)remark expireAfter:(int)expireAfter  view:(UIViewController*)view :(completion) completionBlock{

    @try{
        if(apiProxy.serviceConnected){


            TransactionParameters *transParam = [TransactionParameters alloc];
            PayerParam * payer = [PayerParam alloc];
            payer = [payer initWith_VPA:payerVirtualaddress];


            NSLog(@"  collect payer ==  %@",payer.payerVpa);

            transParam = [transParam initWithTransaction:payee payer:payer amount:amount remark:remark];
            NSLog(@"  collect VirtualAddress ==  %@",payee.getHandle);

            NSString * transactionId = [self generateTransactionID];

            NSDictionary* commonParam = [self getCommonParameters];

            [apiProxy collect:transParam commonParams:commonParam transId:transactionId expierAfter:expireAfter view:view withCompletion:^(BOOL success, NSError *error, id response) {

                if([[response objectForKey:@"response"] isEqualToString:@"0"] ){
                    NSLog(@" collect  responce ==  %@",response);
                    completionBlock(YES,nil,response); // here that call when method complete

                }else{
                    NSLog(@"Error  collect  ==  %@",error);
                    completionBlock(NO,nil,response); // here that call when method complete

                }
            }];
        }else{
            NSLog(@"Sdk not connected..");
        }
    }@catch(NSException * exception){
        NSLog(@" Collect NSException : %@",exception);
    }
}

-(void)stopSdkService{
    apiProxy.serviceConnected = false;
    NSLog(@"Sdk service disconnected..");
}

-(void)balanceInquiry:(VirtualAddress *)virtualaddress custAccount:(CustomerAccount *)custAccount view:(UIViewController*)view :(completion)completionBlock{

    @try{


        NSString* transId = [self generateTransactionID];
        NSString* code = @"NPCI";
        NSString* ki = @"20150822";
        SrvtCommonLib *srvt = [[SrvtCommonLib alloc]init];
        NSDictionary *commonParam = [self getCommonParameters];

        NSString* mobile_no = [NSString stringWithFormat:@"%@%@",@"91",apiProxy.mobileNo];

        __block NSString* mpinParam;

        NSDictionary* credA = custAccount.getCredArr;

        NSMutableArray * key = [credA objectForKey:@"CredAllowed"];

        NSMutableString* strCred = [NSMutableString stringWithFormat:@"%@",@"{\"CredAllowed\":["];

        NSDictionary* config = @{@"payerBankName":custAccount.provider.getAccountProvider};

        NSDictionary* salt = @{@"txnId" :transId,@"appId":apiProxy.appName , @"deviceId":[commonParam objectForKey:@"device-id"],@"mobileNumber":mobile_no};
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString* token = (NSString *)[prefs objectForKey:@"token"];

        NSArray *payInfo  = [NSArray array];
        payInfo = @[@{@"name": @"refId",    @"value": transId },
                    @{@"name": @"refUrl",   @"value": apiProxy.clientApplication.getBankRefUrl },
                    @{@"name": @"account",   @"value": custAccount.getMaskedAccount }];


        for(int i=0 ; i<key.count ; i++){
            NSDictionary* ar = [key objectAtIndex:i];
            //  NSLog(@"key Pay : %d =  %@",i,[key objectAtIndex:i]);
            if ([[ar objectForKey:@"subtype"] isEqualToString:@"MPIN"]) {
                strCred = [strCred stringByAppendingFormat:@"{\"type\":\"%@\",\"subtype\":\"%@\",\"dType\":\"%@\",\"dLength\":%@ }]}",[ar objectForKey:@"type"],[ar objectForKey:@"subtype"],[ar objectForKey:@"dType"],[ar objectForKey:@"dLength"]];
            }
        }
        CommonUtils *commonUtils = [[CommonUtils alloc]init];
        NSDictionary* finalCred = [commonUtils createDict:strCred];
        
        if(apiProxy.serviceConnected){
            TransactionParameters* transactionParameters = [TransactionParameters alloc] ;
            transactionParameters = [transactionParameters initWithVirtualAddress:virtualaddress];
            
            __block NSDictionary* transParam ;
            
            NSString * acc_Type ;
            
            if ([ custAccount.getAccountType isEqualToString:@"SAVINGS"]) {
                acc_Type = @"SAVINGS";
            }else{
                acc_Type = custAccount.getAccountType;
            }
            
            [srvt getCredential:@"NPCI" keyXMLPayload:apiProxy.xmlPayload controls:finalCred configuration:config salt:salt payInfo:payInfo language:@"en_US" token:token view:view withCompletion:^(BOOL success, NSError *error, id response) {
                
                NSLog(@" ApiClient getCredential output ==  %@",response);
                
                NSDictionary* comResponse = [response objectForKey:@"credBlocks"];
            if(success && [[response objectForKey:@"resultCode"] isEqualToString:@"1"]){

                if([comResponse objectForKey:@"MPIN"]){
                    NSLog(@" MPIN ");
                    NSDictionary* data1 = [comResponse valueForKey:@"MPIN"];
                    NSDictionary* data2 = [data1 valueForKey:@"data"];
                    mpinParam = [NSString stringWithFormat:@"%@,%@,%@",code,ki,[data2 objectForKey:@"encryptedBase64String"]] ;
                    
                }
                
                transParam = @{@"account-provider":custAccount.provider.getId,@"account-number":custAccount.getMaskedAccount,@"account-type":acc_Type,@"ifsc":custAccount.getIfsc,@"name":custAccount.getName,@"virtual-address":virtualaddress.getHandle ,@"mpin":mpinParam,@"seq-no":transId};
                
                [apiProxy balanceInquiry:transParam commonParam:commonParam withCompletion:^(BOOL success, NSError *error, id response){
                    
                    if([[response objectForKey:@"response"] isEqualToString:@"0"]){
                        NSLog(@" balanceInquiry  responce ==  %@",response);
                        NSString* balance = [response objectForKey:@"MobileAppData"];
                        NSArray *splitResponse = [balance componentsSeparatedByString:@"="];
                        balance = [splitResponse lastObject];
                        completionBlock(YES,nil,balance); // here that call when method complete
                        
                    }else{
                        NSLog(@"Error  balanceInquiry  =  %@",error);
                        completionBlock(NO,nil,response); // here that call when method complete
                        
                    }
                    
                }];
            }else if([[response objectForKey:@"resultCode"] isEqualToString:@"2"]){
                NSLog(@" resultCode ==  %@",[response objectForKey:@"resultCode"]);


                [apiProxy generateOTP:custAccount.provider.getId commonParam:commonParam ifsc:custAccount.getIfsc accountnumber:custAccount.getMaskedAccount  withCompletion:^(BOOL success, NSError *error, id response) {
                    NSLog(@" resend otp ==  %@",response);
                }];

            }else if(error){
                NSLog(@"Error  Balance Inquiry =  %@",error);
            }

            }];

            
        }else{
            NSLog(@"Sdk not connected..");
        }
    }@catch(NSException * exception){
        NSLog(@" Balance Inquiry NSException : %@",exception);
    }
}


-(void)approve:(VirtualAddress *)fromVirtualAddress pendingTransactionInfo:(PendingTransactionInfo *)pendingTransactionInfo remark:(NSString *)remark view:(UIViewController *)view :(completion)completionBlock{
    
    __block NSDictionary* otherParams;
    BOOL accept = true;
    
    NSString* transId = [self generateTransactionID];
    
    @try{
        
        NSString* code = @"NPCI";
        NSString* ki = @"20150822";
        SrvtCommonLib *srvt = [[SrvtCommonLib alloc]init];
        NSDictionary *commonParam = [self getCommonParameters];
        
        NSString* mobile_no = [NSString stringWithFormat:@"%@%@",@"91",apiProxy.mobileNo];
        
        __block NSString* mpinParam;
        
        TransactionParameters* transactionParameters = [TransactionParameters alloc] ;
        PayeeParam* payeeParam = [PayeeParam alloc] ;
        payeeParam = [payeeParam initWithVPA:pendingTransactionInfo.getPayeeVa];
        
        transactionParameters = [transactionParameters initWithTransaction:fromVirtualAddress payee:payeeParam amount:pendingTransactionInfo.getAmount remark:remark];
        
        
        
        NSDictionary* credA = transactionParameters.customerAccount.getCredArr;
        
        NSMutableArray * key = [credA objectForKey:@"CredAllowed"];
        
        NSMutableString* strCred = [NSMutableString stringWithFormat:@"%@",@"{\"CredAllowed\":["];
        
        NSDictionary* config = @{@"payerBankName":transactionParameters.customerAccount.provider.getAccountProvider};
        
        NSDictionary* salt = @{@"txnId" :transId,@"appId":apiProxy.appName , @"deviceId":[commonParam objectForKey:@"device-id"],@"mobileNumber":mobile_no};
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString* token = (NSString *)[prefs objectForKey:@"token"];
        
        NSArray *payInfo  = [NSArray array];
        payInfo = @[@{@"name": @"refId",    @"value": transId },
                    @{@"name": @"refUrl",   @"value": apiProxy.clientApplication.getBankRefUrl },
                    @{@"name": @"account",   @"value": transactionParameters.customerAccount.getMaskedAccount }];
        
        
        for(int i=0 ; i<key.count ; i++){
            NSDictionary* ar = [key objectAtIndex:i];
            //  NSLog(@"key Pay : %d =  %@",i,[key objectAtIndex:i]);
            if ([[ar objectForKey:@"subtype"] isEqualToString:@"MPIN"]) {
                strCred = [strCred stringByAppendingFormat:@"{\"type\":\"%@\",\"subtype\":\"%@\",\"dType\":\"%@\",\"dLength\":%@ }]}",[ar objectForKey:@"type"],[ar objectForKey:@"subtype"],[ar objectForKey:@"dType"],[ar objectForKey:@"dLength"]];
            }
        }
        CommonUtils *commonUtils = [[CommonUtils alloc]init];
        NSDictionary* finalCred = [commonUtils createDict:strCred];
        
        if(apiProxy.serviceConnected){
            NSString* action ;
            NSString* pre_approved ;
            
            NSString* amount = pendingTransactionInfo.getAmount;
            
            if(![amount containsString:@"."]){
                amount = [amount stringByAppendingString:@".00"];
            }
            NSLog(@"Amount pay : %@",amount);
            
            if (accept) {
                action = @"A";
                // add("mpin", getMpinParam());
                pre_approved = @"M";
                
                [srvt getCredential:@"NPCI" keyXMLPayload:apiProxy.xmlPayload controls:finalCred configuration:config salt:salt payInfo:payInfo language:@"en_US" token:token view:view withCompletion:^(BOOL success, NSError *error, id response) {
                    
                    NSLog(@" ApiClient getCredential output ==  %@",response);
                    
                    NSDictionary* comResponse = [response objectForKey:@"credBlocks"];
                    NSString* resultCode = [response objectForKey:@"resultCode"];
                if(success && [[response objectForKey:@"resultCode"] isEqualToString:@"1"]){

                        if([comResponse objectForKey:@"MPIN"]){
                            NSLog(@" MPIN ");
                            NSDictionary* data1 = [comResponse valueForKey:@"MPIN"];
                            NSDictionary* data2 = [data1 valueForKey:@"data"];
                            mpinParam = [NSString stringWithFormat:@"%@,%@,%@",code,ki,[data2 objectForKey:@"encryptedBase64String"]] ;
                            
                        }
                        
                        otherParams = @{@"payer-va":pendingTransactionInfo.getPayerVa , @"payee-va":pendingTransactionInfo.getPayeeVa,@"amount":amount,@"payer-amount":amount,@"note":pendingTransactionInfo.getRemark,@"action":action,@"upi-tranlog-id":pendingTransactionInfo.getTranLogID,@"account-type":transactionParameters.customerAccount.getAccountType,@"ifsc":transactionParameters.customerAccount.getIfsc,@"account-number":transactionParameters.customerAccount.getMaskedAccount,@"account-provider":transactionParameters.customerAccount.provider.getId ,@"pre-approved":pre_approved,@"seq-no":transId,@"mpin":mpinParam };
                        
                        
                        [apiProxy approveReject:commonParam otherParams:otherParams withCompletion:^(BOOL success, NSError *error, id response){
                            
                            if([[response objectForKey:@"response"] isEqualToString:@"0"]){
                                
                                NSLog(@" approveReject  responce ==  %@",response);
                                completionBlock(YES,nil,response); // here that call when method complete
                                
                            }else{
                                NSLog(@"Error  approveReject  ==  %@",error);
                                completionBlock(NO,nil,response); // here that call when method complete
                            }
                        }];
                }else{
                    NSLog(@"Error  approve  ==  %@",error);
                    NSLog(@"Resonse  approve  ==  %@",response);

                }
             }];
                
            }
            
        }else{
            NSLog(@"Sdk not connected..");
        }
    }@catch(NSException * exception){
        NSLog(@" Aprove NSException : %@",exception);
    }
}

-(void)reject:(VirtualAddress *)virtualaddress pendingTransactionInfo:(PendingTransactionInfo *)pendingTransactionInfo remark:(NSString *)remark view:(UIViewController *)view :(completion)completionBlock{
    
    __block NSDictionary* otherParams;
    
    @try{
        NSString* transId = [self generateTransactionID];
        
        NSDictionary *commonParam = [self getCommonParameters];
        
        TransactionParameters* transactionParameters = [TransactionParameters alloc] ;
        PayeeParam* payeeParam = [PayeeParam alloc] ;
        payeeParam = [payeeParam initWithVPA:pendingTransactionInfo.getPayeeVa];
        
        transactionParameters = [transactionParameters initWithTransaction:virtualaddress payee:payeeParam amount:pendingTransactionInfo.getAmount remark:remark];
        
        if(apiProxy.serviceConnected){
            NSString* action ;
            NSString* pre_approved ;
            
            action = @"R";
            
            pre_approved = @"A";
            
            NSString* amount = pendingTransactionInfo.getAmount;
            
            if(![amount containsString:@"."]){
                amount = [amount stringByAppendingString:@".00"];
            }
            NSLog(@"Amount pay : %@",amount);
            
            otherParams = @{@"payer-va":pendingTransactionInfo.getPayerVa , @"payee-va":pendingTransactionInfo.getPayeeVa,@"amount":amount,@"payer-amount":amount,@"note":pendingTransactionInfo.getRemark,@"action":action,@"upi-tranlog-id":pendingTransactionInfo.getTranLogID,@"account-type":transactionParameters.customerAccount.getAccountType,@"ifsc":transactionParameters.customerAccount.getIfsc,@"account-number":transactionParameters.customerAccount.getMaskedAccount,@"account-provider":transactionParameters.customerAccount.provider.getId ,@"pre-approved":pre_approved,@"seq-no":transId,@"mpin":@"" };
            
            
            [apiProxy approveReject:commonParam otherParams:otherParams withCompletion:^(BOOL success, NSError *error, id response){
                
                if([[response objectForKey:@"response"] isEqualToString:@"0"]){
                    
                    NSLog(@" approveReject  responce ==  %@",response);
                    completionBlock(YES,nil,response); // here that call when method complete
                    
                }else{
                    NSLog(@"Error  balanceInquiry  ==  %@",error);
                    completionBlock(NO,nil,response); // here that call when method complete
                    
                }
            }];
            
        }else{
            NSLog(@"Sdk not connected..");
        }
    }@catch(NSException * exception){
        NSLog(@" Reject NSException : %@",exception);
    }
}

-(void)getComplaintReasonCode:(completion)completionBlock {

    NSDictionary* commonParam = [self getCommonParameters];
    NSString* url =@"transaction/get-reason-codes";
    __block NSMutableArray *complaintCode = [NSMutableArray new];


    if (apiProxy.serviceConnected ) {

        [apiProxy responseWithUrl:commonParam urlString:url withCompletion:^(BOOL success, NSError *error, id response) {

            if(success){

                NSLog(@"getComplaintReasonCode responce ==  %@",response);

                NSString* str = [response objectForKey:@"MobileAppData"];
                // NSLog(@"str : MobileAppData %@", str);

                if([[response objectForKey:@"response"] isEqualToString:@"0"]){

                    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    NSLog(@"getComplaintReasonCode MobileAppData json ==  %@",json);

//                    for (id key in json){
//                        if ([key objectForKey:@"description"]) {
                          //  [complaintCode addObject:[key objectForKey:@"description"]];
                         //   [complaintCode addObject:key];

//                        }
//                    }
//                    NSLog(@"getComplaintReasonCode MobileAppData complaintCode ==  %@",complaintCode);

                    completionBlock(success,nil,json);

                }else{
                    completionBlock(NO,nil,response);
                }

            }else if(error){

                NSLog(@"Error check Vpa ==  %@",error);
                completionBlock(NO,nil,response);
                
            }
            
            
        }];
        
    }else{
        NSLog(@"Sdk not activated ..");
    }
    
}

-(void)getComplaintList:(completion)completionBlock {

    NSDictionary* commonParam = [self getCommonParameters];
    NSString* url =@"transaction/get-complaint-list";
    __block NSMutableArray *compaintCode = [NSMutableArray new];


    if (apiProxy.serviceConnected ) {

        [apiProxy responseWithUrl:commonParam urlString:url withCompletion:^(BOOL success, NSError *error, id response) {

            if(success){

                NSLog(@"getComplaintList responce ==  %@",response);

                NSString* str = [response objectForKey:@"MobileAppData"];
                // NSLog(@"str : MobileAppData %@", str);


                if([[response objectForKey:@"response"] isEqualToString:@"0"]){

                    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
 //                   NSLog(@"getComplaintList MobileAppData json ==  %@",json);
//
//                    for (id key in json){
//                        if ([key objectForKey:@"description"]) {
//                            [compaintCode addObject:[key objectForKey:@"description"]];
//                        }
//                    }
//                    NSLog(@"getComplaintList MobileAppData compaintCode ==  %@",compaintCode);

                    completionBlock(success,nil,json);

                }else{
                    completionBlock(NO,nil,response);
                }

            }else if(error){

                NSLog(@"Error check Vpa ==  %@",error);
                completionBlock(NO,nil,response);
                
            }
            
            
        }];
        
    }else{
        NSLog(@"Sdk not activated ..");
    }
    
}

-(void)getComplaintStatus:(NSString*)complaintId :(completion)completionBlock {

    NSDictionary* commonParam = [self getCommonParameters];
    NSString* url = @"transaction/check-complaint-status";

    NSDictionary* param =@{@"complaint-id":complaintId};

    if (apiProxy.serviceConnected ) {

        [apiProxy complaintResponse:commonParam parameters:param urlString:url withCompletion:^(BOOL success, NSError *error, id response) {

            if(success){

                NSLog(@"getComplaintStatus responce ==  %@",response);

                NSString* str = [response objectForKey:@"MobileAppData"];
                // NSLog(@"str : MobileAppData %@", str);


                if([[response objectForKey:@"response"] isEqualToString:@"0"]){

                                        NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                                        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    //                    NSLog(@"getComplaintList MobileAppData json ==  %@",json);
                    //
                    //                    for (id key in json){
                    //                        if ([key objectForKey:@"description"]) {
                    //                            [compaintCode addObject:[key objectForKey:@"description"]];
                    //                        }
                    //                    }
                    //                    NSLog(@"getComplaintList MobileAppData compaintCode ==  %@",compaintCode);

                    completionBlock(success,nil,json);

                }else{
                    completionBlock(NO,nil,response);
                }
                
            }else if(error){
                
                NSLog(@"Error check Vpa ==  %@",error);
                completionBlock(NO,nil,response);
                
            }
            
            
        }];
        
    }else{
        NSLog(@"Sdk not activated ..");
    }
    
}

-(void)registerComplaint:(NSString*)reasonCode comment:(NSString*)comment rrn:(NSString*)rrn txnType:(NSString*)txnType txnDate:(NSString*)txnDate :(completion)completionBlock{

    NSDictionary* commonParam = [self getCommonParameters];
    NSString* url = @"transaction/register-complaint";

    NSDictionary* param =@{@"reason-code":reasonCode,@"comment":comment,@"rrn":rrn,@"txn-type":txnType,@"txn-date":txnDate};

    if (apiProxy.serviceConnected ) {

        [apiProxy complaintResponse:commonParam parameters:param urlString:url withCompletion:^(BOOL success, NSError *error, id response) {

            if(success){

                NSLog(@"getComplaintStatus responce ==  %@",response);

                if([[response objectForKey:@"response"] isEqualToString:@"0"]){

                    completionBlock(success,nil,response);

                }else{
                    completionBlock(NO,nil,response);
                }
            }else if(error){

                NSLog(@"Error registerComplaint=  %@",error);
                completionBlock(NO,nil,response);
            }
        }];
        
    }else{
        NSLog(@"Sdk not activated ..");
    }

}

@end
