//
//  CardData.m
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "CardData.h"
#import "SdkException.h"
#import "CommonUtils.h"

@implementation CardData


@synthesize lastSix;
@synthesize expiryMonth;
@synthesize expiryYear;



-(id)initWith :(NSString*)lastSixDigit expiry_month:(NSString*)expiry_month expiry_year:(NSString*)expiry_year{

    if (self = [super init]) {
        lastSix = lastSixDigit;
        expiryMonth = expiry_month;
        expiryYear = expiry_year;
    }
    return self;
}


- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.lastSix = [decoder decodeObjectForKey:@"lastSix"];
        self.expiryMonth = [decoder decodeObjectForKey:@"expiryMonth"];
        self.expiryYear = [decoder decodeObjectForKey:@"expiryYear"];

    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:lastSix forKey:@"lastSix"];
    [encoder encodeObject:expiryMonth forKey:@"expiryMonth"];
    [encoder encodeObject:expiryYear forKey:@"expiryYear"];
    
}

-(NSString*) getLastSix{
    return lastSix;
}

-(void) setLastSix:(NSString*)lastSixDig{
    
    lastSix = lastSixDig;
}

-(NSString*) getExpiryMonth{
    return expiryMonth;
}

-(void) setExpiryMonth:(NSString*) expiryMonthOfCard{
    expiryMonth = expiryMonthOfCard;
}

-(NSString*)getExpiryYear{
    return expiryYear;
}

-(void) setExpiryYear:(NSString*) expiryYearOfCard{
    expiryYear = expiryYearOfCard;
}

-(void)validate:(CardData*)cardData{
    
    CommonUtils* commonUtils = [[CommonUtils alloc]init];
    [commonUtils validateTrue:[cardData.expiryMonth intValue]>0 error:@"Expiry Month should be greater than zero"];
    [commonUtils validateTrue:[cardData.expiryMonth intValue]<12 error:@"Expiry Month should be less than 12"];

    [commonUtils validateTrue:[cardData.expiryYear intValue]>16 error:@"Expiry expiryYear should be greater than 16"];
    [commonUtils validateTrue:[cardData.expiryYear intValue]<=99 error:@"Expiry Month should be less than 99"];

    [commonUtils notNull:cardData.lastSix name:@"Card Last Six digit"];
    [commonUtils notBlank:cardData.lastSix name:@"Card Last Six digit"];
    [commonUtils validateTrue:(cardData.lastSix.length == 6) error:[NSString stringWithFormat:@"%@%u",@"Card Last Six digit is expected. Actual length is",cardData.lastSix.length]];
}



@end
