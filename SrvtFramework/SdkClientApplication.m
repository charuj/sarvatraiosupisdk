//
//  SdkClientApplication.m
//  SrvtFramework
//
//  Created by Charushila on 11/07/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SdkClientApplication.h"


@implementation SdkClientApplication

@synthesize appName;
@synthesize transactionId;
@synthesize instituteId;
@synthesize channel;
@synthesize bankRefUrl;
@synthesize userName;
@synthesize userMail;
@synthesize mobile;
@synthesize userIdentity;


-(NSString*) getApplicationName{
    return appName;
}

-(NSString*) getTransactionIDPrefix{
    return transactionId;
}

-(NSString*) getInstitute{
    return instituteId;
}

-(NSString*) getChannel{
    return channel;
}

-(NSString*) getBankRefUrl{
    return bankRefUrl;
}

-(NSString*) getUserName{
    return userName;
}

-(NSString*) getUserEmail{
    return userMail;
}

-(NSString*) retrieveMobile{
    return mobile;
}

-(NSString*) getUserIdentity{
    return userIdentity;
}

-(void) storeValue :(NSString*)key value:(NSString*)value{

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*) readValue :(NSString*)key{

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString* value = (NSString *)[prefs objectForKey:key];

    return  value;
}

-(id)initWithUserName:(NSString*)user_Name appName:(NSString*)app_Name instituteId:(NSString*)institute_Id channel:(NSString*)channel_Code bankRefUrl:(NSString*)bankRef_Url userMail:(NSString*)user_Mail mobile:(NSString*)mobile_No userIdentity:(NSString*)user_Identity transactionID_Prefix:(NSString*)transactionID_Prefix{

    if( self = [super init] )
    {
        userName = user_Name;
        appName = app_Name;
        instituteId = institute_Id;
        channel = channel_Code;
        bankRefUrl = bankRef_Url;
        userMail = user_Mail;
        mobile = mobile_No;
        userIdentity = user_Identity;
        transactionId = transactionID_Prefix;
    }

    return self;
}

@end
