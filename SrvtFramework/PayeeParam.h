//
//  PayeeParam.h
//  SrvtFramework
//
//  Created by Charushila on 06/07/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountProvider.h"
#import "ParticipantParam.h"
#import "TransactionItem.h"
#import "VirtualAddress.h"

@interface PayeeParam : NSObject{

    NSString* account;
    NSString* ifsc;
    NSString* payeeName;
    NSString* mobile;
    int mmid;
    NSString* aadhar;
    NSString* virtualAddress;

    AccountProvider* accountProvider;
    ParticipantParam* participantParam;
    VirtualAddress *vr;
    int payeeMode;

}

@property (nonatomic) NSString* account;
@property (nonatomic) NSString* ifsc;
@property (nonatomic) NSString* payeeName;
@property (nonatomic) int mmid;
@property (nonatomic) NSString* aadhar;
@property (nonatomic) NSString* vpa;
@property (nonatomic) int payeeMode;
@property (nonatomic) VirtualAddress* vr;

//@property (nonatomic) PayeeMode payeeMode;

/**
 *  Get payee name.
 */
-(NSString*)getPayeeName;

/**
 *  Get payee mobile number.
 */
-(NSString*)getMobile;

/**
 *  Get payee ifsc code.
 */
-(NSString*)getifsc;

/**
 *  Get payee mmid.
 */
-(int)getMmid;

/**
 *  Get payee adhar.
 */
-(NSString*)getAadhar;

/**
 *  Get payee vpa.
 */
-(NSString*)getVpa;

/**
 *  Get payee mode.
 */
-(int)getPayeeMode;

/**
 *  Get Virtual address class object name.
 */
-(VirtualAddress*)getVirtualAddress;

/**
 *  Get AccountProvider class object.
 */
-(AccountProvider*)getAccountProvider;

/**
 *  Initialise with vpa.
 */
-(id)initWithVPA:(NSString *)vpa;

/**
 *  Initialise with VirtualAddress class object.
 */

-(id)initWithVirtualAddress:(VirtualAddress *)virtualAddr;

/**
 *  Initialise with payee details.
 */
-(id)initWithAccount:(NSString *)payee_name ifsc:(NSString *)ifsc_no account:(NSString *)account_no;


/**
 *  Initialise with payee details.
 */
-(id)initWithMobileMmid:(NSString *)payee_name mobile:(NSString *)mobileNo mmid:(int)mmidNo;


/**
 *  Initialise with payee details.
 */
-(id)initWithAdharIin:(NSString *)payee_name aadhar:(NSString *)aadhar_no iin:(AccountProvider*)provider;

@end
