//
//  PayeeParam.m
//  SrvtFramework
//
//  Created by Charushila on 06/07/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "PayeeParam.h"
#import "TransactionParameters.h"


@implementation PayeeParam

TransactionParameters* tranParam;


@synthesize payeeName;
@synthesize account;
@synthesize ifsc;
@synthesize mmid;
@synthesize aadhar;
@synthesize payeeMode;
@synthesize vr;


-(NSString*) getAccount{
    return account;
}

-(NSString*) getifsc{
    return ifsc;
}

-(NSString*) getPayeeName{
    return payeeName;
}

-(NSString*) getMobile{
    return mobile;
}

-(int) getMmid{
    return mmid;
}

-(NSString*) getAadhar{
    return aadhar;
}

-(NSString*) getVpa{
    return virtualAddress;
}

-(int)getPayeeMode{
    return payeeMode;
}

-(VirtualAddress*)getVirtualAddress{
    return vr;
}

-(AccountProvider*)getAccountProvider{
    return accountProvider;
}


-(id)initWithVPA:(NSString *)vpa{

    if( self = [super init] )
    {
        virtualAddress = vpa;
        tranParam = [[TransactionParameters alloc] init];
        payeeMode = 1;
        tranParam.payeeMode = payeeMode;
    }
    return  self;
}

-(id)initWithVirtualAddress:(VirtualAddress *)virtualAddr{

    if( self = [super init] )
    {
        virtualAddress = virtualAddr.getHandle;
        
        ifsc = virtualAddr.custAccount.getIfsc;
        account = virtualAddr.custAccount.getMaskedAccount;
        mmid = (int)virtualAddr.custAccount.getMmid;
        payeeName = virtualAddr.custAccount.getName;
        accountProvider = [[AccountProvider alloc] init];
        accountProvider.providerId = virtualAddr.custAccount.provider.getId;

    }
    return  self;
}


-(id)initWithAccount:(NSString *)payee_name ifsc:(NSString *)ifsc_no account:(NSString *)account_no{
    if( self = [super init] )
    {
        NSLog(@"initWithAccount Payee");
        payeeName = payee_name;
        ifsc = [ifsc_no lowercaseString];
        account = [account_no lowercaseString];
        tranParam = [[TransactionParameters alloc] init];
        //[tranParam setPayeeMode:Account];
        payeeMode = 2;
        tranParam.payeeMode = payeeMode;
        virtualAddress = [NSString stringWithFormat:@"%@%@%@%@",account,@"@",ifsc,@".ifsc.npci"];
    }
    return self;

}
-(id)initWithMobileMmid:(NSString *)payee_name mobile:(NSString *)mobileNo mmid:(int)mmidNo{
    if( self = [super init] )
    {

        payeeName = payee_name;
        mobile = mobileNo;
        mmid = mmidNo;
        payeeMode = 3;
        tranParam.payeeMode = payeeMode;
        virtualAddress = [NSString stringWithFormat:@"%@%@%d%@",mobileNo,@"@",mmid,@".mmid.npci"];

    }

    return self;

}

-(id)initWithAdharIin:(NSString *)payee_name aadhar:(NSString *)aadhar_no iin:(AccountProvider *)provider{
    if( self = [super init] )
    {
        accountProvider = [[AccountProvider alloc] init];
        accountProvider = provider;
        payeeName = payee_name;
        aadhar = aadhar_no;
        payeeMode = 4;
        tranParam.payeeMode = payeeMode;
        virtualAddress = [NSString stringWithFormat:@"%@%@%@%@",aadhar_no,@"@",provider.getIin,@".iin.npci"];

    }

    return self;
    
}



@end
