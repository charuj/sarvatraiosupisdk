//
//  PayerParam.m
//  SrvtFramework
//
//  Created by Charushila on 12/07/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "PayerParam.h"

@implementation PayerParam

@synthesize payerVpa;
@synthesize vr;

-(id)initWithVPA:(VirtualAddress *)virtualAddress{
    if( self = [super init] )
    {
        
       vr = virtualAddress;
    }
    return self;
}

-(id)initWith_VPA:(NSString *)vpa{
    if( self = [super init] )
    {

        payerVpa = vpa;
    }
    return self;
}

-(VirtualAddress*)getPayerVpa{
    return vr;
}

@end
