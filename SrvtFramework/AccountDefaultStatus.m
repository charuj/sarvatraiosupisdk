//
//  AccountDefaultStatus.m
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "AccountDefaultStatus.h"



@implementation AccountDefaultStatus

@synthesize defaultCredit;
@synthesize defaultDebit;

//- (void)dealloc {
//    [defaultCredit release];
//    [defaultDebit release];
//    [super dealloc];
//}

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.defaultCredit = [decoder decodeBoolForKey:@"defaultCredit"];
        self.defaultDebit = [decoder decodeBoolForKey:@"defaultDebit"];

    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeBool:defaultCredit forKey:@"defaultCredit"];
    [encoder encodeBool:defaultDebit forKey:@"defaultDebit"];

}

-(id)initWithDefaultCredit : (BOOL)default_Credit{
    if (self = [super init]) {
    defaultCredit = default_Credit;
    }
    return  self;
}

-(id)initWithDefaultDebit : (BOOL)default_Debit{
    if (self = [super init]) {

    defaultDebit = default_Debit;
    }
    return  self;
}

-(id)initWithDefaultDebitCredit:(BOOL)default_Debit default_Credit:(BOOL)default_Credit{
    if (self = [super init]) {

        defaultDebit = default_Debit;
        defaultCredit = default_Credit;
    }
    return  self;
}

@end
