//
//  AccountProvider.m
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "AccountProvider.h"


@implementation AccountProvider

@synthesize iin;
@synthesize providerId;
@synthesize accountProvider;



-(NSString*)getId{
    return providerId;
}

-(NSString*)getIin{
    return iin;
}

-(NSString*)getAccountProvider{
    return accountProvider;
}

-(id)initWithAccountProviderId:(NSString *)provider_id provider:(id)provider providerIin:(NSString *)provider_Iin{

    if( self = [super init] )
    {
        providerId = provider_id;
        accountProvider = provider;
        iin = provider_Iin;
    }

    return self;
}


@end
