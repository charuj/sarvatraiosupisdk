//
//  LinkableTransaction.m
//  SrvtFramework
//
//  Created by Charushila on 04/08/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "LinkableTransaction.h"
#import "CommonUtils.h"
#import "BharatQr.h"

@implementation LinkableTransaction

@synthesize t_mode;


-(id)initWith:(NSString*)payee_VPA  payeeName:(NSString*)payee_Name {

    if( self = [super init] )
    {
        payeeVPA = payee_VPA;
        payeeName = payee_Name;
        t_mode = Simple;
    }
    return self;
}

-(id)initWithPayeeName:(NSString*)payee_VPA  payeeName:(NSString*)payee_Name mode:(int)mode{
    if( self = [super init] )
    {
        payeeVPA = payee_VPA;
        payeeName = payee_Name;
        t_mode = mode;
        payeeMerchantCode = @"0000";
        currencyCode = @"INR";
    }
    return self;

}



-(BOOL)validate{

    CommonUtils *commonUtils = [[CommonUtils alloc ] init];
    [commonUtils notNull:[self getPayeeVPA] name:@"Payee VPA"];
    [commonUtils notNull:[self getPayeeName] name:@"Payee Name"];

    if (t_mode == Dynamic) {
        [commonUtils notNull:[self getAmount] name:@"Amount"];
        [commonUtils notNull:[self getTransactionReferenceID] name:@"Transaction Reference ID"];
        [commonUtils equals:@"INR" value2:[self getCurrencyCode] name:@"Currency Code"];

    }
    return true;

}


-(NSString*)getPayeeVPA{
    return payeeVPA;
}



-(void)setPayeeVPA : (NSString*)payee_VPA{
    payeeVPA = payee_VPA;
}



-(NSString*)getPayeeName{
    return payeeName;
}



-(void)setPayeeName : (NSString*)payee_Name{
    payeeName = payee_Name;
}


-(NSString*)getPayeeMerchantCode{

    return payeeMerchantCode;
}


-(void)setPayeeMerchantCode : (NSString*)payee_Merchant_Code{
    payeeMerchantCode = payee_Merchant_Code;
}


-(NSString*)getTransactionID{
    return transactionID;
}


-(void)setTransactionID : (NSString*)transaction_ID{
    transactionID = transaction_ID;
}


-(NSString*)getTransactionReferenceID{
    return transactionReferenceID;
}


-(void)setTransactionReferenceID : (NSString*)transaction_ReferenceID{
    transactionReferenceID = transaction_ReferenceID;
}


-(NSString*)getRemarks{
    return remarks;
}


-(void)setRemarks : (NSString*)transaction_remarks{
    remarks = transaction_remarks;
}


-(NSString*)getCurrencyCode{
    return currencyCode;
}


-(void)setCurrencyCode : (NSString*)currency_Code{
    currencyCode = currency_Code;
}


-(NSString*)getTransactionDetailsUrl{
    return transactionDetailsUrl;
}


-(void)setTransactionDetailsUrl : (NSString*)transaction_DetailsUrl{
    transactionDetailsUrl = transaction_DetailsUrl;
}


-(NSDecimalNumber*)getAmount{

    return amount;
}


-(void)setAmount : (NSDecimalNumber*)transaction_amount{

    amount = transaction_amount;
}


-(NSDecimalNumber*)getMinimumAmount{
    return minimumAmount;
}

-(void)setMinimumAmount : (NSDecimalNumber*)minimum_Amount{
    minimumAmount = minimum_Amount;
}

-(NSString*)getAccNo{
    return accNo;
}

-(void)setAccNo:(NSString *)acc_No{
    accNo = acc_No;
}

-(NSString*)getIfsc{
    return ifsc;
}

-(void)setIfsc:(NSString *)ifsc_Code{
    ifsc = ifsc_Code;
}

-(NSString*)getAadharNo{
    return aadharNo;
}


-(void)setAadharNo:(NSString *)aadhar_no{
    aadharNo = aadhar_no;
}


- (NSString *)toIntentData : (LinkableTransaction *)transaction{

    NSString *transUrl =@"upi://pay?";
    NSString* trans_amount;
    NSString* min_amount;
    NSString* curCode;


    transUrl = [transUrl stringByAppendingFormat:@"pa=%@&pn=%@&mc=%@&tid=%@&tr=%@&tn=%@",transaction.getPayeeVPA , transaction.getPayeeName ,transaction.getPayeeMerchantCode,transaction.getTransactionID , transaction.getTransactionReferenceID,transaction.getRemarks];

    if (transaction.getAmount != NULL || transaction != nil) {
        trans_amount = [NSString stringWithFormat:@"%.2f",[transaction.getAmount doubleValue]];
        curCode = transaction.getCurrencyCode;

    }else{
        trans_amount = @"";
        curCode = @"";

    }

    if (transaction.getMinimumAmount != NULL || transaction != nil) {
        min_amount = [NSString stringWithFormat:@"%.2f",[transaction.getMinimumAmount doubleValue]];

    }else{
        min_amount = @"";
    }

    transUrl = [transUrl stringByAppendingFormat:@"&am=%@&mam=%@&cu=%@&url=%@",trans_amount,min_amount,curCode,transaction.getTransactionDetailsUrl];

    NSLog(@"Qr code generate string : %@",transUrl);

    return transUrl;

}

- (LinkableTransaction *)parseIntentData : (NSString *)transactionUrl{

    NSLog(@"parseIntentData transactionUrl : %@",transactionUrl);

    LinkableTransaction *linkableTransaction = [[LinkableTransaction alloc]init];

    if([transactionUrl hasPrefix:@"upi://"]){

        NSArray *arr1 = [transactionUrl componentsSeparatedByString:@"?"];

        if(arr1.count>1){

            NSString *url = arr1[1];

            NSArray *separateArr = [url componentsSeparatedByString:@"&"];

            for(int i=0;i<separateArr.count ; i++){

                NSArray *splitedArr = [separateArr[i] componentsSeparatedByString:@"="];

                for(int j=0;j<splitedArr.count ; j++){

                    NSString *ch = splitedArr[0];

                    if([ch isEqualToString:@"pa"]){

                        [linkableTransaction setPayeeVPA:splitedArr[1]];

                    }else if([ch isEqualToString:@"pn"]){

                        [linkableTransaction setPayeeName:splitedArr[1]];

                    }else if([ch isEqualToString:@"mc"]){

                        [linkableTransaction setPayeeMerchantCode:splitedArr[1]];

                    }else if([ch isEqualToString:@"tid"]){

                        [linkableTransaction setTransactionID:splitedArr[1]];

                    }else if([ch isEqualToString:@"tr"]){

                        [linkableTransaction setTransactionReferenceID:splitedArr[1]];

                    }else if([ch isEqualToString:@"tn"]){

                        [linkableTransaction setRemarks:splitedArr[1]];

                    }else if([ch isEqualToString:@"am"]){

                        NSString *amt = splitedArr[1];
                        if (![amt containsString:@"."]) {
                            amt = [amt stringByAppendingString:@".00"];
                        }
                        [linkableTransaction setAmount:[NSDecimalNumber decimalNumberWithString:amt]];
                        
                    }else if([ch isEqualToString:@"cu"]){
                        
                        [linkableTransaction setCurrencyCode:splitedArr[1]];
                        
                    }else if([ch isEqualToString:@"mam"]){
                        
                        [linkableTransaction setMinimumAmount:splitedArr[1]];
                    }else if([ch isEqualToString:@"url"]){

                        [linkableTransaction setTransactionDetailsUrl:splitedArr[1]];
                    }
                }
            }
        }

    }else{

        BharatQr *bharatQr = [[BharatQr alloc]init];

        NSMutableArray *outputArray = [NSMutableArray array];

        outputArray = [bharatQr parse:transactionUrl];

        linkableTransaction = [bharatQr handleBharatQrData:outputArray linkableTransaction:linkableTransaction];

        NSLog(@"Bharat QR getPayeeVPA : %@",linkableTransaction.getPayeeVPA);

    }


    return linkableTransaction;

}


-(void)sharedWithPSP : (LinkableTransaction *)linkableTransaction{

    NSString * url = [self toIntentData:linkableTransaction];

    url = [url stringByReplacingOccurrencesOfString:@" " withString:@"%20"];

    NSURL *URL = [NSURL URLWithString:url];

//    UIApplication *application = [UIApplication sharedApplication];
//    [application openURL:URL];

   // [[UIApplication sharedApplication] openURL:URL options:@{} completionHandler:nil];
    UIApplication *application = [UIApplication sharedApplication];
    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
        if (success) {
            NSLog(@"Test : success case");
         //   completionBlock(YES);
        }else{
          //  completionBlock(NO);
            NSLog(@"Test : Failed case");
        }
    }];
}


-(NSString*)generateBharatQrCode : (LinkableTransaction*)linkableTransaction{

    NSString* bharatQrString = @"";


   // NSMutableString* bharatQr = @"000201010212021647250010000000120415526550000000001061661000900000000310823ABCD0001238123801519337";

    NSMutableString* bharatQr = [NSMutableString stringWithFormat:@"%@",@"00020101021202164725001000000012041552655000000000106166100090000000031"];
    NSString* trans_amount;
    NSString* min_amount;

    if (linkableTransaction.getMinimumAmount != NULL) {
        min_amount = [NSString stringWithFormat:@"%.2f",[linkableTransaction.getMinimumAmount doubleValue]];
    }

    if (linkableTransaction.getIfsc !=NULL && linkableTransaction.getAccNo !=NULL ) {
        NSString* str = [NSString stringWithFormat:@"%@%@",linkableTransaction.getIfsc,linkableTransaction.getAccNo];
        bharatQr = [bharatQr stringByAppendingFormat:@"%@%lu%@",@"08",(unsigned long)str.length,str];
    }


    if (linkableTransaction.getPayeeVPA !=NULL) {
        NSString* len = @"";

        if(min_amount.length < 10){
            len = [NSString stringWithFormat:@"%@%lu",@"0",(unsigned long)min_amount.length];
        }else{
            len = [NSString stringWithFormat:@"%lu",(unsigned long)min_amount.length];

        }

        NSString* str = [NSString stringWithFormat:@"%@%lu%@%@%@%@",@"0010A00000052401",(unsigned long)linkableTransaction.getPayeeVPA.length,linkableTransaction.getPayeeVPA,@"02",len,min_amount];




        bharatQr = [bharatQr stringByAppendingFormat:@"%@%lu%@",@"26",(unsigned long)str.length,str];

    }

    if (linkableTransaction.getTransactionReferenceID !=NULL) {
        NSString* str = [NSString stringWithFormat:@"%@%lu%@%@%lu%@",@"0010A00000052401",linkableTransaction.getTransactionReferenceID.length,linkableTransaction.getTransactionReferenceID,@"02",linkableTransaction.getTransactionDetailsUrl.length,linkableTransaction.getTransactionDetailsUrl];
        bharatQr = [bharatQr stringByAppendingFormat:@"%@%lu%@",@"27",(unsigned long)str.length,str];
    }

    if (linkableTransaction.getAadharNo !=NULL) {
        NSString* str = [NSString stringWithFormat:@"%@%lu%@",@"0010A00000052401",linkableTransaction.getAadharNo.length,linkableTransaction.getAadharNo];
        bharatQr = [bharatQr stringByAppendingFormat:@"%@%lu%@",@"28",(unsigned long)str.length,str];
    }

    if (linkableTransaction.getPayeeMerchantCode !=NULL) {
        bharatQr = [bharatQr stringByAppendingFormat:@"%@%lu%@",@"520",(unsigned long)linkableTransaction.getPayeeMerchantCode.length,linkableTransaction.getPayeeMerchantCode];
    }

    if (linkableTransaction.getCurrencyCode !=NULL) {
        if([linkableTransaction.getCurrencyCode isEqualToString:@"INR"]){
            bharatQr = [bharatQr stringByAppendingFormat:@"%@%lu%@",@"530",(unsigned long)linkableTransaction.getCurrencyCode.length,@"356"];
        }
    }

    if (linkableTransaction.getAmount != NULL) {
        trans_amount = [NSString stringWithFormat:@"%.2f",[linkableTransaction.getAmount doubleValue]];
        NSString* len = @"";

        if(trans_amount.length < 10){
            len = [NSString stringWithFormat:@"%@%lu",@"0",(unsigned long)trans_amount.length];
        }else{
            len = [NSString stringWithFormat:@"%lu",(unsigned long)trans_amount.length];

        }

        bharatQr = [bharatQr stringByAppendingFormat:@"%@%@%@",@"54",len,trans_amount];

    }

    if (linkableTransaction.getPayeeName !=NULL) {

            bharatQr = [bharatQr stringByAppendingFormat:@"%@%lu%@",@"59",(unsigned long)linkableTransaction.getPayeeName.length,linkableTransaction.getPayeeName];
    }

    if (linkableTransaction.getAadharNo !=NULL) {
        NSString* len = @"";

        if(linkableTransaction.getRemarks.length < 10){
            len = [NSString stringWithFormat:@"%@%lu",@"0",(unsigned long)linkableTransaction.getRemarks.length];
        }else{
            len = [NSString stringWithFormat:@"%lu",(unsigned long)linkableTransaction.getRemarks.length];

        }
        NSString* str = [NSString stringWithFormat:@"%@%@%@",@"0203***0403***0603***07080000000308",len,linkableTransaction.getRemarks];


        bharatQr = [bharatQr stringByAppendingFormat:@"%@%lu%@",@"62",(unsigned long)str.length,str];
    }






    bharatQrString = [NSString stringWithFormat:@"%@",bharatQr];

    NSLog(@"bharatQrString : %@",bharatQrString);

    return bharatQrString;


}



@end
