//
//  PendingTransactionInfo.m
//  SrvtFramework
//
//  Created by Charushila on 31/07/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "PendingTransactionInfo.h"

@implementation PendingTransactionInfo

@synthesize direction;
@synthesize date;
@synthesize expireAfter;
@synthesize tranLogID;
@synthesize transactionItem;


-(id)initWith :(NSDictionary*)pendingDetails{
    if( self = [super init] )
    {
        transactionItem = [TransactionItem alloc];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];

        direction = [pendingDetails objectForKey:@"direction"];

        [dateFormat setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
        expireAfter = [dateFormat dateFromString:[pendingDetails objectForKey:@"expireAfter"]];

        [dateFormat setDateFormat:@"yyyyMMddHHmmss"];
        date = [dateFormat dateFromString:[pendingDetails objectForKey:@"date"]];


        tranLogID =[pendingDetails objectForKey:@"upiTranlogId"];

         transactionItem = [transactionItem initWith:[pendingDetails objectForKey:@"payeeVa"] payer_Va:[pendingDetails objectForKey:@"payerVa"] transac_Remark:[pendingDetails objectForKey:@"note"] transac_Amount:[pendingDetails objectForKey:@"amount"] transaction_Type:[pendingDetails objectForKey:@"type"] transaction_status:[pendingDetails objectForKey:@"status"] transaction_SeqNo:[pendingDetails objectForKey:@"seqNo"]];


        /*
         amount = 20;
         date = 20170802122149;
         direction = INITIATED;
         expireAfter = "2017-08-0215:41:49";
         note = Remark;
         payeeVa = "charu23@equitas";
         payerVa = "charu21@equitas";
         seqNo = UNWPmXkplcu29iMU6R78OEwtedhz5xwsY5i;
         status = PENDING;
         type = COLLECT;
         upiTranlogId = 31745;
         
         */
    }
    return  self;
}

-(NSString*) getDirection{
    return direction;
}

-(NSDate*)getExpireAfter{
    return expireAfter;
}

-(NSDate*) getDate{
    return date;
}

-(NSString*) getTranLogID{
    return  tranLogID;
}

-(NSString *)getPayeeVa{
   return transactionItem.payeeVa;
}

-(NSString *)getPayerVa{
    return transactionItem.payerVa;

}
-(NSString *)getAmount{
    return transactionItem.amount;
}

-(NSString *)getSeqNo{
    return transactionItem.seqNo;

}

-(NSString *)getRemark{
    return transactionItem.remark;

}

-(NSString *)getTransactionType{
    return transactionItem.transactionType;

}

-(NSString *)getStatus{
    return transactionItem.status;

}


@end
