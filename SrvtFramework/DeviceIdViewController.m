//
//  DeviceIdViewController.m
//  TestCommonSdkApp
//
//  Created by Charushila on 05/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "DeviceIdViewController.h"

static NSString * const KeychainItem_Service = @"FDKeychain";
static NSString * const KeychainItem_UUID = @"Local";
@interface DeviceIdViewController ()

@end

@implementation DeviceIdViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //NSString* Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
    
    
//    NSString *UUID = [self generateUUID];
//    NSLog(@"Device Id is : %@", UUID);
    
}

#pragma Create UUID
-(NSString *)generateUUID {
    NSString *CFUUID = nil;
    
    if (![FDKeychain itemForKey: KeychainItem_UUID
                     forService: KeychainItem_Service
                          error: nil]) {
        //create CFUUDID.
        CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
        
        CFUUID = (NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuid));
        
        //[[NSUserDefaults standardUserDefaults] setObject:CFUUID forKey:@"UUID"];
        
        [FDKeychain saveItem: CFUUID
                      forKey: KeychainItem_UUID
                  forService: KeychainItem_Service
                       error: nil];
        
    } else {
        // CFUUID = [[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"];
        CFUUID = [FDKeychain itemForKey: KeychainItem_UUID
                             forService: KeychainItem_Service
                                  error: nil];
    }
    
    return CFUUID;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
