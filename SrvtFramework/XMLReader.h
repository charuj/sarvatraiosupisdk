//
//  XMLReader.h
//  SrvtFramework
//
//  Created by Charushila on 29/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XMLReader : NSObject

{
    NSMutableArray *dictionaryStack;
    NSMutableString *textInProgress;
    NSError * __weak *errorPointer;
}

+ (NSDictionary *)dictionaryForXMLData:(NSData *)data error:(NSError **)errorPointer;
+ (NSDictionary *)dictionaryForXMLString:(NSString *)string error:(NSError **)errorPointer;
@end
