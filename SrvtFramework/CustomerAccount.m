//
//  CustomerAccount.m
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "CustomerAccount.h"

@implementation CustomerAccount


@synthesize  accountProvider;
@synthesize provider;

-(AccountProvider*) getProvider{
    return provider;
}

/**
 * Get Account MMID
 *
 * @return MMID
 */
-(NSString *)getMmid{
    return mmid;
}

/**
 * Get Account Number
 *
 * @return Account Number
 */
-(NSString *)getAccount{
    return account;
}

/**
 * Get IFSC of the customer account
 *
 * @return IFSC
 */
-(NSString *)getIfsc{
    return ifsc;
}
/**
 * Get Name as per Bank records
 *
 * @return Name
 */
-(NSString *)getName{
    return name;
}

/**
 * Get if Mobile Banking is enabled on this Record. Host Application can show "Change MPIN" or "RESET MPIN" option if value is true. User Can use existing MPIN.
 *
 * @return true if mobile banking is enabled, Null if data not available.
 */
-(BOOL)getMobileBankingEnabled{
    return mobileBankingEnabled;
}
/**
 * Get if Aadhaar is mapped(available) as per bank record for this account.
 *
 * @return true if Aadhaar banking is present, Null if data not available.
 */
-(BOOL)getAadhaarEnabled{
    return aadhaarEnabled;
}

/**
 * Get Account Provider information
 *
 * @return Account Provider
 */
-(NSString*) getAccountProvider{
    return provider;
}

/**
 * Get Account Type (Savings/Current..)
 *
 * @return Account Type
 */
-(NSString *)getAccountType{
    return accountType;
}

/**
 * Get Masked account number
 *
 * @return Masked account number
 */
-(NSString *)getMaskedAccount{
    return maskedAccount;
}

/**
 * Get if this account is default Credit account set for the Virtual Address
 *
 * @return true if credit account, Null if data not available. False if account is not default Credit.
 */
-(BOOL)getDefaultCredit{
    return defaultCredit;
}

/**
 * Get if this account is default Debit account set for the Virtual Address
 *
 * @return true if debit account, Null if data not available. False if account is not default Debit.
 */
-(BOOL)getDefaultDebit{
    return defaultDebit;
}


/**
 * Credential Configuration Data (internal use)
 *
 * @return Credential Data
 */
-(NSString *)getCredentialRequired{
    return credentialRequired;
}

/**
 * Credential Configuration Data (internal use)
 *
 * @return Credential Data
 */
-(NSDictionary *)getCredentialData{
    return credentialData;
}

/**
 * Credential Configuration Data (internal use)
 *
 * @return Credential Data
 */
-(NSDictionary *)getCredArr{
    return credDict;
}

-(NSString *)getId{
    return providerid;
}

-(id)collectCustomerDetails : (NSDictionary*)details provider:(AccountProvider*)accprovider{
    NSError* error = nil;
    provider = [[AccountProvider alloc] init];

    mmid = [details objectForKey:@"mmid"];
    name = [details objectForKey:@"name"];
    ifsc = [details objectForKey:@"ifsc"];
    accountType = [details objectForKey:@"accountType"];
    account = [details objectForKey:@"accRefNumber"];
    maskedAccount = [details objectForKey:@"account"];
    credentialData = [details objectForKey:@"CredsAllowed"];

    NSString*aeba = [details objectForKey:@"aeba"];
    NSString*mbeba = [details objectForKey:@"mbeba"];
    accountProvider = accprovider.accountProvider;
    providerid = accprovider.getId;

    [provider setProviderId:accprovider.getId];
    [provider setAccountProvider:accprovider.accountProvider];
    [provider setIin:accprovider.iin];


    if([aeba isEqualToString:@"Y"]) {
        aadhaarEnabled = true;
    }else{
        aadhaarEnabled = false;
    }
    if([mbeba isEqualToString:@"Y"]) {
        mobileBankingEnabled = true;
    }else{
        mobileBankingEnabled = false;
    }

    credArr = [credentialData objectForKey:@"Child"];

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:credArr options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    credentialRequired = jsonString;

    NSString* str = [self credential:credArr];
    str = [self formatString:str];
    credDict  = [self createDict:str];

    return self;
}


- (NSDictionary*)createDict :(NSString*)str{

  //  NSString *jsonString = @"{\"ID\":{\"Content\":268,\"type\":\"text\"},\"ContractTemplateID\":{\"Content\":65,\"type\":\"text\"}}";
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    return json;
}




-(NSString*)credential:(NSMutableDictionary *)cred{
    NSString* type;
    NSString* subtype;
    NSString* dType;
    NSString* dLength;
    NSString* credentialStr;
    NSMutableArray *credDictionary   = [[NSMutableArray alloc] init];
    credDictionary = [cred mutableCopy];

   NSMutableString* str = [NSMutableString stringWithFormat:@"%@",@"{\"CredAllowed\":["];

    for(id keys in cred){

        for(id key in keys){
            if ([key isEqualToString:@"CredsAllowedDLength"]){
                dLength = [keys objectForKey:key];
            }else if([key isEqualToString:@"dLength"]){
                dLength = [keys objectForKey:@"dLength"];
            }

            if ([key isEqualToString:@"CredsAllowedDType"]){
                if ([[keys objectForKey:key] isEqualToString:@"Numeric"] ) {
                    dType = @"NUM";
                }else{
                    dType = @"ALPH";
                }
            }
            if ([key isEqualToString:@"CredsAllowedSubType"]){
                subtype = [keys objectForKey:key];
            }
            if ([key isEqualToString:@"CredsAllowedType"]){
                type = [keys objectForKey:key];
            }
        }

        str = [str stringByAppendingFormat:@"{\"type\":\"%@\",\"subtype\":\"%@\",\"dType\":\"%@\",\"dLength\":%@ },",type,subtype,dType,dLength];

    }

    credentialStr = [NSString stringWithFormat:@"%@",str];
    return credentialStr;

}

-(NSString*)creadString : (NSString*)str{

    NSMutableString* str1 = [NSMutableString stringWithString:str];

    NSMutableString* str2 = [NSMutableString stringWithFormat:@"%@",@"{\"CredAllowed\":"];

    str1 = [str1 stringByReplacingOccurrencesOfString:@"]" withString:@"]}"];

    str2 = [str2 stringByAppendingString:str1];

    return str2;
}


/*-(id)initWithAccountDetails:(NSDictionary *)mappedcustomerAccount{

    //va = "asusmerchant3@purz";
    NSLog(@"mappedcustomerAccount : NSDictionary : %@",mappedcustomerAccount);

    providerid = [mappedcustomerAccount objectForKey:@"account-provider"];
    name = [mappedcustomerAccount objectForKey:@"customer-name"];
    ifsc = [mappedcustomerAccount objectForKey:@"ifsc"];
    accountType = [mappedcustomerAccount objectForKey:@"account-type"];
    account = [mappedcustomerAccount objectForKey:@"account-ref-no"];
    maskedAccount = [mappedcustomerAccount objectForKey:@"account"];

    NSString*defCredit = [mappedcustomerAccount objectForKey:@"default-credit"];
    NSString*defDebit = [mappedcustomerAccount objectForKey:@"default-debit"];

    accountProvider = [mappedcustomerAccount objectForKey:@"account-provider-name"];

    if([mappedcustomerAccount objectForKey:@"mmid"]){
        mmid = [mappedcustomerAccount objectForKey:@"mmid"];
    }



    if([defDebit isEqualToString:@"D"]) {
        defaultDebit = true;
    }else{
        defaultDebit = false;
    }
    if([defCredit isEqualToString:@"D"]) {
        defaultCredit = true;
    }else{
        defaultCredit = false;
    }

    if ([mappedcustomerAccount objectForKey:@"CredsAllowed"]) {
        credentialData = [mappedcustomerAccount objectForKey:@"CredsAllowed"];
        credArr = [credentialData objectForKey:@"Child"];

        credentialRequired = [self credentialData:credArr];
        credentialRequired = [self formatStr:credentialRequired];

        NSLog(@"credentialRequired : %@ ",credentialRequired);

        NSString* str = [self credential:credArr];
        str = [self formatString:str];
        credDict  = [self createDict:str];


    }else{
        credentialRequired = [mappedcustomerAccount objectForKey:@"credsAllowedJson"];
        NSLog(@"Before credentialRequired : %@ ",credentialRequired);

       // credentialRequired = [self creadString:credentialRequired];
        NSMutableString *str = [self creadString:credentialRequired];
        NSLog(@"after credentialRequired : %@ ",str);

        NSDictionary *cr = [self createDict:str];

        NSLog(@"creadString : NSDictionary cr : %@",cr);
        NSLog(@"creadString : NSDictionary cr : %@",[cr objectForKey:@"CredAllowed"]);

        NSDictionary *cread = [cr objectForKey:@"CredAllowed"];
        NSLog(@"creadString : NSDictionary cread : %@",cread);

        NSString* strCrd = [self credential:cread];
        
        NSLog(@"creadString :  strCrd : %@",strCrd);
        NSString* crdStr = [self formatString:strCrd];
        credDict  = [self createDict:crdStr];

    }

    return self;

}*/



-(id)initWithAccountDetails:(NSDictionary *)mappedcustomerAccount{

    NSDictionary* customer;
    NSDictionary* acc_Provider;

    if([mappedcustomerAccount objectForKey:@"customerDetails"]){
        customer = [mappedcustomerAccount objectForKey:@"customerDetails"];
    }else{
        customer = mappedcustomerAccount;
    }

    if([mappedcustomerAccount objectForKey:@"accountProvider"]){
        acc_Provider = [mappedcustomerAccount objectForKey:@"accountProvider"];
    }


    NSString*defCredit ;
    NSString*defDebit ;
    provider = [[AccountProvider alloc] init];

    if([customer objectForKey:@"mmid"]){
        mmid = [customer objectForKey:@"mmid"];
    }

    if([customer objectForKey:@"name"]){
        name = [customer objectForKey:@"name"];
    }

    if([customer objectForKey:@"customer-name"]){
        name = [customer objectForKey:@"customer-name"];
    }

    if([customer objectForKey:@"ifsc"]){
        ifsc = [customer objectForKey:@"ifsc"];
    }

    if([customer objectForKey:@"accountType"]){
        accountType = [customer objectForKey:@"accountType"];
    }

    if([customer objectForKey:@"account-type"]){
        accountType = [customer objectForKey:@"account-type"];
    }

    if([customer objectForKey:@"accRefNumber"]){
        account = [customer objectForKey:@"accRefNumber"];
    }

    if([customer objectForKey:@"account-ref-no"]){
        account = [mappedcustomerAccount objectForKey:@"account-ref-no"];
    }

    if([customer objectForKey:@"account"]){
        maskedAccount = [customer objectForKey:@"account"];
    }

    if([customer objectForKey:@"aeba"]){
        NSString*aeba = [customer objectForKey:@"aeba"];

        if([aeba isEqualToString:@"Y"]) {
            aadhaarEnabled = true;
        }else{
            aadhaarEnabled = false;
        }
    }

    if([acc_Provider objectForKey:@"account-provider"]){
        [provider setProviderId:[acc_Provider objectForKey:@"account-provider"]];

    }else if([customer objectForKey:@"account-provider"]){
        [provider setProviderId:[customer objectForKey:@"account-provider"]];
    }

    if([customer objectForKey:@"default-credit"]){
        defCredit = [customer objectForKey:@"default-credit"];
    }

    if([customer objectForKey:@"default-debit"]){
        defDebit = [customer objectForKey:@"default-debit"];
    }

    if([defDebit isEqualToString:@"D"]) {
        defaultDebit = true;
    }else{
        defaultDebit = false;
    }
    if([defCredit isEqualToString:@"D"]) {
        defaultCredit = true;
    }else{
        defaultCredit = false;
    }

    if([customer objectForKey:@"mbeba"]){
        NSString*mbeba = [customer objectForKey:@"mbeba"];

        if([mbeba isEqualToString:@"Y"]) {
            mobileBankingEnabled = true;
        }else{
            mobileBankingEnabled = false;
        }
    }

    if([acc_Provider objectForKey:@"account-provider-name"]){
       // provider.accountProvider = [mappedcustomerAccount objectForKey:@"account-provider-name"];
        [provider setAccountProvider:[acc_Provider objectForKey:@"account-provider-name"]];
        NSLog(@"provider name : %@",provider.getAccountProvider);
    }else if([customer objectForKey:@"account-provider-name"]){
        [provider setAccountProvider:[customer objectForKey:@"account-provider-name"]];
    }

    if([acc_Provider objectForKey:@"iin"]){
        // provider.accountProvider = [mappedcustomerAccount objectForKey:@"account-provider-name"];
        [provider setIin:[acc_Provider objectForKey:@"iin"]];
    }


    if ([customer objectForKey:@"CredsAllowed"]) {

        credentialData = [customer objectForKey:@"CredsAllowed"];
        credArr = [credentialData objectForKey:@"Child"];

        credentialRequired = [self credentialData:credArr];
        credentialRequired = [self formatStr:credentialRequired];

        NSString* str = [self credential:credArr];
        str = [self formatString:str];
        credDict  = [self createDict:str];


    }else if([customer objectForKey:@"credsAllowedJson"]){

        credentialRequired = [customer objectForKey:@"credsAllowedJson"];
        // credentialRequired = [self creadString:credentialRequired];
        NSMutableString *str = [self creadString:credentialRequired];
       // NSLog(@"after credentialRequired : %@ ",str);

        NSDictionary *cr = [self createDict:str];

        NSDictionary *cread = [cr objectForKey:@"CredAllowed"];

        NSString* strCrd = [self credential:cread];

        NSString* crdStr = [self formatString:strCrd];
        credDict  = [self createDict:crdStr];
    }

    return self;

}

-(NSString*)credentialData:(NSMutableDictionary *)cred{
    NSString* type;
    NSString* subtype;
    NSString* dType;
    NSString* dLength;
    NSString* allowedLength;

    NSString* credentialStr;

    NSMutableArray *credDictionary   = [[NSMutableArray alloc] init];
    credDictionary = [cred mutableCopy];

    NSMutableString* str = [NSMutableString stringWithFormat:@"%@",@"["];

    for(id keys in cred){

        for(id key in keys){

            if ([key isEqualToString:@"CredsAllowedType"]){
                type = [keys objectForKey:key];
            }
            if ([key isEqualToString:@"CredsAllowedDType"]){

                dType = [keys objectForKey:key];
                
            }
            if ([key isEqualToString:@"CredsAllowedSubType"]){
                subtype = [keys objectForKey:key];
            }


            if ([key isEqualToString:@"CredsAllowedDLength"]){
                allowedLength = [keys objectForKey:key];
            }

            if([key isEqualToString:@"dLength"]){
                dLength = [keys objectForKey:key];
            }
        }

        str = [str stringByAppendingFormat:@"{\"CredsAllowedType\":\"%@\",\"CredsAllowedDType\":\"%@\",\"CredsAllowedSubType\":\"%@\",\"CredsAllowedDLength\":\"%@\",\"dLength\":%@ },",type,dType,subtype,allowedLength,dLength];


    }

    credentialStr = [NSString stringWithFormat:@"%@",str];

    return credentialStr;
    
}

-(NSString*)formatStr :(NSString*)str{

    NSRange lastComma = [str rangeOfString:@"," options:NSBackwardsSearch];

    if(lastComma.location != NSNotFound) {
        str = [str stringByReplacingCharactersInRange:lastComma
                                           withString: @"]"];
    }

    return str;
}
-(NSString*)formatString :(NSString*)str{

    NSRange lastComma = [str rangeOfString:@"," options:NSBackwardsSearch];

    if(lastComma.location != NSNotFound) {
        str = [str stringByReplacingCharactersInRange:lastComma
                                           withString: @"]}"];
    }

    return str;
}

-(NSString*)getSubstring:(NSString*)str{


    NSRange r1 = [str rangeOfString:@"["];
    NSRange r2 = [str rangeOfString:@"]"];
    NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location - r1.location - r1.length);
    NSString *sub = [str substringWithRange:rSub];

    return sub;
}

-(NSString*)replace : (NSMutableDictionary*)cred{

    NSMutableString* str = [NSMutableString stringWithFormat:@"%@",@"{\"CredAllowed\":["];
    for (id key in cred){
        if ([key isEqualToString:@"CredsAllowedDLength"]){

        }
    }
         str = [str stringByReplacingOccurrencesOfString:@"\"" withString:@""];

    return str;
}


@end
