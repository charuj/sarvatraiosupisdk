//
//  VirtualAddress.m
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "VirtualAddress.h"

@implementation VirtualAddress


@synthesize handle;
@synthesize custAccount;
@synthesize custAccountList;

-(NSString*)getHandle{
    return handle;
}

-(NSArray*)getCustAccountList{
    return custAccountList;

}

-(CustomerAccount*)custAccount{
    return custAccount;
}

-(id)initWith:(NSDictionary *)account handle:(NSString*)vpa_handle{
   // NSLog(@"VirtualAddress.h // account : %@",account);

    custAccount = [CustomerAccount alloc];

    if( self = [super init] )
    {
       handle = vpa_handle;
       custAccount = [custAccount initWithAccountDetails:account];

     //  NSLog(@"VirtualAddress.h // custAccount : %@",custAccount);
    }
    return self;
}

@end
