//
//  TransactionItem.m
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "TransactionItem.h"

@implementation TransactionItem

@synthesize payeeVa;
@synthesize payerVa;
@synthesize remark;
@synthesize amount;
@synthesize transactionType;
@synthesize status;
@synthesize seqNo;

-(id)initWith:(NSString*)payee_Va payer_Va:(NSString*)payer_Va transac_Remark:(NSString*)transac_Remark transac_Amount:(NSString*)transac_Amount transaction_Type:(NSString*)transaction_Type transaction_status:(NSString*)transaction_status transaction_SeqNo:(NSString*)transaction_SeqNo{

    if( self = [super init] )
    {
        payeeVa = payee_Va;
        payerVa = payer_Va;
        remark = transac_Remark;
        amount = transac_Amount;
        transactionType = transaction_Type;
        status = transaction_status;
        seqNo = transaction_SeqNo;
    }

    return self;
}


-(NSString *)getPayeeVa{
    return payeeVa;
}

-(NSString *)getPayerVa{
    return payerVa;
}

-(NSString *)getRemark{
    return remark;
}

-(NSString *)getAmount{
    return amount;
}

-(NSString *)getTransactionType{
    return transactionType;
}

-(NSString *)getStatus{
    return status;
}

-(NSString *)getSeqNo{
    return seqNo;
}




@end
