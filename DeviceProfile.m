//
//  DeviceProfile.m
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "DeviceProfile.h"
#import "DeviceIdViewController.h"


@implementation DeviceProfile

@synthesize mobile;

- (id)init {
    if (self = [super init]) {
        DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc] init];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        if ([prefs objectForKey:@"user_Id"]){
            userID = (NSString *)[prefs objectForKey:@"user_Id"];
        }else{
            userID =@"";
 
        }

        deviceID= [deviceIdController generateUUID];;
        mobile=@"";
    }
    return self;
}



-(void)setDeviceID{
    
    DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc] init];
    deviceID = [deviceIdController generateUUID];
    
}
-(NSString*)getDeviceID{
    return deviceID;
}

-(void)setMobile :(NSString*)mobileNo{
    
    mobile = mobileNo;
}
-(NSString*)getMobile{
    return  mobile;
}

-(void)setUserID{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([prefs objectForKey:@"user_Id"])
        userID = (NSString *)[prefs objectForKey:@"user_Id"];

}
-(NSString*)getUserID{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([prefs objectForKey:@"user_Id"])
        userID = (NSString *)[prefs objectForKey:@"user_Id"];
    return userID;
    
}




@end
