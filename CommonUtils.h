//
//  CommonUtils.h
//  SrvtFramework
//
//  Created by Charushila on 15/07/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SdkClientApplication.h"
#import "DeviceProfile.h"

@interface CommonUtils : NSObject{

    SdkClientApplication* clientApplication;
    DeviceProfile* deviceProfile;
}

typedef enum {Initial,Reset,Rotate} TokenType;

@property (nonatomic) TokenType tokenType;



-(void)validateTrue :(BOOL)value error : (NSString*)error;

-(void)notNull :(NSObject*)object name : (NSString*)name;

-(void)notBlank :(NSString*)value name : (NSString*)name;

-(void)maxLength :(NSString*)value length:(int)length name:(NSString*)name;

-(void)minLength :(NSString*)value length:(int)length name:(NSString*)name;

-(void)equals :(NSString*)value1 value2:(NSString*)value2 name:(NSString*)name;

-(void)aboveZero :(int)value name:(NSString*)name;





-(id)initWith:(SdkClientApplication*)clientApplication  deviceProfile:(DeviceProfile*)deviceProfile;

-(NSString*)getToken:(NSString*) challenge tokenType:(TokenType*)tokenType;

-(NSString*)byteArrayToHex:(NSArray *)byteArray;

-(NSString*)fromDate:(NSDate *)date dateFormat:(NSString*)dateFormat;

-(NSString*)createSdkToken:(DeviceProfile*) profile sdkClientApplication:(SdkClientApplication*)sdkClientApplication;

-(NSString*)fromAmount:(NSDecimalNumber*) amount;

- (NSDictionary*)createDict :(NSString*)str;
@end
