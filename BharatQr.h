//
//  BharatQr.h
//  SrvtFramework
//
//  Created by Charushila on 20/09/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LinkableTransaction.h"


@interface BharatQr : NSObject

- (NSMutableArray*)parse : (NSString*)tlvStr;
-(LinkableTransaction*)handleBharatQrData : (NSMutableArray*)array linkableTransaction:(LinkableTransaction*)linkableTransaction;

@end
