//
//  ApiProxyProtocol.m
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//


#import "ApiProxyProtocol.h"
#import <AFNetworking.h>
#import "ApiConstants.h"
#import "SrvtCommonLib.h"
#import "DeviceIdViewController.h"
#import "DeviceProfile.h"
#import "XMLReader.h"
#import "CardData.h"
#import "SdkException.h"
//#import "SdkServiceFactory.h"


@implementation ApiProxyProtocol

@synthesize userId;
@synthesize listAccountProvider;
@synthesize listKeys;
@synthesize sessionId;
@synthesize transactionId;
@synthesize channel;
@synthesize appName;
@synthesize sequenceNo;
@synthesize mobileNo;
@synthesize listMappedVirtualId;
@synthesize xmlPayload;
@synthesize serviceConnected;
@synthesize clientApplication;
@synthesize customerAccount;
@synthesize deviceProfile;




NSString* TAG = @"Sdk-API-Proxy";


NSString* sub_type = @"initial";
NSString* challenge = nil;
//SdkServiceFactory* sdkServiceFactory;


//private Object syncObject = new Object();

static NSString* session = nil;
static NSDate *sessionUpdated = nil;
//    private static final long SESSION_TIMEOUT = 1000 * 1 * 60;
static long SESSION_TIMEOUT = 10;

//private DeviceProfile deviceProfile;
 NSDictionary* paramDict;

//private final Class<T> clazz;
//private AppDataFieldType appDataFieldType;
//private String transactionID;
//private UpiResult apiResult;

+ (id)sharedManager {
    
    static ApiProxyProtocol *sharedMyManager = nil;
    @synchronized(self) {
        if (sharedMyManager == nil)
            sharedMyManager = [[self alloc] init];
    }
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        userId = @"";
        listAccountProvider=@{@"":@""};
        listKeys=@{@"":@""};
        sessionId = @"";
        transactionId = @"";
        channel = @"";
        appName = @"";
        deviceProfile = [[DeviceProfile alloc]init];
        sequenceNo = @"";
        mobileNo=@"";
        listMappedVirtualId=@{@"":@""};
        xmlPayload=@"";
        serviceConnected = false;
        clientApplication =[SdkClientApplication alloc] ;

    }
    return self;
}
-(BOOL)initSDKservice{

    serviceConnected = true;

    //    Intent serviceIntent = new Intent();
    //    serviceIntent.setAction("in.sarvatra.upisdk.UpiService");
    //    serviceIntent.setPackage(context.getPackageName());

    if(sdkConnector == nil){
        sdkConnector = [SdkConnector alloc];
        sdkConnector = [sdkConnector initWith:clientApplication deviceProfile:deviceProfile];
    }

   // NSLog(@"Sdk Service Activated..");
    return true;
    
}

/*
 * Activate Sdk and initiate common library.

 */

-(NSDictionary*)activateSdk :(NSDictionary *)parametersDict withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{


    NSLog(@"Sent parameter to parametersDict: %@",parametersDict);
    
    __block NSDictionary *dictData;
    __block NSDictionary *response;
    AFHTTPSessionManager *manager = [self setSessionManager];
    
    NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"user/activate-sdk"]];
    
    NSLog(@" user/activate-sdk   : loginUrl : %@",loginUrl);

    __block NSDictionary *params =@{@"name":[parametersDict objectForKey:@"name"],@"sdk-client-id":[parametersDict objectForKey:@"sdk-client-id"],@"email":[parametersDict objectForKey:@"email"],@"sdk-client":[parametersDict objectForKey:@"sdk-client"],@"device-id":[parametersDict objectForKey:@"device-id"],@"mobile":[parametersDict objectForKey:@"mobile"]} ; // Add your parameters
    
    //deviceProfile.mobile = [parametersDict objectForKey:@"mobile"];
    
    NSLog(@"Sent parameter to server 1 : %@",params);
    
    [manager POST:loginUrl parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"In activateSdk loginUrl =%@",loginUrl);

        NSLog(@"Response from server 1 :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        NSError* error;
        response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

        
        if([response objectForKey:@"success"]){
            
            NSLog(@" Sdk Activeted... ");
            [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@""] forKey:@"user_Id"];
            [[NSUserDefaults standardUserDefaults] synchronize];
//            [self activateCommonLib:[parametersDict objectForKey:@"app-id"] deviceId:[parametersDict objectForKey:@"device-id"] channel:[parametersDict objectForKey:@"channel-code"] sessionid:[response objectForKey:@"session-id"]];
        }
    
        if(completion){
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(YES,nil,response); // here that call when method complete
            });
        }
    }
          failure:^(NSURLSessionTask *operation, NSError *error)
     
     {
         NSLog(@"Error: %@", error);
         if(completion){
             dispatch_async(dispatch_get_main_queue(), ^{
                 completion(NO,nil,nil); // here that call when method complete
             });
         }

     }];
    
    return dictData;
}


-(AFHTTPSessionManager*)setSessionManager{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    AFSecurityPolicy* policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    
    [policy setValidatesDomainName:NO];
    
    [policy setAllowInvalidCertificates:YES];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/x-www-form-urlencoded",nil];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/plain",nil];
    
    return manager;
}

-(NSDictionary *)activateUser:(NSDictionary *)params withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{
    
    __block NSDictionary *dictData;
    AFHTTPSessionManager *manager = [self setSessionManager];
    
    NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"user/activate"]];

    
    [manager POST:loginUrl parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"Response from server 1 :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        NSError* error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"user-id"] forKey:@"userId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
       // [self activateCommonLib:[params objectForKey:@"app-id"] deviceId:[params objectForKey:@"device-id"] channel:[params objectForKey:@"channel-code"]];

        if(completion){
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(YES,nil,response); // here that call when method complete
            });
        }
    }
          failure:^(NSURLSessionTask *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         if(completion){
             dispatch_async(dispatch_get_main_queue(), ^{
                 completion(NO,nil,nil); // here that call when method complete
             });
         }
     }];
    return dictData;
}


-(NSDictionary*)loginUser:(NSDictionary*)param withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{
   // NSLog(@"loginUser mobile: %@",deviceProfile.mobile);

    
    __block NSDictionary *dictData;
    AFHTTPSessionManager *manager = [self setSessionManager];
    
        NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"user/login"]];
         NSLog(@"loginUser loginUrl: %@",loginUrl);

        ///open network
        NSDictionary *parameters =@{@"device-id":[param objectForKey:@"device-id"],@"user-id":[param objectForKey:@"user-id"],@"channel-code":[param objectForKey:@"channel-code"],@"password":[param objectForKey:@"password"],@"mobile":[param objectForKey:@"mobile"]};  // Add your parameters
        
    
    deviceProfile.mobile = [param objectForKey:@"mobile"];

    
     [manager POST:loginUrl parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            
            NSLog(@"Response from server 1 :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
            
            
//            [self activateCommonLib:[param objectForKey:@"app-id"] deviceId:[param objectForKey:@"device-id"] channel:[param objectForKey:@"channel-code"] sessionid:[response objectForKey:@"session-id"]];
            
            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }
            
        }
              failure:^(NSURLSessionTask *operation, NSError *error)
         
         {
             NSLog(@"Error: %@", error);
             if(completion){
                 dispatch_async(dispatch_get_main_queue(), ^{
                     completion(YES,nil,nil); // here that call when method complete
                 });
             }
             
         }];
        
    
    return dictData;
}

-(void)activateCommonLib :(NSString*)appId deviceId:(NSString*)deviceId channel:(NSString*)channelcode sessionid:(NSString*)sessionid userId:(NSString*)userid mobile:(NSString*)mobileno seqNo:(NSString*)seqNo{
    
    // Call common lib framework
    
    NSString* token = nil;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    SrvtCommonLib* srvt = [[SrvtCommonLib alloc]init];
    NSMutableString *mutableChallenge = [[NSMutableString alloc ]init];
    userId = (NSString *)[prefs objectForKey:@"user_Id"];
   NSLog(@"common lib userId Clservice  : %@", userId);

    if ([prefs objectForKey:@"token"])
    {

        token = (NSString *)[prefs objectForKey:@"token"];

        NSString *previous_date = (NSString *)[prefs objectForKey:@"token_date"];
        //previous_date = @"2017-04-30 12:31:45";

        NSDate *currentDate = [NSDate date];

        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormat dateFromString:previous_date];

        NSString *today = [dateFormat stringFromDate:currentDate];

//        NSLog(@" previous_date  : %@", previous_date);
//        NSLog(@" previous_date > date : %@", date);
//        NSLog(@" previous_date > date today : %@", today);


        NSTimeInterval interval = [[dateFormat dateFromString:today] timeIntervalSinceDate:date];
      //  NSLog(@" interval  : %f", interval);

        if (interval>2592000) ///interval for 30 days
        {
          //  NSLog(@"---------------- sub_type Rotate ----------------------- \n");

            sub_type=@"rotate";
            challenge = [srvt getChallenge:sub_type device_Id:deviceId app_Id:appId];
            challenge = [srvt getChallenge:sub_type device_Id:deviceId app_Id:appId];
            NSString *regexToReplace = @"/\r?\n|\r/g";
            NSError *error = NULL;
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexToReplace
                                                                                   options:NSRegularExpressionCaseInsensitive
                                                                                     error:&error];
           // NSString *modifiedString = [regex stringByReplacingMatchesInString:string
//                                                                       options:0
//                                                                         range:NSMakeRange(0, [string length])
//                                                                  withTemplate:@"$1"];

            challenge = [regex stringByReplacingMatchesInString:challenge options:0 range:NSMakeRange(0, [challenge length]) withTemplate:@"$1"];
           // challenge = [challenge stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            [mutableChallenge appendString:@"NPCI,20150822,"];
            [mutableChallenge appendString:challenge];

//            NSLog(@"regex challenge : %@",challenge);

            NSDictionary *parameters =@{@"device-id":deviceId,@"app-id":appId,@"channel-code":channelcode,@"session-id":sessionid,@"seq-no":seqNo,@"challenge":mutableChallenge,@"sub-type":sub_type,@"user-id":userId,@"mobile":mobileno,};  // Add your parameters

            [self registerapp:parameters];

        }else{
          //  NSLog(@"---------------- sub_type initial ----------------------- \n");

            sub_type=@"initial";
        }

    }else{
        //NSLog(@"---------------- toekn nil ----------------------- \n");

      //  NSLog(@"common lib userId Clservice  : %@", userId);

        sub_type=@"initial";
        challenge = [srvt getChallenge:sub_type device_Id:deviceId app_Id:appId];

        @try {

            if ([challenge isEqualToString:@""]) {

                @throw [[SdkException alloc] initWithName:@"Challenge" reason:@"Error while getting challenge." userInfo:nil];

            }else{

                NSLog(@"challenge Clservice  : %@", challenge);

                [mutableChallenge appendString:@"NPCI,20150822,"];
                [mutableChallenge appendString:challenge];

                NSDictionary *parameters =@{@"device-id":deviceId,@"app-id":appId,@"channel-code":channelcode,@"session-id":sessionid,@"seq-no":seqNo,@"challenge":mutableChallenge,@"sub-type":sub_type,@"user-id":userId,@"mobile":mobileno};  // Add your parameters

                [self registerapp:parameters];
            }

        }@catch ( NSException *e ) {
            NSLog(@"Exception : %@",e);
        }
    }
}

-(void)registerapp:(NSDictionary*)param{

    NSLog(@"---------------- registerapp param :- %@ \n",param);

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    SrvtCommonLib* srvt = [[SrvtCommonLib alloc]init];


    [self getToken:param withCompletion:^(BOOL success, NSError *error, id response) {

    @try {

        if(success){
          //  NSLog(@"get Token + responce ==  %@",response);
            NSString* mobileData = [response objectForKey:@"MobileAppData"];
            NSError *parseError = nil;
            NSData *data = [mobileData dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLData:data error:&parseError];

            NSDictionary* keys = [xmlDictionary valueForKeyPath:@"ns2:RespListKeys.keyList.key.keyValue"];

            NSString* token = [keys objectForKey:@"text"];
            NSLog(@"get token++ Token Text string ==>>%@",token); // here you get response once method camplete
           // token = [token stringByReplacingOccurrencesOfString:@"\n" withString:@""];

            // Register mobile in common lib.

            BOOL registerAppSuccess =  [srvt registerApp:[param objectForKey:@"app-id"] mobile_no:[param objectForKey:@"mobile"] device_Id:[param objectForKey:@"device-id"] token:token];

            NSLog(registerAppSuccess ? @"App Registered : Yes" : @"App Registered : No");

            if(registerAppSuccess){
                [prefs setObject:token forKey:@"token"];

                NSDate *today = [NSDate date];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSString *dateString = [dateFormat stringFromDate:today];
               // NSLog(@"date: %@", dateString);

                [prefs setObject:dateString forKey:@"token_date"];

                [[NSUserDefaults standardUserDefaults] synchronize];

            }
            
        }else if(error!= NULL){
            NSLog(@"Error ==  %@",error);
        }

   }@catch ( NSException *e ) {
        NSLog(@"Exception : %@",e);
   }

}];

}



-(void)getToken:(NSDictionary *)params withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{
    
   // __block NSDictionary *dictData;
    AFHTTPSessionManager *manager = [self setSessionManager];
 //   NSLog(@"Response from server 1 getToken params ==  %@",params);



    NSMutableString *tokenUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/get-token"]];
    
    [manager POST:tokenUrl parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"Response from server getToken :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        NSError* error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

//        NSString* mobileData = [response objectForKey:@"MobileAppData"];
//        NSError *parseError = nil;
//        NSData *data = [mobileData dataUsingEncoding:NSUTF8StringEncoding];
//        NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLData:data error:&parseError];
//
//        NSDictionary* keys = [xmlDictionary valueForKeyPath:@"ns2:RespListKeys.keyList.key.keyValue"];
//
//        NSString* toekn = [keys objectForKey:@"text"];
//        NSLog(@"xmlDictionary2 Token Text string ==>>  %@",toekn); // here you get response once method camplete
//
//        [[NSUserDefaults standardUserDefaults] setObject:toekn forKey:@"token"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        

        if(completion){
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(YES,nil,response); // here that call when method complete
            });
        }
    }
          failure:^(NSURLSessionTask *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         if(completion){
             dispatch_async(dispatch_get_main_queue(), ^{
                 completion(NO,nil,nil); // here that call when method complete
             });
         }
     }];
}


-(void)listAccountProviderForDeviceId :(NSDictionary*)param userId:(NSString*)userID sessionId:(NSString*)sessionID withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{
    
    AFHTTPSessionManager *manager = [self setSessionManager];
    
    NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/lap"]];
    
    NSDictionary *params =@{@"device-id":[param objectForKey:@"device-id"],@"user-id":userID,@"channel-code":[param objectForKey:@"channel-code"],@"session-id":sessionID,@"seq-no":[param objectForKey:@"seq-no"]};
    
    [manager POST:loginUrl parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"Response from server 1 listAccountProviderForDeviceId :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        NSError* error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

        if(completion){
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(YES,nil,response); // here that call when method complete
            });
        }
    }
          failure:^(NSURLSessionTask *operation, NSError *error)
     
     {
         NSLog(@"Error: %@", error);
         
     }];
    
}



-(void)listAccount :(AccountProvider*)provider commonParam:(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{
    __block NSDictionary *dictData;

    AFHTTPSessionManager *manager = [self setSessionManager];
    DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc]init];
    NSString* deviceId = [deviceIdController generateUUID];

    
    NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/la"]];
    
    NSDictionary *params =@{@"account-provider":provider.getId,@"device-id":[commonParam objectForKey:@"device-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"session-id":[commonParam objectForKey:@"session-id"],@"seq-no":[commonParam objectForKey:@"seq-no"],@"mobile":[commonParam objectForKey:@"mobile"]};
    
    NSLog(@"listAccount params: %@", params);

    [manager POST:loginUrl parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"Response from server 1 listAccount :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        NSError* error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];


        if(completion){
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(YES,nil,response); // here that call when method complete
            });
        }
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error)
     
     {
         NSLog(@"Error: %@", error);

         if (error.code == -1009) {
             NSLog(@"listAccount : The Internet connection appears to be offline.");
         }
         
     }];
}



-(void)checkVpa:(NSString*)vpa commonParam:(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{
    
    __block NSDictionary *dictData;
    AFHTTPSessionManager *manager = [self setSessionManager];
    DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc]init];
    NSString* deviceId = [deviceIdController generateUUID];
    
    NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/check-va"]];
    
    ///open network
    NSDictionary *params =@{@"virtual-address":vpa ,@"device-id":[commonParam objectForKey:@"device-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"session-id":[commonParam objectForKey:@"session-id"] ,@"channel-code":[commonParam objectForKey:@"channel-code"],@"seq-no":[commonParam objectForKey:@"seq-no"]};
    
    [manager POST:loginUrl parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        //NSLog(@"Response from server 1 : checkVpa %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        NSError* error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
      //  NSLog(@"api proxy checkVPA responce ==  %@",[response objectForKey:@"response"]);

        if(completion){
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(YES,nil,response); // here that call when method complete
            });
        }
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error)
     
     {
         NSLog(@"Error: %@", error);
         if(completion){
             dispatch_async(dispatch_get_main_queue(), ^{
                 completion(YES,nil,nil); // here that call when method complete
             });
         }
         
     }];
    
}

/*-(void)listVPA:(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    __block NSDictionary *dictData;
    AFHTTPSessionManager *manager = [self setSessionManager];
    DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc]init];
    NSString* deviceId = [deviceIdController generateUUID];

    NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/check-va"]];

    ///open network
    NSDictionary *params =@{@"virtual-address":vpa ,@"device-id":deviceId,@"user-id":userid,@"session-id":sessionId ,@"channel-code":channelcode,@"seq-no":[self getRandomSequenceNoString]};

    [manager POST:loginUrl parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {

        //NSLog(@"Response from server 1 : checkVpa %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        NSError* error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

        //  NSLog(@"api proxy checkVPA responce ==  %@",[response objectForKey:@"response"]);

        if(completion){
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(YES,nil,response); // here that call when method complete
            });
        }

    }
          failure:^(NSURLSessionTask *operation, NSError *error)

     {
         NSLog(@"Error: %@", error);
         if(completion){
             dispatch_async(dispatch_get_main_queue(), ^{
                 completion(YES,nil,nil); // here that call when method complete
             });
         }

     }];
    
}*/


-(void)generateOTP :(NSString*)accountprovider commonParam:(NSDictionary*)commonParam ifsc:(NSString*)ifsc accountnumber:(NSString*)accountnumber withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion
{

    @try {

        AFHTTPSessionManager *manager = [self setSessionManager];
        DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc]init];
        NSString* deviceId = [deviceIdController generateUUID];


        NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/gotp"]];

        NSDictionary *params =@{@"account-provider":accountprovider,@"device-id":[commonParam objectForKey:@"device-id"],@"ifsc":ifsc,@"channel-code":[commonParam objectForKey:@"channel-code"],@"session-id":[commonParam objectForKey:@"session-id"],@"mobile":[commonParam objectForKey:@"mobile"],@"account-number":accountnumber ,@"seq-no":[commonParam objectForKey:@"seq-no"], @"user-id":[commonParam objectForKey:@"user-id"]};


        NSLog(@"generateOTP apiproxy: %@", params);


        [manager POST:loginUrl parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response from server 1 generateOTP :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }

        }
              failure:^(NSURLSessionTask *operation, NSError *error)
         
         {
             NSLog(@"Error: %@", error);
             
         }];

    } @catch (NSException *exception) {
        NSLog(@"generateOTP NSException : %@",exception);
    }
}


-(NSDictionary*)miniStatement:(NSDictionary*)param withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{
    
    __block NSDictionary *dictData;
    AFHTTPSessionManager *manager = [self setSessionManager];

    
    NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/mini-stmt"]];
    
    ///open network
    NSDictionary *parameters =@{@"device-id":[param objectForKey:@"device-id"],@"session-id":[param objectForKey:@"session-id"],@"user-id":[param objectForKey:@"user-id"],@"channel-code":[param objectForKey:@"channel-code"],@"from-date":[param objectForKey:@"from-date"],@"to-date":[param objectForKey:@"to-date"],@"seq-no":[param objectForKey:@"seq-no"],@"mobile":[param objectForKey:@"mobile"]};  // Add your parameters
    
    [manager POST:loginUrl parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        NSLog(@"Response from server  miniStatement:  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        NSError* error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
        
        
        if(completion){
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(YES,nil,response); // here that call when method complete
            });
        }
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error)
     
     {
         NSLog(@"Error: %@", error);
         if(completion){
             dispatch_async(dispatch_get_main_queue(), ^{
                 completion(YES,nil,nil); // here that call when method complete
             });
         }
         
     }];
    
    
    return dictData;
}


-(NSDictionary*)pay:(NSDictionary*)param otherParam:(NSDictionary*)otherParam commonParam:(NSDictionary*)commonParam mpin:(NSString*)mpin withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    @try {

        __block NSDictionary *dictData;
        AFHTTPSessionManager *manager = [self setSessionManager];

        NSMutableString *loginUrl;
        NSDictionary *parameters;
        NSString* ifsc =@"" ;
        NSString* account_number=@"" ;


        if ([otherParam objectForKey:@"ifsc"]) {
            ifsc = [param objectForKey:@"ifsc"];
        }

        if ([otherParam objectForKey:@"account-number"]) {
            account_number = [param objectForKey:@"account-number"];
        }


        if([[otherParam objectForKey:@"mode"] isEqualToString:@"VPA"]){

            loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/pay-request"]];

            parameters = @{@"device-id":[commonParam objectForKey:@"device-id"],@"session-id":[commonParam objectForKey:@"session-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"mobile":[commonParam objectForKey:@"mobile"],@"mpin":mpin,@"payer-va":[otherParam objectForKey:@"payer-va"],@"amount":[otherParam objectForKey:@"amount"] , @"note":[otherParam objectForKey:@"note"],@"pre-approved":@"M",@"default-debit":@"D",@"default-credit": @"N",@"seq-no":[otherParam objectForKey:@"seq-no"],@"payee-va":[otherParam objectForKey:@"payee-va"],@"use-default-acc":[otherParam objectForKey:@"use-default-acc"],@"account-provider":[otherParam objectForKey:@"account-provider"]};  // Add your parameters

        }else{

         //   NSLog(@"In Global Pay");

            loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/pay-request-global"]];

            if ([[otherParam objectForKey:@"mode"] isEqualToString:@"Account"] ) {

                parameters =@{@"device-id":[commonParam objectForKey:@"device-id"],@"session-id":[commonParam objectForKey:@"session-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"mobile":[commonParam objectForKey:@"mobile"],@"amount":[otherParam objectForKey:@"amount"] , @"note":[otherParam objectForKey:@"note"],@"pre-approved":@"M",@"use-default-acc":[otherParam objectForKey:@"use-default-acc"],@"default-credit": @"N",@"account-type":[otherParam objectForKey:@"account-type"], @"account-provider":[otherParam objectForKey:@"account-provider"],@"payer-va":[otherParam objectForKey:@"payer-va"],@"use-default-acc":[otherParam objectForKey:@"use-default-acc"],@"seq-no":[otherParam objectForKey:@"seq-no"],@"mcc":@"0000",@"merchant-type":@"PERSON",@"payee-name":[param objectForKey:@"payee-name"],@"global-address-type":[param objectForKey:@"global-address-type"],@"payee-account":[param objectForKey:@"payee-account"],@"payee-ifsc":[param objectForKey:@"payee-ifsc"],@"mpin":mpin , @"ifsc":ifsc,@"account-number":account_number,@"payee-va":[otherParam objectForKey:@"payee-va"],@"default-debit":@"D"};  // Add your parameters

            }else if ([[otherParam objectForKey:@"mode"] isEqualToString:@"Mobile"]) {

              parameters =@{@"device-id":[commonParam objectForKey:@"device-id"],@"session-id":[commonParam objectForKey:@"session-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"mobile":[commonParam objectForKey:@"mobile"],@"amount":[otherParam objectForKey:@"amount"] , @"note":[otherParam objectForKey:@"note"],@"pre-approved":@"M",@"use-default-acc":[otherParam objectForKey:@"use-default-acc"],@"default-credit": @"N",@"account-type":[otherParam objectForKey:@"account-type"], @"account-provider":[otherParam objectForKey:@"account-provider"],@"payer-va":[otherParam objectForKey:@"payer-va"],@"use-default-acc":[otherParam objectForKey:@"use-default-acc"],@"seq-no":[otherParam objectForKey:@"seq-no"],@"mcc":@"0000",@"merchant-type":@"PERSON",@"payee-name":[param objectForKey:@"payee-name"],@"global-address-type":[param objectForKey:@"global-address-type"],@"mpin":mpin , @"ifsc":ifsc,@"account-number":account_number,@"payee-va":[otherParam objectForKey:@"payee-va"],@"default-debit":@"D",@"payee-mobile":[param objectForKey:@"payee-mobile"],@"payee-mmid":[param objectForKey:@"mmid"]};  // Add your parameters


            }else if ([[otherParam objectForKey:@"mode"] isEqualToString:@"AADHAAR_IIN"]) {

                parameters =@{@"device-id":[commonParam objectForKey:@"device-id"],@"session-id":[commonParam objectForKey:@"session-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"mobile":[commonParam objectForKey:@"mobile"],@"amount":[otherParam objectForKey:@"amount"] , @"note":[otherParam objectForKey:@"note"],@"pre-approved":@"M",@"use-default-acc":[otherParam objectForKey:@"use-default-acc"],@"default-credit": @"N",@"account-type":[otherParam objectForKey:@"account-type"], @"account-provider":[otherParam objectForKey:@"account-provider"],@"payer-va":[otherParam objectForKey:@"payer-va"],@"use-default-acc":[otherParam objectForKey:@"use-default-acc"],@"seq-no":[otherParam objectForKey:@"seq-no"],@"mcc":@"0000",@"merchant-type":@"PERSON",@"payee-name":[param objectForKey:@"payee-name"],@"global-address-type":[param objectForKey:@"global-address-type"],@"mpin":mpin , @"ifsc":ifsc,@"account-number":account_number,@"payee-va":[otherParam objectForKey:@"payee-va"],@"default-debit":@"D",@"payee-aadhar":[param objectForKey:@"payee-aadhar"],@"payee-iin":[param objectForKey:@"payee-iin"]};  // Add your parameters


            }

        }

        NSLog(@"Request Parameters pay : %@", parameters);

        [manager POST:loginUrl parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response from server 1 pay :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];


            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }

        }
              failure:^(NSURLSessionTask *operation, NSError *error)

         {
             NSLog(@"Error: %@", error);
             if(completion){
                 dispatch_async(dispatch_get_main_queue(), ^{
                     completion(YES,nil,nil); // here that call when method complete
                 });
             }
             
         }];

        return dictData;

    } @catch (NSException *exception) {
        NSLog(@"NSException : %@",exception);
    }
    
}

-(void)listMappedVirtualAddresses :(NSDictionary*)param withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    @try {

        AFHTTPSessionManager *manager = [self setSessionManager];
        DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc]init];
        NSString* deviceId = [deviceIdController generateUUID];

        NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"accounts/list-customer-acc"]];

        NSDictionary *parameters =@{@"device-id":deviceId,@"session-id":[param objectForKey:@"session-id"],@"user-id":[param objectForKey:@"user-id"],@"channel-code":[param objectForKey:@"channel-code"],@"seq-no":[param objectForKey:@"seq-no"],@"mobile":[param objectForKey:@"mobile"]};  // Add your parameters

        NSLog(@"listMappedVirtualAddresses Request parameters : %@", parameters);

        [manager POST:loginUrl parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response from server 1 listMappedVirtualAddresses :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);

            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }

        }
              failure:^(NSURLSessionTask *operation, NSError *error)
         
         {
             NSLog(@"Error: %@", error);
             
         }];


    } @catch (NSException *exception) {
        NSLog(@"listMappedVirtualAddresses NSException : %@",exception);
    }
}

-(void)storeVPA :(NSDictionary*)param withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{
    
    AFHTTPSessionManager *manager = [self setSessionManager];
    DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc]init];
    NSString* deviceId = [deviceIdController generateUUID];


    @try {

        NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"accounts/store-acc-details"]];

        NSDictionary *parameters =@{@"device-id":deviceId,@"session-id":[param objectForKey:@"session-id"],@"user-id":[param objectForKey:@"user-id"],@"channel-code":[param objectForKey:@"channel-code"],@"account-provider":[param objectForKey:@"account-provider"],@"account-number":[param objectForKey:@"account-number"],@"account-type":[param objectForKey:@"account-type"],@"default-debit":[param objectForKey:@"default-debit"],@"default-credit":[param objectForKey:@"default-credit"],@"ifsc":[param objectForKey:@"ifsc"],@"mmid":[param objectForKey:@"mmid"],@"name":[param objectForKey:@"name"],@"virtual-address":[param objectForKey:@"virtual-address"],@"mobile":[param objectForKey:@"mobile"],@"seq-no":[param objectForKey:@"seq-no"],@"credsAllowedJson":[param objectForKey:@"credsAllowedJson"]};  // Add your parameters

        NSLog(@"storeVPA Parameters: %@", parameters);

        [manager POST:loginUrl parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response from server 1 storeVPA :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }

        }
              failure:^(NSURLSessionTask *operation, NSError *error)
         
         {
             NSLog(@"Error: %@", error);
             
         }];

    } @catch (NSException *exception) {
        NSLog(@"storeVPA NSException : %@",exception);
    }

    

}

-(void)registerMobile :(NSDictionary*)param withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

   // NSLog(@"registerMobile param: %@", param);

    @try {

        AFHTTPSessionManager *manager = [self setSessionManager];
        DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc]init];
        NSString* deviceId = [deviceIdController generateUUID];
        NSLog(@"registerMobile  deviceId: %@", deviceId);

        NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"accounts/register-mobile"]];

        NSDictionary *parameters =@{@"action-flag":[param objectForKey:@"action-flag"],@"card-digits":[param objectForKey:@"card-digits"],@"expiry-date":[param objectForKey:@"expiry-date"],@"mpin":[param objectForKey:@"mpin"],@"account-provider":[param objectForKey:@"account-provider"],@"account-number":[param objectForKey:@"account-number"],@"account-type":[param objectForKey:@"account-type"],@"default-debit":[param objectForKey:@"default-debit"],@"default-credit":[param objectForKey:@"default-credit"],@"ifsc":[param objectForKey:@"ifsc"],@"otp":[param objectForKey:@"otp"],@"name":[param objectForKey:@"name"],@"credsAllowedJson":[param objectForKey:@"credsAllowedJson"],@"seq-no":[param objectForKey:@"seq-no"],@"device-id":deviceId,@"mobile":[param objectForKey:@"mobile"],@"session-id":[param objectForKey:@"session-id"],@"session-id":[param objectForKey:@"session-id"],@"user-id":[param objectForKey:@"user-id"],@"channel-code":[param objectForKey:@"channel-code"],@"virtual-address":[param objectForKey:@"virtual-address"],@"mmid":[param objectForKey:@"mmid"]};  // Add your parameters


        NSLog(@"registerMobile  param: %@", parameters);

        [manager POST:loginUrl parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response from server 1 registerMobile :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }

        }
              failure:^(NSURLSessionTask *operation, NSError *error)
         
         {
             NSLog(@"Error: %@", error);
             
         }];


    } @catch (NSException *exception) {
        NSLog(@"registerMobile NSException : %@",exception);
    }


}

-(void)validateVirtualAddresses :(NSDictionary*)param commonParam:(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    @try {

        AFHTTPSessionManager *manager = [self setSessionManager];


        NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/validate-address"]];

        NSDictionary *parameters =@{@"session-id":[commonParam objectForKey:@"session-id"],@"device-id":[commonParam objectForKey:@"device-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"seq-no":[commonParam objectForKey:@"seq-no"],@"virtual-address":[param objectForKey:@"virtual-address"],@"payee-name":[param objectForKey:@"payee-name"],@"mobile":[commonParam objectForKey:@"mobile"]};  // Add your parameters

        NSLog(@"validateVirtualAddresses parameters: %@", parameters);

        [manager POST:loginUrl parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response from server 1 validateVirtualAddresses :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }

        }
              failure:^(NSURLSessionTask *operation, NSError *error)
         
         {
             NSLog(@"Error: %@", error);
             
         }];

    } @catch (NSException *exception) {
        NSLog(@"validateVirtualAddresses NSException : %@",exception);
    }


}
-(void)listKeys :(NSDictionary*)param sessionId:(NSString*)sessionid withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{
   // NSLog(@"listKeys: param : %@", param);

    @try {

        AFHTTPSessionManager *manager = [self setSessionManager];
        DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc]init];
        NSString* deviceId = [deviceIdController generateUUID];

        NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/list-keys"]];

        NSDictionary *parameters =@{@"device-id":deviceId,@"user-id":[param objectForKey:@"user-id"],@"session-id":sessionid,@"channel-code":[param objectForKey:@"channel-code"],@"seq-no":[param objectForKey:@"seq-no"]};  // Add your parameters

        [manager POST:loginUrl parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            //  NSLog(@"Response from server listKeys :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }

        }
              failure:^(NSURLSessionTask *operation, NSError *error)
         
         {
             NSLog(@"Error: %@", error);
             
         }];

    } @catch (NSException *exception) {
        NSLog(@"listKeys NSException : %@",exception);
    }

}



-(NSString *) getRandomSequenceNoString{
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: 35];

    for (int i=0; i<35; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
  //  NSLog(@"randomString Seq No : %@ ", randomString );

    return randomString;
}

-(void)removeVPA :(NSString*)vpa reason:(NSString*)reason params:(NSDictionary*)params withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    @try {

        AFHTTPSessionManager *manager = [self setSessionManager];

        NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"user/de-register"]];

        NSDictionary *parameters =@{@"delete-va-flag":@"true",@"virtual-address": vpa, @"device-id":[params objectForKey:@"device-id"],@"session-id":[params objectForKey:@"session-id"],@"user-id":[params objectForKey:@"user-id"],@"channel-code":[params objectForKey:@"channel-code"],@"seq-no":[params objectForKey:@"seq-no"],@"mobile":[params objectForKey:@"mobile"],@"reason": reason};


        [manager POST:loginUrl parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response from server removeVPA :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }

        }
              failure:^(NSURLSessionTask *operation, NSError *error)
         
         {
             NSLog(@"Error: %@", error);
             
         }];

    } @catch (NSException *exception) {
        NSLog(@"removeVPA NSException : %@",exception);
    }

}
-(void)deregister :(NSString*)reason params:(NSDictionary*)params withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{


    @try {

        AFHTTPSessionManager *manager = [self setSessionManager];

        NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"user/de-register"]];

        NSDictionary *parameters =@{@"delete-va-flag":@"false",@"reason": reason, @"device-id":[params objectForKey:@"device-id"],@"session-id":[params objectForKey:@"session-id"],@"user-id":[params objectForKey:@"user-id"],@"channel-code":[params objectForKey:@"channel-code"],@"seq-no":[params objectForKey:@"seq-no"],@"mobile":[params objectForKey:@"mobile"]};

        [manager POST:loginUrl parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response from server deregister :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }

        }
              failure:^(NSURLSessionTask *operation, NSError *error)
         
         {
             NSLog(@"Error: %@", error);
             
         }];

    } @catch (NSException *exception) {
        NSLog(@"deregister NSException : %@",exception);
    }

}

-(void)getPendingRequest :(NSString*)vpa params:(NSDictionary*)params withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    @try {

        AFHTTPSessionManager *manager = [self setSessionManager];

        NSMutableString *url = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/get-pending-request"]];

        NSDictionary *parameters =@{@"device-id":[params objectForKey:@"device-id"],@"session-id":[params objectForKey:@"session-id"],@"user-id":[params objectForKey:@"user-id"],@"channel-code":[params objectForKey:@"channel-code"],@"virtual-address":vpa,@"seq-no":[params objectForKey:@"seq-no"]};

        [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response from server 1 getPendingRequest :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }

        }
              failure:^(NSURLSessionTask *operation, NSError *error)
         
         {
             NSLog(@"Error: %@", error);
             
         }];

    } @catch (NSException *exception) {
        NSLog(@"getPendingRequest NSException : %@",exception);
    }

}

-(void)changeMpin :(NSDictionary*)params commonParam:(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    @try {

        AFHTTPSessionManager *manager = [self setSessionManager];

        NSMutableString *url = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/change-mpin"]];
       // NSLog(@"changeMpin  Apiproxy ");

        NSDictionary *parameters =@{@"device-id":[commonParam objectForKey:@"device-id"],@"session-id":[commonParam objectForKey:@"session-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"seq-no":[params objectForKey:@"seq-no"],@"mobile":[commonParam objectForKey:@"mobile"],@"account-provider": [params objectForKey:@"account-provider"],@"old-mpin":[params objectForKey:@"old-mpin"] ,@"ifsc":[params objectForKey:@"ifsc"],@"account-number":[params objectForKey:@"account-number"],@"account-type":[params objectForKey:@"account-type"],@"new-mpin":[params objectForKey:@"new-mpin"],@"virtual-address":[params objectForKey:@"virtual-address"]};

        NSLog(@"changeMpin Request parameters : %@",parameters);

        [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response from server  changeMpin :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }

        }
              failure:^(NSURLSessionTask *operation, NSError *error)

         {
             NSLog(@"Error: %@", error);

         }];

    } @catch (NSException *exception) {
        NSLog(@"changeMpin NSException : %@",exception);
    }
    
}

-(void)resetMpin :(NSDictionary*)params commonParam:(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    @try {

        AFHTTPSessionManager *manager = [self setSessionManager];

        NSMutableString *url = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/change-mpin"]];

        NSDictionary *parameters =@{@"device-id":[params objectForKey:@"device-id"],@"session-id":[params objectForKey:@"session-id"],@"user-id":[params objectForKey:@"user-id"],@"channel-code":[params objectForKey:@"channel-code"],@"seq-no":[params objectForKey:@"seq-no"]};

        [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response from server  changeMpin :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }

        }
              failure:^(NSURLSessionTask *operation, NSError *error)

         {
             NSLog(@"Error: %@", error);

         }];

    } @catch (NSException *exception) {
        NSLog(@"resetMpin NSException : %@",exception);
    }
    
}

-(void)changeVPADefaultAccount :(NSDictionary*)param commonParam:(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    AFHTTPSessionManager *manager = [self setSessionManager];
    DeviceIdViewController *deviceIdController = [[DeviceIdViewController alloc]init];
    NSString* deviceId = [deviceIdController generateUUID];

    @try {

        NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"accounts/change-primary-account"]];

        NSDictionary *parameters =@{@"device-id":[commonParam objectForKey:@"device-id"],@"session-id":[commonParam objectForKey:@"session-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"account-provider":[param objectForKey:@"account-provider"],@"account-number":[param objectForKey:@"account-number"],@"account-type":[param objectForKey:@"account-type"],@"default-debit":[param objectForKey:@"default-debit"],@"default-credit":[param objectForKey:@"default-credit"],@"ifsc":[param objectForKey:@"ifsc"],@"name":[param objectForKey:@"name"],@"virtual-address":[param objectForKey:@"virtual-address"],@"mobile":[commonParam objectForKey:@"mobile"],@"seq-no":[commonParam objectForKey:@"seq-no"]};  // Add your parameters

      //  NSLog(@"changeVPADefaultAccount Parameters: %@", parameters);

        [manager POST:loginUrl parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response from server 1 change VPA DefaultAccount :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }
        }
              failure:^(NSURLSessionTask *operation, NSError *error)

         {
             NSLog(@"Error: %@", error);

         }];

    } @catch (NSException *exception) {
        NSLog(@"changeVPADefaultAccount NSException : %@",exception);
    }
}

-(void)collect :(TransactionParameters*)transParam commonParams:(NSDictionary*)commonParam transId:(NSString*)transId expierAfter:(int)expierAfter view:(UIViewController*)view withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion
{

    AFHTTPSessionManager *manager = [self setSessionManager];
    NSString* amount = [NSString stringWithFormat:@"%.2f",[transParam.amount doubleValue]];
    NSString* expireTime = [NSString stringWithFormat:@"%d",expierAfter];
    NSLog(@"collect transParam amount %@",amount);

    NSLog(@"collect transParam.payerParam.getPayerVpa: %@", transParam.payerParam.getPayerVpa);
    NSLog(@"collect transParam.payeeParam.getVpa: %@", transParam.payeeParam.getVpa);

    @try {

        NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/collect-request"]];

        NSDictionary *parameters =@{@"device-id":[commonParam objectForKey:@"device-id"],@"session-id":[commonParam objectForKey:@"session-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"account-provider":transParam.payeeParam.getAccountProvider.getId,@"account-number":transParam.payeeParam.account,@"ifsc":transParam.payeeParam.getifsc,@"note":transParam.remark,@"amount":amount,@"mobile":[commonParam objectForKey:@"mobile"],@"seq-no":transId ,@"payer-va":transParam.payerParam.payerVpa,@"payee-va":transParam.payeeParam.getVpa ,@"expire-after":expireTime };  // Add your parameters

        NSLog(@"collect Parameters: %@", parameters);

        [manager POST:loginUrl parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response  from server [collect] :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }
        }
              failure:^(NSURLSessionTask *operation, NSError *error)

         {
             NSLog(@"Error: %@", error);

         }];

    } @catch (NSException *exception) {
        NSLog(@"collect NSException : %@",exception);
    }

}

-(void)balanceInquiry :(NSDictionary*)params commonParam:(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    @try {

        AFHTTPSessionManager *manager = [self setSessionManager];

        NSMutableString *url = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/bal-inq"]];

        NSDictionary *parameters =@{@"device-id":[commonParam objectForKey:@"device-id"],@"session-id":[commonParam objectForKey:@"session-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"seq-no":[params objectForKey:@"seq-no"],@"mobile":[commonParam objectForKey:@"mobile"],@"account-provider":[params objectForKey:@"account-provider"],@"account-number":[params objectForKey:@"account-number"],@"account-type":[params objectForKey:@"account-type"],@"ifsc":[params objectForKey:@"ifsc"],@"name":[params objectForKey:@"name"],@"virtual-address":[params objectForKey:@"virtual-address"] ,@"mpin":[params objectForKey:@"mpin"]};

        NSLog(@"balanceInquiry Parameters: %@", parameters);

        [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response from server  balanceInquiry :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }

        }
              failure:^(NSURLSessionTask *operation, NSError *error)

         {
             NSLog(@"Error: %@", error);

         }];

    } @catch (NSException *exception) {
        NSLog(@"balanceInquiry NSException : %@",exception);
    }
    
}

-(void)getPendingTransaction :(NSDictionary*)commonParam withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    @try {

        AFHTTPSessionManager *manager = [self setSessionManager];

        NSMutableString *url = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/get-pending-request"]];

        NSDictionary *parameters =@{@"device-id":[commonParam objectForKey:@"device-id"],@"session-id":[commonParam objectForKey:@"session-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"seq-no":[commonParam objectForKey:@"seq-no"],@"mobile":[commonParam objectForKey:@"mobile"]};

        NSLog(@"getPendingTransaction parameters  : %@",parameters);

        [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response from server  getPendingTransaction :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }

        }
              failure:^(NSURLSessionTask *operation, NSError *error)

         {
             NSLog(@"Error: %@", error);

         }];

    } @catch (NSException *exception) {
        NSLog(@"getPendingTransaction NSException : %@",exception);
    }
    
}

-(void)approveReject:(NSDictionary*)commonParam otherParams:(NSDictionary*)otherParams withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{

    @try {

        AFHTTPSessionManager *manager = [self setSessionManager];

        NSMutableString *url = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,@"transaction/collect-auth"]];

        NSDictionary *parameters =@{@"device-id":[commonParam objectForKey:@"device-id"],@"session-id":[commonParam objectForKey:@"session-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"seq-no":[otherParams objectForKey:@"seq-no"],@"mobile":[commonParam objectForKey:@"mobile"],@"payer-va":[otherParams objectForKey:@"payer-va"] , @"payee-va":[otherParams objectForKey:@"payee-va"],@"amount":[otherParams objectForKey:@"amount"],@"payer-amount":[otherParams objectForKey:@"payer-amount"],@"note":[otherParams objectForKey:@"note"],@"action":[otherParams objectForKey:@"action"],@"upi-tranlog-id":[otherParams objectForKey:@"upi-tranlog-id"],@"account-type":[otherParams objectForKey:@"account-type"],@"ifsc":[otherParams objectForKey:@"ifsc"],@"account-number":[otherParams objectForKey:@"account-number"],@"account-provider":[otherParams objectForKey:@"account-provider"] ,@"pre-approved":[otherParams objectForKey:@"pre-approved"],@"mpin":[otherParams objectForKey:@"mpin"]};

        NSLog(@"Collect Auth parameters  : %@",parameters);

        [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {

            NSLog(@"Response from server  Collect Auth :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
            NSError* error;
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];

            if(completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,response); // here that call when method complete
                });
            }

        }
              failure:^(NSURLSessionTask *operation, NSError *error)

         {
             NSLog(@"Error: %@", error);

         }];

    } @catch (NSException *exception) {
        NSLog(@"Collect Auth NSException : %@",exception);
    }

}

-(void)responseWithUrl:(NSDictionary*)commonParam urlString:(NSString*)url withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{
    __block NSDictionary *dictData;

    AFHTTPSessionManager *manager = [self setSessionManager];

    NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,url]];

    NSDictionary *params =@{@"device-id":[commonParam objectForKey:@"device-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"session-id":[commonParam objectForKey:@"session-id"],@"seq-no":[commonParam objectForKey:@"seq-no"],@"mobile":[commonParam objectForKey:@"mobile"]};

    NSLog(@"listAccount params: %@", params);

    [manager POST:loginUrl parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {

        NSLog(@"Response from server listVa :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        NSError* error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];


        if(completion){
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(YES,nil,response); // here that call when method complete
            });
        }

    }
          failure:^(NSURLSessionTask *operation, NSError *error)

     {
         NSLog(@"Error: %@", error);

         if (error.code == -1009) {
             NSLog(@"listAccount : The Internet connection appears to be offline.");
         }
         
     }];
}

-(void)complaintResponse:(NSDictionary*)commonParam parameters:(NSDictionary*)parameters urlString:(NSString*)url withCompletion:(void(^)(BOOL success, NSError* error, id responce))completion{
    __block NSDictionary *dictData;
    NSDictionary *params;
    AFHTTPSessionManager *manager = [self setSessionManager];

    NSMutableString *loginUrl = [[NSMutableString alloc] initWithString:[NSString stringWithFormat: @"%@%@",URL,url]];

    if([url isEqualToString:@"transaction/check-complaint-status"]){

        params =@{@"device-id":[commonParam objectForKey:@"device-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"session-id":[commonParam objectForKey:@"session-id"],@"seq-no":[commonParam objectForKey:@"seq-no"],@"mobile":[commonParam objectForKey:@"mobile"],@"complaint-id":[parameters objectForKey:@"complaint-id"]};

    }else{
        params =@{@"device-id":[commonParam objectForKey:@"device-id"],@"user-id":[commonParam objectForKey:@"user-id"],@"channel-code":[commonParam objectForKey:@"channel-code"],@"session-id":[commonParam objectForKey:@"session-id"],@"seq-no":[commonParam objectForKey:@"seq-no"],@"mobile":[commonParam objectForKey:@"mobile"],@"reason-code":[parameters objectForKey:@"reason-code"],@"comment":[parameters objectForKey:@"comment"],@"rrn":[parameters objectForKey:@"rrn"],@"txn-type":[parameters objectForKey:@"txn-type"],@"txn-date":[parameters objectForKey:@"txn-date"]};
    }

    NSLog(@"complaintResponse params: %@", params);

    [manager POST:loginUrl parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {

        NSLog(@"Response from server complaintResponse :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        NSError* error;
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];


        if(completion){
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(YES,nil,response); // here that call when method complete
            });
        }

    }
          failure:^(NSURLSessionTask *operation, NSError *error)

     {
         NSLog(@"Error: %@", error);

         if (error.code == -1009) {
             NSLog(@"listAccount : The Internet connection appears to be offline.");
         }
         
     }];
}

@end
