//
//  SdkConnector.m
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "SdkConnector.h"
#import <CommonLibrary/CLServices.h>




@implementation SdkConnector

@synthesize deviceProfile;
@synthesize clientApplication;
@synthesize secureKeys;
@synthesize token;
@synthesize accountProviderMap;
@synthesize accountProviders;

//NSString* TAG = @"Sdk-Connector";
CLServices* clservices;


-(id)initWith:(SdkClientApplication *)client_Application deviceProfile:(DeviceProfile *)device_Profile{

    if (self = [super init]) {

     deviceProfile = device_Profile;
     clientApplication = client_Application;
  }
    return self;
}

-(NSString *)getChallenge:(NSString *)type device_Id:(NSString *)device_Id app_Id:(NSString *)app_Id{

    NSString* challenge = nil;
    NSError* error = nil;
    //    NSLog(@"getChallenge type : %@",type);
    //    NSLog(@"getChallenge Challenge : %@",device_Id);
    //    NSLog(@"getChallenge Challenge : %@",app_Id);

    BOOL success = [CLServices getChallengeForDeviceId:device_Id appId:app_Id type:type challenge:&challenge error:&error];

    NSLog(success ? @"SrvtCommonLib Yes" : @"SrvtCommonLib No");

    NSLog(@"SrvtCommonLib Challenge : %@",challenge);
    
    return challenge;
}



-(NSString*)getToken{
    return token;
}

-(CLServices*)getCLService{
    return  clservices;
}

-(void)integrateLibrary:(TokenType)tokenType{

    NSLog(@"CommonUtils tokenType:",tokenType);

    token = [clientApplication readValue:@"token"];


   // if(token==nil || tokenType)


}












@end
