//
//  AccountService.m
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountService.h"


@implementation AccountService

SdkClientApplication* clientApplication;
DeviceProfile* deviceProfile;
SdkConnector* sdkConnector;

-(id)initWith :(SdkClientApplication*) client_Application  device_Profile:(DeviceProfile*) device_Profile sdk_Connector:(SdkConnector*) sdk_Connector{

    if (self = [super init]) {

    clientApplication = client_Application;
    deviceProfile = device_Profile;
    sdkConnector = sdk_Connector;

    }
    return self;

}






@end

