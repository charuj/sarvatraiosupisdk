//
//  SdkAppUser.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//
#import <Foundation/Foundation.h>

@protocol SdkAppUser

-(void)printing;
@end


//@protocol SdkAppUser<NSObject>
//
///**
// * Returns Host Application User Name.
// * @return User Name
// */
//-(NSString*)getUserName;
//
///**
// * Returns Host Application User Email.
// * @return User Email
// */
//-(NSString*)getUserEmail;
//
///**
// * Returns Host Application User Mobile.
// * @return Mobile (without +91 or 91)
// */
//-(NSString*)retrieveMobile;
//
///**
// * Host Application Unique ID of the current User
// * @return Unique ID
// */
//-(NSString*)getUserIdentity;
//
//@end



