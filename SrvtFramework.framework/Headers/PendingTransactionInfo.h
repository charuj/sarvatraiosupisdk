//
//  PendingTransactionInfo.h
//  SrvtFramework
//
//  Created by Charushila on 31/07/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransactionItem.h"

@interface PendingTransactionInfo : TransactionItem {
    NSString *direction;
    NSDate  *expireAfter;
    NSDate  *date;
    NSString *tranLogID;
    TransactionItem *transactionItem;

}

@property (nonatomic) NSString *direction;
@property (nonatomic) NSDate *expireAfter;
@property (nonatomic) NSDate *date;
@property (nonatomic) NSString *tranLogID;
@property (nonatomic) TransactionItem *transactionItem;


/**
 * Initialise pending transaction info.
 *
 * @return PendingTransactionInfo object.
 */
-(id)initWith :(NSDictionary*)pendingDetails;


/**
 * Get direction.
 *
 * @return direction.
 */
-(NSString*) getDirection;

/**
 * Get expire After.
 *
 * @return expire After.
 */
-(NSDate*) getExpireAfter;

/**
 * Get Date.
 *
 * @return Date.
 */
-(NSDate*) getDate;

/**
 * Get TranLogID.
 *
 * @return TranLogID.
 */
-(NSString*)getTranLogID;


@end
