//
//  DeviceProfile.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceProfile : NSObject{
    
    NSString* deviceID ;
    NSString* mobile;
    NSString* userID;
    
}

//@property (nonatomic, retain) NSString *deviceID;
@property (nonatomic, retain) NSString *mobile;
//@property (nonatomic, retain) NSString *userID;

-(void)setDeviceID;
-(NSString*)getDeviceID;

-(void)setMobile;
-(NSString*)getMobile;

-(void)setUserID;
-(NSString*)getUserID;



@end
