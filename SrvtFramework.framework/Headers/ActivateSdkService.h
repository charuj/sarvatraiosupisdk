//
//  ActivateSdkService.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "ApiProxyProtocol.h"

@interface ActivateSdkService : NSObject



-(BOOL)hasEnableSessionCheck;

//-(BOOL)execute;

-(void) addApiParameters:(NSDictionary*)sdkKeyValue;

-(NSString*)getApiUrl;




@end
