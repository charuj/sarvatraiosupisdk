//
//  RegisterVPAService.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardData.h"
#import "CustomerAccount.h"
#import "AccountDefaultStatus.h"
#import <UIKit/UIKit.h>


@interface RegisterVPAService : NSObject{

    CardData *cardData;
    CustomerAccount *custAccount;
    NSString *vpa;
    NSString *mpinParam;
    NSString *otpParam;
    AccountDefaultStatus *accDefaultStatus;
    

}
typedef enum { Register,ResetMPIN} Mode;

@property (nonatomic) Mode mode;

-(void)addApiParameters;


@end
