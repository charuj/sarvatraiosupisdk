//
//  AccountDefaultStatus.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccountDefaultStatus : NSObject <NSCoding>{
    BOOL defaultCredit;
    BOOL defaultDebit;
}


@property (nonatomic) BOOL defaultCredit;
@property (nonatomic) BOOL defaultDebit;


@end
