//
//  TransactionItem.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransactionItem : NSObject{

    NSString* payeeVa;
    NSString* payerVa;
    NSString* remark;
    NSDecimalNumber* amount;
    NSString* transactionType;
    NSString* status;
    NSString* seqNo;
}
@property (nonatomic) NSString* payeeVa;
@property (nonatomic) NSString* payerVa;
@property (nonatomic) NSString* remark;
@property (nonatomic) NSDecimalNumber* amount;
@property (nonatomic) NSString* transactionType;
@property (nonatomic) NSString* status;
@property (nonatomic) NSString* seqNo;

typedef enum { Pending,MiniStatement} TransactionItemMode;

@property (nonatomic) TransactionItemMode tm_mode;

@end
