//
//  TransactionServiceImpl.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SdkClientApplication.h"
#import "CustomerAccount.h"
#import "SdkConnector.h"
#import "DeviceProfile.h"
#import "TransactionParameters.h";


@interface TransactionServiceImpl : NSObject


-(void)pay:(TransactionParameters*)transactionParam;




@end
