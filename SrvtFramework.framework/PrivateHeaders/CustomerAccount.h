//
//  CustomerAccount.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomerAccount : NSObject{
    
    NSString* mmid;
    NSString* account;
    NSString* maskedAccount;

    NSString* ifsc;
    NSString* name;
    BOOL mobileBankingEnabled;
    BOOL aadhaarEnabled;
    //private AccountProvider provider;

    BOOL defaultCredit;
    BOOL defaultDebit;
    NSString* accountProvider;
    NSString* accountType;
    NSDictionary* credentialData;
    NSString* credentialRequired;

}


-(NSString*)getMmid;
-(NSString*)getAccount;
-(NSString*)getIfsc;
-(NSString*)getName;
-(BOOL)getMobileBankingEnabled;
-(BOOL)getAadhaarEnabled;


//public AccountProvider getProvider() {
//    return provider;
//}
-(NSString*)getAccountProvider;

-(NSString*)getAccountType;
-(NSString*)getMaskedAccount;
-(BOOL)getDefaultCredit;
-(BOOL)getDefaultDebit;
-(NSDictionary*)getCredentialData;
-(NSString*)getCredentialRequired;


@end
