//
//  CardData.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CardData : NSObject<NSCoding>{
    NSString* lastSix;
    short expiryMonth;
    short expiryYear;

}
@property (nonatomic) NSString* lastSix;
@property (nonatomic) short expiryMonth;
@property (nonatomic) short expiryYear;


/**
 * Get last six digits of the Card
 *
 * @return cards six digits
 */
-(NSString*) getLastSix;
/**
 * Set last six digits of the Card
 *
 * @param lastSix Last six digits of the Card (Plain text)
 */
-(void) setLastSix:(NSString*)lastSix;

/**
 * Get Expiry Month of the Card
 *
 * @return Expiry Month
 */
-(short) getExpiryMonth;
/**
 * Set Expiry Month of the Card
 *
 * @param expiryMonth Expiry Month
 */
-(void) setExpiryMonth:(short) expiryMonth ;
/**
 * Get Expiry Year of the Card
 *
 * @return Expiry Year
 */
-(short)getExpiryYear;
/**
 * Set Expiry Year of the Card
 *
 * @param expiryYear Expiry Year
 */
-(void) setExpiryYear:(short) expiryYear ;
//-(void) validate;
@end
