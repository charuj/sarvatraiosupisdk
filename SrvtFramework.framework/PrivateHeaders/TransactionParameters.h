//
//  TransactionParameters.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomerAccount.h"
#import "PayeeParam.h"
#import "PayerParam.h"


@interface TransactionParameters : NSObject{

    NSDecimalNumber* amount;
    NSString* remark;
    CustomerAccount* custAccount;
    PayeeParam* payeeParam;
    PayerParam* payerParam;

    BOOL usingDefaultAccount;
    BOOL aadhaarEnabled;
    NSString* vpa;

    //private AccountProvider provider;
}

typedef enum { Pay, Collect, Manage, MerchantCollect} ParameterMode;
typedef enum { VPA, Account, Mobile, AADHAAR_IIN} PayeeMode;

@property (nonatomic) PayeeMode payeeMode;
@property (nonatomic) ParameterMode parameterMode;
@property (nonatomic) PayeeParam* payeeParam;


- (id)initWithAccount:(NSString *)payeeName
                 ifsc:(NSString *)ifsc
              account:(NSString *)account;



- (id)initWithVPA:(NSString *)vpa;


- (id)initWithAccount:(NSString *)payeeName
                 ifsc:(NSString *)ifsc
              account:(NSString *)account;

-(id)initWithTransaction:(NSString *)from_vpa
                   payee:(PayeeParam *)payee
                  amount:(NSDecimalNumber *)amountToTrans
                  remark:(NSString *)tranRemark;



@end
