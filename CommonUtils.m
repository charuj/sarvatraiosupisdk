//
//  CommonUtils.m
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import "CommonUtils.h"
#import "SdkException.h"

@implementation CommonUtils

@synthesize tokenType;




-(void)validateTrue:(BOOL)value error:(NSString *)error{

    if (!value) {
        @throw [[SdkException alloc] initWithName:nil reason:error userInfo:nil];
    }
}

-(void)notNull:(NSObject *)object name:(NSString *)name{

    if (!object || object== nil || object == NULL) {
        @throw [[SdkException alloc] initWithName:name reason:[NSString stringWithFormat:@"%@%@",name,@" is null."] userInfo:nil];
    }
}

-(void)notBlank:(NSString *)value name:(NSString *)name{
    if (value==NULL || [value isEqualToString:@""]) {
        @throw [[SdkException alloc] initWithName:name reason:[NSString stringWithFormat:@"%@%@",name,@" is blank."] userInfo:nil];

    }
}

-(void)maxLength:(NSString *)value length:(int)length name:(NSString *)name{
    if (value==NULL || [value isEqualToString:@""]) {
        return;
    }
    if (value.length > length) {
        @throw [[SdkException alloc] initWithName:name reason:[NSString stringWithFormat:@"%@%@",name,@" is exceeds allowed limit"] userInfo:nil];
    }

}

-(void)minLength:(NSString *)value length:(int)length name:(NSString *)name{
    if (value == nil || [value isEqualToString:@""]) {
        @throw [[SdkException alloc] initWithName:name reason:[NSString stringWithFormat:@"%@%@",name,@" is blank."] userInfo:nil];
    }
    if (value.length<length) {
        @throw [[SdkException alloc] initWithName:name reason:[NSString stringWithFormat:@"%@%@",name,@" is missing required length."] userInfo:nil];
    }

}

-(void)equals:(NSString *)value1 value2:(NSString *)value2 name:(NSString *)name{
    if (![value1 isEqualToString:value2]) {
        @throw [[SdkException alloc] initWithName:name reason:[NSString stringWithFormat:@"%@%@%@%@%@",name,@" value not matching.",value1,@" = ",value2] userInfo:nil];

    }
}

-(void)aboveZero:(int)value name:(NSString *)name{

    if (value<=0) {
        @throw [[SdkException alloc] initWithName:name reason:[NSString stringWithFormat:@"%@%@",name,@" is value possibly zero or less."] userInfo:nil];

    }
}

- (NSDictionary*)createDict :(NSString*)str{

    //  NSString *jsonString = @"{\"ID\":{\"Content\":268,\"type\":\"text\"},\"ContractTemplateID\":{\"Content\":65,\"type\":\"text\"}}";
    NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    return json;
}

//CommonUtils(SdkClientApplication clientApplication, DeviceProfile deviceProfile) {
//    this.clientApplication = clientApplication;
//    this.deviceProfile = deviceProfile;
//}
//
//public String getToken(String challenge, TokenType tokenType) {
//    return new TokenHelper(clientApplication, deviceProfile, challenge, tokenType).getToken();
//}
//
//public static String byteArrayToHex(byte[] a) {
//    StringBuilder sb = new StringBuilder(a.length * 2);
//    for (byte b : a)
//        sb.append(String.format("%02x", b & 0xff));
//    return sb.toString();
//}
//
//
//static String fromDate(Date date, String dateFormat) {
//    if (date == null || StringUtils.isBlank(dateFormat))
//        return null;
//
//    DateFormat format = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
//    return format.format(date);
//}
//
//static String createSdkToken(DeviceProfile profile, SdkClientApplication sdkClientApplication) {
//    try {
//        String secret = "ilpaptml-b7ac-4976-a070-9883bc75ee42";
//        String message = sdkClientApplication.getApplicationName() + sdkClientApplication.getUserProfile().getUserName() + "-" + sdkClientApplication.getUserProfile().retrieveMobile() + "-" + profile.getDeviceId() + sdkClientApplication.getUserProfile().getUserIdentity();
//        Log.d("Message$$", message);
//        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
//        SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
//        sha256_HMAC.init(secret_key);
//        String hash = Base64.encodeToString(sha256_HMAC.doFinal(message.getBytes()), Base64.NO_WRAP);
//        return hash;
//    } catch (Exception e) {
//        System.out.println("Error");
//    }
//
//    return "1234";
//}
//
//public static String fromAmount(BigDecimal amount) {
//    DecimalFormat df = new DecimalFormat();
//    df.setMaximumFractionDigits(2);
//    df.setMinimumFractionDigits(2);
//    df.setGroupingUsed(false);
//    return df.format(amount);
//}


@end
