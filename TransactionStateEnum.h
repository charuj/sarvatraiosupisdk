//
//  TransactionStateEnum.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransactionStateEnum : NSObject

typedef enum
{
    /**
     * Checking Parameters provided
     */
    CHECKING_PARAMETERS = 0,
    
    /**
     * Before Pushing Parameters to API
     */
    BEFORE_PUSH = 1,
    /**
     * Got Response from API or Failure to Connect
     */
    ON_RESULT = 2
    
} TransactionState;

@end
