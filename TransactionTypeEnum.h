//
//  TransactionTypeEnum.h
//  SrvtFramework
//
//  Created by Charushila on 19/06/17.
//  Copyright © 2017 Charushila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransactionTypeEnum : NSObject
typedef enum
{
    /**
     * Not Defined
     */
    NOT_DEFINED = 0,
    /**
     * Pay
     */
    Pay = 1,
    /**
     * Pay to Merchant
     */
    PayMerchant = 2,
    /**
     * Balance Inquiry
     */
    BalanceInquiry = 3,
    /**
     * Approve Pending
     */
    ApprovePending = 4,
    /**
     * Register Mobile
     */
    RegisterMobile = 5,
    /**
     * Change MPIN
     */
    ChangePin = 5
} TransactionType;

@end
